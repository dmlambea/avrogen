// Package generator contains utility methods for managing and writing generated code
package generator

import (
	"fmt"
	"io/ioutil"
	"path"
	"path/filepath"
	"regexp"
)

// PackageHeaderCustomizerFunc is a function type to allow adding header
// information to generated source files.
type PackageHeaderCustomizerFunc func() string

// Package represents the output package with a header comment
type Package struct {
	HeaderCustomizerFunc PackageHeaderCustomizerFunc
	PackageName          string
	TargetDir            string
}

var packageNameRegexp = regexp.MustCompile(`^[a-zA-Z][a-zA-Z0-9]*$`)

// WriteFile decorates the sourceCode with this package's name and header comments, then
// writes the resulting text to fileName.
func (p Package) WriteFile(fileName, sourceCode string) (err error) {
	pkgName := p.PackageName
	if pkgName == "" {
		pkgName = path.Base(p.TargetDir)
		if pkgName == "/" || pkgName == "." {
			pkgName = "main"
		}
	}
	if !packageNameRegexp.MatchString(pkgName) {
		return fmt.Errorf("invalid package name '%s'", pkgName)
	}

	targetFile := filepath.Join(p.TargetDir, fileName)
	customHeader := ""
	if p.HeaderCustomizerFunc != nil {
		customHeader = p.HeaderCustomizerFunc()
	}
	fileContents := fmt.Sprintf("%vpackage %v\n%v", customHeader, pkgName, sourceCode)
	err = ioutil.WriteFile(targetFile, []byte(fileContents), 0640)
	if err != nil {
		err = fmt.Errorf("error writing file %s: %v", targetFile, err)
	}
	return
}
