package flat

import (
	"fmt"

	"gitlab.com/dmlambea/avrogen/internal/decorators"
	"gitlab.com/dmlambea/avrogen/internal/generator/flat/templates"
)

// NewDecoratorProcessor creates a processor for generating source code
func NewDecoratorProcessor() *flatDecoratorProcessor {
	return &flatDecoratorProcessor{}
}

type flatDecoratorProcessor struct{}

func (f flatDecoratorProcessor) GenerateSource(def decorators.Decorator) (string, error) {
	tpl, err := templates.Template(def.Kind())
	if err != nil {
		return "", err
	}
	return templates.Evaluate(tpl, def)
}

func (f flatDecoratorProcessor) GenerateContainerSource(def decorators.Decorator) (string, error) {
	if !def.IsContainerizable() {
		return "", fmt.Errorf("definition %s is not containerizable", def.Name())
	}

	panic("OCF is unsupported yet")
	//return templates.Evaluate(templates.RecordContainerTemplate, def)
}
