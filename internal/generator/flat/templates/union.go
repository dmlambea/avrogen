package templates

const unionTemplate = `
import (
	"fmt"
	"io"
	"reflect"

	"gitlab.com/dmlambea/avrogen/pkg/avro"
)

// {{ .Name }} is a convenience type to hold any of the following supported
// types:
{{- range $i, $child := .Children }}
	{{- if $.OptIdx | ne $i }}
//   - {{ $child.Type }}
	{{- end }}
{{- end }}
// Values are set by calling the Set() method.
// For checking and returning the current union's value type, the following
// methods have been generated:
{{- range $i, $child := .Children }}
	{{- if $.OptIdx | ne $i }}
//   Is{{ $child.Name }}() / As{{ $child.Name }}()
	{{- end }}
{{- end }}
type {{ .Name }} struct {
	index int
	value interface{}
}

// New{{ .Name }} creates a new union of type {{ .Name }}
// holding an initial value of 'value'.
func New{{ .Name }}(value interface{}) {{ .Type }} {
	u := {{ if .IsOpt }}&{{ end }}{{ .Name }}{}
	return u.Set(value)
}

// Set makes this union to have the given value, which must be of one of its
// supported types. This method returns the same receiver it was called on, to
// make it chainable and create expressive code.
func (u *{{ .Name }}) Set(value interface{}) {{ .Type }} {
	switch t := value.(type) {
{{- range $i, $child := .Children }}
	{{- if $.OptIdx | ne $i }}
	case {{ $child.Type }}:
		u.index = {{ $i }}
	{{- end }}
{{- end }}
	default:
		panic(fmt.Sprintf("invalid union value of type %T for {{ .Name }}", t))
	}
	u.value = value
	return {{ if .IsOpt | not }}*{{ end -}}u
}

{{- range $i, $child := .Children }}
	{{- if $.OptIdx | ne $i }}

// Is{{ $child.Name }} return true if this union is currently holding a
// value of type {{ $child.Type }}
func (u *{{ $.Name }}) Is{{ $child.Name }}() bool {
	return (u.index == {{ $i }})
}

// As{{ $child.Name }} is a convenience function that return this union's
// current value, casted to type {{ $child.Type }}.
func (u *{{ $.Name }}) As{{ $child.Name }}() {{ $child.Type }} {
	return u.value.({{$child.Type}})
}
	{{- end }}
{{- end }}

func write{{ .Name }}(u {{ if .IsOpt }}*{{ end }}{{ .Name }}, w io.Writer) (err error) {
	{{- if .IsOpt }}
	if u == nil {
		return avro.WriteLong({{ .OptIdx }}, w)
	}{{- end }}
	if err = avro.WriteLong(int64(u.index), w); err != nil {
		return
	}
	switch u.index {
{{- range $i, $child := .Children }}
	{{- if $.OptIdx | ne $i }}
	case {{ $i }}:
		err = {{ $child.SerializerMethod }}(u.value.({{$child.Type}}), w)
	{{- end }}
{{- end }}
	default:
		panic(fmt.Sprintf("invalid union type %v for {{ .Name }}", u.index))
	}
	return
}

func (u {{ .Name }}) UnionTypes() []reflect.Type {
	return typesFor{{ .Name }}
}

var (
	typesFor{{ .Name }} = []reflect.Type{
{{- range $i, $child := .Children }}
	{{- if $.OptIdx | ne $i }}
		reflect.TypeOf((*{{$child.Type}})(nil)),
	{{- else }}
		reflect.TypeOf(nil),
	{{- end }}
{{- end }}	
	}
)
`
