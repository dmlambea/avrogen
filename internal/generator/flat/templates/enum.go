package templates

const enumTemplate = `
import (
	"fmt"
	"io"

	"gitlab.com/dmlambea/avrogen/pkg/avro"
)

// {{ .Name }} is the type defining type-safe numeric values for the
// list of symbols of the enum type {{ .Name }}.
// Enum values can be assigned by using their constant names, e.g.:
//     myEnum := {{ (index .Children 0 ).Name }}
// Symbol strings can be casted to enum types by using:
//     myEnum, err := {{ .Name  }}FromString("{{ (index .Children 0 ).Type }}")
{{ if ne .Doc "" -}}
// The Avro schema documents this enum with the following:
// {{ .Doc }}
{{ end -}}
type {{ .Name }} int32

const (
	{{- range $i, $sym := .Children }}{{- if ne $i 0 }}
{{ end }}
	// {{ $sym.Name }} is the constant value for this enum's symbol "{{ $sym.Type }}"
	{{ $sym.Name }} {{ $.Name }} = {{ $i }}
	{{- end }}
)

// {{ .Name  }}FromString returns the enum constant value for the given
// symbol, or an error if the symbol is not valid.
func {{ .Name  }}FromString(symbol string) (enum {{ .Name }}, err error) {
	switch symbol {
{{- range $i, $sym := .Children }}
	case "{{ $sym.Type }}":
		enum = {{ $sym.Name }}
{{- end }}
	default:
		err = fmt.Errorf("invalid symbol '%s' for enum {{ .Name }}", symbol)
	}
	return
}

// String returns the symbol string value for this enum constant.
func (e {{ .Name  }}) String() string {
	switch e {
{{- range $i, $sym := .Children }}
	case {{ $sym.Name }}:
		return "{{ $sym.Type }}"
{{- end }}
	}
	panic(fmt.Sprintf("internal error: unexpected constant value %d for enum {{ .Name }}", e))
}

func write{{ .Name }}(e {{ .Name }}, w io.Writer) (err error) {
	return avro.WriteInt(int32(e), w)
}

`
