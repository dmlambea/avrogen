package templates

const arrayTemplate = `
import (
	"io"

	"gitlab.com/dmlambea/avrogen/pkg/avro"
)

// {{ .Name }} is the type defining an Avro array of {{ .Type }}
type {{ .Name }} {{ .Type }}

func write{{ .Name }}(r {{ .Name }}, w io.Writer) (err error) {
	if err = avro.WriteLong(int64(len(r)), w); err != nil || len(r) == 0 {
		return
	}
	for _, elem := range r {

	{{- $f := (index .Children 0) }}
	{{- if $f.IsOptSimple }}
		switch elem == nil {
		case true:
			if err = avro.WriteLong({{ $f.OptIdx }}, w); err != nil {
				return
			}
		default:
			if err = avro.WriteLong({{ $f.NonOptIdx }}, w); err != nil {
				return
			}
			if err = {{ $f.SerializerMethod }}({{ $f.Indirect }}elem, w); err != nil {
				return
			}
		}
	{{- else }}
		if err = {{ $f.SerializerMethod }}(elem, w); err != nil {
			return
		}
	{{- end }}
	}
	return avro.WriteLong(0, w)
}
`
