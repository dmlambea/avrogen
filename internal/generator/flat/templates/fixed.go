package templates

const fixedTemplate = `
import (
	"io"
)

// {{ .Name }} is the type defining a fixed-size array of {{ .Type }}
type {{ .Name }} {{ .Type }}

func write{{ .Name }}(f {{ .Name }}, w io.Writer) error {
	_, err := w.Write(f[:])
	return err
}
`
