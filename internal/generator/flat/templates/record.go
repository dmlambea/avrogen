package templates

const recordTemplate = `
import (
	"fmt"
	"io"

	"gitlab.com/dmlambea/avrogen/pkg/avro"
)

// {{ .Name }} is the struct for holding values of
// the Avro type {{ .Name }}.
{{ if ne .Doc "" -}}
// {{ .Doc }}
{{ end -}}
type {{ .Name }} struct { 
{{- range $field := .Children }}
	{{- if ne $field.Doc "" }}
	// {{ $field.Doc }}
	{{- end }}
	{{ $field.Name }} {{ $field.Type }}
{{- end }}
}

// Deserialize{{ .Name }} consumes as much data from r as needed to
// deserialize an instance of type {{ .Name }} and returns it, or an
// error.
func Deserialize{{ .Name }}(r io.Reader) (t {{ .Name }}, err error) {
	return Deserialize{{ .Name }}FromBinarySchema(r, t.BinarySchema())
}

// Deserialize{{ .Name }}FromBinarySchema deserializes the data from r, which was
// written by a writer using the given schema in binary format.
func Deserialize{{ .Name }}FromBinarySchema(r io.Reader, schema []byte) (t {{ .Name }}, err error) {
	if err = avro.Deserialize(r, schema, &t); err != nil {
		err = fmt.Errorf("error deserializing {{ .Name }}: %v", err)
	}
	return
}

// Deserialize{{ .Name }}FromJSONSchema deserializes the data from r, which was
// written by a writer using the given schema in JSON format.
func Deserialize{{ .Name }}FromJSONSchema(r io.Reader, schema string) (t {{ .Name }}, err error) {
	if err = avro.DeserializeJSON(r, schema, &t); err != nil {
		err = fmt.Errorf("error deserializing {{ .Name }}: %v", err)
	}
	return
}

// Serialize writes this struct's current data to writer w in Avro binary encoding.
func (r {{ .Name }}) Serialize(w io.Writer) error {
	return write{{ .Name }}(r, w)
}

// BinarySchema returns the binary serialized schema used to generate this struct.
func (r {{ .Name }}) BinarySchema() []byte {
	return {{ printf "%v" .BinarySchema }}
}

// JSONSchema returns the JSON schema used to generate this struct.
func (r {{ .Name }}) JSONSchema() string {
	return {{ printf "%v" .JSONSchema }}
}

func write{{ .Name }}(r {{ .Name }}, w io.Writer) (err error) {
{{- range $f := .Children }}	
	{{- if $f.IsOptSimple }}
	if r.{{ $f.Name }} == nil {
		if err = avro.WriteLong({{ $f.OptIdx }}, w); err != nil {
			return
		}
	} else {
		if err = avro.WriteLong({{ $f.NonOptIdx }}, w); err != nil {
			return
		}
		if err = {{ $f.SerializerMethod }}({{ $f.Indirect }}r.{{ $f.Name }}, w); err != nil {
			return
		}
	}
	{{- else }}
	if err = {{ $f.SerializerMethod }}(r.{{ $f.Name }}, w); err != nil {
		return
	}
	{{- end }}
{{- end }}
	return
}
`
