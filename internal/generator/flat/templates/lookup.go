package templates

import (
	"bytes"
	"errors"
	"text/template"

	"gitlab.com/dmlambea/avrogen/internal/decorators"
)

var errNoTemplateForType = errors.New("No template exists for supplied type")

// Template returns the template string for the given kind of decorator, or an
// error if there is no template for such kind.
func Template(kind decorators.Kind) (tpl string, err error) {
	switch kind {
	case decorators.KindArray:
		tpl = arrayTemplate
	case decorators.KindMap:
		tpl = mapTemplate
	case decorators.KindUnion:
		tpl = unionTemplate
	case decorators.KindEnum:
		tpl = enumTemplate
	case decorators.KindFixed:
		tpl = fixedTemplate
	case decorators.KindRecord:
		tpl = recordTemplate
	default:
		err = errNoTemplateForType
	}
	return
}

// Evaluate runs the given template for the object `obj` and returns the
// resulting text, or an error.
func Evaluate(templateStr string, obj interface{}) (string, error) {
	buf := &bytes.Buffer{}
	t, err := template.New("").Parse(templateStr)
	if err != nil {
		return "", err
	}

	err = t.Execute(buf, obj)
	if err != nil {
		return "", err
	}

	return buf.String(), nil
}
