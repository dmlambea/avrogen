package templates

const mapTemplate = `
import (
	"io"

	"gitlab.com/dmlambea/avrogen/pkg/avro"
)

// {{ .Name }} is the type defining an Avro map of {{ (index .Children 0).Type }}
type {{ .Name }} {{ .Type }}

// New{{ .Name }} is a constructor function to create a map of type {{ .Type }}
// This call can be chained with Add() to easily create & initializate a map.
func New{{ .Name }}() {{ .Name }} {
	return make({{ .Name }})
}

func (m {{ .Name }}) Add(key string, val {{ (index .Children 0).Type }}) {{ .Name }} {
	m[key] = val
	return m
}

func write{{ .Name }}(r {{ .Name }}, w io.Writer) (err error) {
	if err = avro.WriteLong(int64(len(r)), w); err != nil || len(r) == 0 {
		return
	}
	for key, val := range r {
		if err = avro.WriteString(key, w); err != nil {
			return err
		}

	{{- $f := (index .Children 0) }}
	{{- if $f.IsOptSimple }}
		switch val == nil {
		case true:
			if err = avro.WriteLong({{ $f.OptIdx }}, w); err != nil {
				return
			}
		default:
			if err = avro.WriteLong({{ $f.NonOptIdx }}, w); err != nil {
				return
			}
			if err = {{ $f.SerializerMethod }}({{ $f.Indirect }}val, w); err != nil {
				return
			}
		}
	{{- else }}
		if err = {{ $f.SerializerMethod }}(val, w); err != nil {
			return
		}
	{{- end }}
	}
	return avro.WriteLong(0, w)
}

`
