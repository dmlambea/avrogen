package generator

import (
	"fmt"

	"gitlab.com/dmlambea/avrogen/internal/decorators"
	"gitlab.com/dmlambea/avrogen/internal/generator/flat"
	"gitlab.com/dmlambea/avrogen/internal/identifiers"
)

// Type defines the code generator type
type Type int

const (
	// TypeFlat is the constant for a flat code generator, i.e., all generated
	// code within the same package.
	TypeFlat Type = iota

	// TypeFlatWithContainers is the constant for generating code all within
	// the same package, along with OCF types.
	TypeFlatWithContainers
)

// Generator is the interace implemented by all code generators
type Generator interface {
	Generate(decorators.Decorator) error
}

// FileWriter is the interface for file writers
type FileWriter interface {
	WriteFile(name, contents string) error
}

// New is a factory method for creating source code generators of the given type
func New(ofType Type, wrt FileWriter) Generator {
	gen := generatorImpl{
		containers: ofType == TypeFlatWithContainers,
		wrt:        wrt,
		proc:       flat.NewDecoratorProcessor(),
	}
	return &gen
}

// decoratorProcessor is the interface that manages decorated types and generates
// source code from them.
type decoratorProcessor interface {
	GenerateSource(def decorators.Decorator) (string, error)
	GenerateContainerSource(def decorators.Decorator) (string, error)
}

type generatorImpl struct {
	containers bool
	wrt        FileWriter
	proc       decoratorProcessor
}

func (g generatorImpl) Generate(def decorators.Decorator) (err error) {
	var src string
	if src, err = g.proc.GenerateSource(def); err != nil {
		return fmt.Errorf("unable to generate source code for %s: %v", def.Name(), err)
	}

	fileName := identifiers.ToSnake(def.Name()) + ".go"
	if err = g.wrt.WriteFile(fileName, src); err != nil {
		return fmt.Errorf("unable to write target file %s: %v", fileName, err)
	}

	if g.containers && def.IsContainerizable() {
		if src, err = g.proc.GenerateContainerSource(def); err != nil {
			return fmt.Errorf("unable to generate container source code for %s: %v", def.Name(), err)
		}
		fileName = identifiers.ToSnake(def.Name()) + "_container.go"
		if err = g.wrt.WriteFile(fileName, src); err != nil {
			return fmt.Errorf("unable to write target file %s: %v", fileName, err)
		}
	}
	return
}
