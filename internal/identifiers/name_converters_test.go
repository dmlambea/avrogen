package identifiers

import "testing"

type nameConverterTestcase struct {
	name     string
	expected string
}

var defaultFixtures = []nameConverterTestcase{
	{"aName", "AName"},
	{"Aname", "Aname"},
	{"a_name", "AName"},
	{"pkg.aname", "Aname"},
	{"com.acme.aname", "Aname"},
}

var nsShortFixtures = []nameConverterTestcase{
	{"aName", "AName"},
	{"Aname", "Aname"},
	{"a_name", "AName"},
	{"pkg.aname", "PkgAname"},
	{"com.acme.aname", "AcmeAname"},
}

var nsFullFixtures = []nameConverterTestcase{
	{"aName", "AName"},
	{"Aname", "Aname"},
	{"a_name", "AName"},
	{"pkg.aname", "PkgAname"},
	{"com.acme.aname", "ComAcmeAname"},
}

func TestDefaultConverter(t *testing.T) {
	n := ConverterForStyle(NamespaceNone)
	for _, f := range defaultFixtures {
		t.Run(f.name, func(t *testing.T) {
			name := n.ToPublicName(f.name)
			if name != f.expected {
				t.Errorf(`ToPublicName("%s") Expected %v, got %v`, f.name, f.expected, name)
			}
		})
	}
}

func TestNamespaceAwareConverterShort(t *testing.T) {
	n := ConverterForStyle(NamespaceShort)
	for _, f := range nsShortFixtures {
		t.Run(f.name, func(t *testing.T) {
			name := n.ToPublicName(f.name)
			if name != f.expected {
				t.Errorf(`ToPublicName("%s") Expected %v, got %v`, f.name, f.expected, name)
			}
		})
	}
}

func TestNamespaceAwareConverterFull(t *testing.T) {
	n := ConverterForStyle(NamespaceFull)
	for _, f := range nsFullFixtures {
		t.Run(f.name, func(t *testing.T) {
			name := n.ToPublicName(f.name)
			if name != f.expected {
				t.Errorf(`ToPublicName("%s") Expected %v, got %v`, f.name, f.expected, name)
			}
		})
	}
}
