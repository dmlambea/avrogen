package identifiers

import (
	"fmt"
	"regexp"
	"strings"
)

// NamespaceStyle defines the namig strategy for a namespace-aware name converter.
type NamespaceStyle string

const (
	// NamespaceNone makes the converter to ignore the namespace at all.
	NamespaceNone NamespaceStyle = "none"

	// NamespaceShort makes the converter to take into account the last part
	// of the namespace when making name convertions.
	NamespaceShort NamespaceStyle = "short"

	// NamespaceFull makes the converter to process the whole namespace of the
	// names.
	NamespaceFull NamespaceStyle = "full"
)

// NamespaceStyleFromString returns the proper constant value for the given
// style name
func NamespaceStyleFromString(style string) (nsStyle NamespaceStyle, err error) {
	validStyles := []NamespaceStyle{NamespaceNone, NamespaceShort, NamespaceFull}
	for i := range validStyles {
		if string(validStyles[i]) == style {
			nsStyle = validStyles[i]
			return
		}
	}
	err = fmt.Errorf("invalid namespace style '%s'", style)
	return
}

// NameConverter is the interface defining an implementation of a naming
// conversion strategy to generate valid Go identifiers from a given string.
type NameConverter interface {
	ToPublicName(name string) string
}

// ConverterForStyle returns a converter for the given style of namespace awareness.
func ConverterForStyle(style NamespaceStyle) nameConverter {
	return nameConverter{style: style}
}

var invalidTokensRegexp = regexp.MustCompile(`[[:^alnum:]]+`)

// nameConverter takes into account the last part/parts of the namespace of a
// fqn, so final names can be fully distinguished in a flat package structure.
type nameConverter struct {
	style NamespaceStyle
}

// ToPublicName implements the go-idiomatic public name as in DefaultNamer's
// struct, but with additional treatment applied in order to remove possible
// invalid tokens from it. Final string is then converted to camel-case.
func (c nameConverter) ToPublicName(name string) string {
	parts := strings.Split(name, ".")

	var useParts int
	switch c.style {
	case NamespaceFull:
		useParts = len(parts)
	case NamespaceShort:
		useParts = 2
	default:
		useParts = 1
	}
	firstPart := len(parts) - useParts
	if firstPart < 0 {
		firstPart = 0
	}
	name = strings.Join(parts[firstPart:], ".")
	name = invalidTokensRegexp.ReplaceAllString(name, " ")
	return strings.Replace(strings.Title(name), " ", "", -1)
}
