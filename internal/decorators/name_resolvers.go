package decorators

import (
	"fmt"
	"strings"

	"gitlab.com/dmlambea/avrogen/pkg/types"
)

type namer interface {
	ToPublicName(name string) string
}

// typeName returns the name a type should receive when using the supplied namer.
func typeName(t types.Generic, n namer) string {
	switch tp := t.(type) {
	case *types.Primitive:
		return n.ToPublicName(tp.Name())
	case *types.Array:
		return fmt.Sprintf("Array%s", typeName(tp.Type(), n))
	case *types.Map:
		return fmt.Sprintf("Map%s", typeName(tp.Type(), n))
	case *types.Enum:
		return n.ToPublicName(tp.Name())
	case *types.Fixed:
		return n.ToPublicName(tp.Name())
	case *types.Record:
		return n.ToPublicName(tp.Name())
	case *types.Union:
		var str strings.Builder
		str.WriteString("Union")
		for _, item := range tp.Children() {
			f := item.(*types.Field)
			str.WriteString(typeName(f.Type(), n))
		}
		return str.String()
	default:
		panic(fmt.Sprintf("Unsupported type %T", t))
	}
}

// typeType returns the Go type a type should receive when using the supplied
// namer. Special cases are arrays and maps, whose types depend on their child
// types. So for arrays and maps, the returned value is the format string
// needed to compose their types during construction.
func typeType(t types.Generic, n namer) string {
	switch tp := t.(type) {
	case *types.Primitive:
		return tp.GoType()
	case *types.Array:
		return "[]%s"
	case *types.Map:
		return "map[string]%s"
	case *types.Enum:
		return typeName(tp, n)
	case *types.Fixed:
		return fmt.Sprintf("[%d]byte", tp.SizeBytes())
	case *types.Record:
		return typeName(tp, n)
	case *types.Union:
		return typeName(tp, n)
	default:
		panic(fmt.Sprintf("Unsupported type %T", t))
	}
}
