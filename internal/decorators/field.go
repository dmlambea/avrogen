package decorators

// fieldDecorator decorates fields by providing only the field namd and doc. All other calls are
// redirected to the decorated field's type.
type fieldDecorator struct {
	inner Decorator
	name  string
	doc   string
}

func (d *fieldDecorator) Name() string {
	return d.name
}

func (d *fieldDecorator) Doc() string {
	return d.doc
}

func (d *fieldDecorator) Type() string {
	if _, ok := d.inner.(*parentDecorator); ok || d.inner.Kind() == KindFixed {
		return d.inner.Name()
	}
	return d.inner.Type()
}

func (d *fieldDecorator) SerializerMethod() string {
	return d.inner.SerializerMethod()
}

func (d *fieldDecorator) IsContainerizable() bool {
	return d.inner.IsContainerizable()
}

func (d *fieldDecorator) Kind() Kind {
	return d.inner.Kind()
}

func (d *fieldDecorator) Schema() interface{} {
	return d.inner.Schema()
}

func (d *fieldDecorator) IsOpt() bool {
	return d.inner.IsOpt()
}

func (d *fieldDecorator) IsOptSimple() bool {
	return d.inner.IsOptSimple()
}

func (d *fieldDecorator) Indirect() string {
	return d.inner.Indirect()
}

func (d *fieldDecorator) OptIdx() int {
	return d.inner.OptIdx()
}

func (d *fieldDecorator) NonOptIdx() int {
	return d.inner.NonOptIdx()
}

func (d *fieldDecorator) Children() []Decorator {
	return d.inner.Children()
}
