package decorators

type enumSymbolDecorator struct {
	nullDecorator
	name   string
	symbol string
}

func (s *enumSymbolDecorator) Name() string {
	return s.name
}

func (s *enumSymbolDecorator) Type() string {
	return s.symbol
}
