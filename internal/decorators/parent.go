package decorators

// parentDecorator is the base type for types having children, like Record, Enum and Unions.
// Enums do not precisely have "children types", but their "symbols" array can be interpreted as
// children for decoration purposes.
type parentDecorator struct {
	basicDecorator
	children []Decorator
	doc      string  // for enums
	schemas  schemas // for record fields
}

type schemas struct {
	binary string
	json   string
}

func (d *parentDecorator) Children() []Decorator {
	return d.children
}

func (d *parentDecorator) Doc() string {
	return d.doc
}

func (d *parentDecorator) BinarySchema() string {
	return d.schemas.binary
}

func (d *parentDecorator) JSONSchema() string {
	return d.schemas.json
}
