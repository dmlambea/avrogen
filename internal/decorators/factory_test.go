package decorators

import (
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dmlambea/avrogen/internal/identifiers"
	"gitlab.com/dmlambea/avrogen/pkg/parser"
)

func TestDecorators(t *testing.T) {
	fixtures := []string{
		"int",
		"long",
		"float",
		"double",
		"string",
		"bool",
		"bytes",
		"array",
		"map",
		"fixed",
		"enum",
		"record",
		"union",
		"complex",
	}
	for i := range fixtures {
		roundtrip(t, fixtures[i])
	}
}

func roundtrip(t *testing.T, name string) {
	schema := goldenLoad(fmt.Sprintf("schema_%s.avsc", name))

	tp, err := parser.ParseSchema(string(schema))
	require.Nil(t, err, fmt.Sprintf("test for %s failed", name))

	d := Decorate(tp, identifiers.ConverterForStyle(identifiers.NamespaceNone))

	var str strings.Builder
	checkDecorator(&str, "", d)
	goldenEquals(t, fmt.Sprintf("schema_%s", name), []byte(str.String()))
}

func checkDecorator(str *strings.Builder, indent string, d Decorator) {
	str.WriteString(fmt.Sprintf("%sName: '%s'\n", indent, d.Name()))
	panicProofCheck(str, indent, "Type", func() interface{} { return d.Type() })
	panicProofCheck(str, indent, "SerializerMethod", func() interface{} { return d.SerializerMethod() })
	indent = fmt.Sprintf("  %s", indent)
	panicProofCheck(str, indent, "Children", func() interface{} { return d.Children() })
	panicProofCheck(str, indent, "Doc", func() interface{} { return d.Doc() })
	panicProofCheck(str, indent, "Schema", func() interface{} { return d.Schema() })
	panicProofCheck(str, indent, "IsOpt", func() interface{} { return d.IsOpt() })
	panicProofCheck(str, indent, "IsOptSimple", func() interface{} { return d.IsOptSimple() })
	panicProofCheck(str, indent, "OptIdx", func() interface{} { return d.OptIdx() })
	panicProofCheck(str, indent, "NonOptIdx", func() interface{} { return d.NonOptIdx() })
}

func panicProofCheck(str *strings.Builder, indent string, name string, fn func() interface{}) {
	defer func() {
		r := recover()
		if r != nil {
			str.WriteString(fmt.Sprintf("'%v'", r))
		}
		str.WriteString("\n")
	}()

	str.WriteString(fmt.Sprintf("%s%s: ", indent, name))
	result := fn()
	switch tp := result.(type) {
	case []Decorator:
		str.WriteString("\n")
		indent = fmt.Sprintf("  %s", indent)
		for _, d := range tp {
			checkDecorator(str, indent, d)
		}
	default:
		str.WriteString(fmt.Sprintf("'%v'", result))
	}
}
