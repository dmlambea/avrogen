package decorators

// nullDecorator is the decorator implementing defaults or panics for all interface methods. This is the
// base for the rest of the decorators, so they don't need to provide empty methods for non-applicable calls.
// The only default calls implemented by this decorator are IsOpt and IsOptSimple. Those calls are only
// overriden by optional unions, so other types/fields are never optional.
type nullDecorator struct{}

func (d *nullDecorator) IsOpt() bool {
	return false
}

func (d *nullDecorator) IsOptSimple() bool {
	return false
}

func (d *nullDecorator) Indirect() string {
	return ""
}

func (d *nullDecorator) Name() string {
	panic("Name() not implemented!")
}

func (d *nullDecorator) Type() string {
	panic("Type() not implemented!")
}

func (d *nullDecorator) SerializerMethod() string {
	panic("SerializerMethod() not implemented!")
}

func (d *nullDecorator) IsContainerizable() bool {
	panic("Containerizable() not implemented!")
}

func (d *nullDecorator) Kind() Kind {
	panic("Kind() not implemented!")
}

func (d *nullDecorator) Doc() string {
	panic("Doc() not implemented!")
}

func (d *nullDecorator) Schema() interface{} {
	panic("Schema() not implemented!")
}

func (d *nullDecorator) OptIdx() int {
	panic("OptIdx() not implemented!")
}

func (d *nullDecorator) NonOptIdx() int {
	panic("NonOptIdx() not implemented!")
}

func (d *nullDecorator) Children() []Decorator {
	panic("Children() not implemented!")
}
