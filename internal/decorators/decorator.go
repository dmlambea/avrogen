package decorators

// Kind holds the kind of schema type a decorator decorates
type Kind int

const (
	// KindPrimitive is the constant value for decorators of type Primitive
	KindPrimitive Kind = iota

	// KindArray is the constant value for decorators of type Array
	KindArray

	// KindEnum is the constant value for decorators of type Enum
	KindEnum

	// KindFixed is the constant value for decorators of type Fixed
	KindFixed

	// KindMap is the constant value for decorators of type Map
	KindMap

	// KindRecord is the constant value for decorators of type Record
	KindRecord

	// KindUnion is the constant value for decorators of type Union
	KindUnion
)

// Decorator is the interface used to mask the schema types, so that their info
// can be preprocessed prior to generating the code. This way, the schema information
// is not tainted with Go-language details.
type Decorator interface {
	Name() string
	Kind() Kind
	Type() string
	SerializerMethod() string
	IsContainerizable() bool
	Children() []Decorator
	Doc() string
	Schema() interface{}
	IsOpt() bool
	IsOptSimple() bool
	Indirect() string
	OptIdx() int
	NonOptIdx() int
}
