package decorators

type basicDecorator struct {
	nullDecorator
	name             string
	typeName         string
	serializerMethod string
	containerizable  bool
	kind             Kind
}

func (d *basicDecorator) Name() string {
	return d.name
}

func (d *basicDecorator) Type() string {
	return d.typeName
}

func (d *basicDecorator) SerializerMethod() string {
	return d.serializerMethod
}

func (d *basicDecorator) IsContainerizable() bool {
	return d.containerizable
}

func (d *basicDecorator) Kind() Kind {
	return d.kind
}
