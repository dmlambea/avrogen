package decorators

type unionDecorator struct {
	basicDecorator
	children    []Decorator
	isOpt       bool
	isOptSimple bool
	indirect    string
	optIndex    int
	nonOptIndex int
}

func (d *unionDecorator) Children() []Decorator {
	return d.children
}

func (d *unionDecorator) IsOpt() bool {
	return d.isOpt
}

func (d *unionDecorator) IsOptSimple() bool {
	return d.isOptSimple
}

func (d *unionDecorator) Indirect() string {
	return d.indirect
}

func (d *unionDecorator) OptIdx() int {
	return d.optIndex
}

func (d *unionDecorator) NonOptIdx() int {
	if !d.IsOptSimple() {
		panic("Undefined non-opt index: not a single-typed, optional union")
	}
	return d.nonOptIndex
}
