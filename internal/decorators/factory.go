package decorators

import (
	"fmt"
	"strings"

	"gitlab.com/dmlambea/avrogen/pkg/types"
)

// Decorate returns a decorator/decorator tree for the given type.
func Decorate(t types.Generic, n namer) Decorator {
	d := decoratorFactory{
		n: n,
		v: make(map[types.Generic]Decorator),
	}
	return d.decorate(t)
}

// decoratorFactory is the struct holding the decoration process state: naming
// strategy and visit map.
type decoratorFactory struct {
	n namer
	v map[types.Generic]Decorator
}

// decorate returns a decorator for the given type, taking into account the
// already decorated types to avoid cycles.
func (d *decoratorFactory) decorate(t types.Generic) (dec Decorator) {
	// Recursion protection
	var ok bool
	if dec, ok = d.v[t]; ok {
		return
	}

	switch tp := t.(type) {
	case *types.Array:
		return d.newSingleChildDecorator(t, KindArray)
	case *types.Enum:
		return d.newEnumDecorator(tp)
	case *types.Field:
		return d.newFieldDecorator(tp)
	case *types.Fixed:
		return d.newBasicDecorator(tp, KindFixed)
	case *types.Map:
		return d.newSingleChildDecorator(t, KindMap)
	case *types.Record:
		return d.newRecordDecorator(tp)
	case *types.Union:
		return d.newUnionDecorator(tp)
	default:
		return d.newBasicDecorator(tp, KindPrimitive)
	}
}

// newBasicDecorator returns a decorator that fulfills a limited, basic subset of the decorator
// interface method set. Only the Name(), Type() and SerializerMethod() methods are implemented.
// Other methods of the interface panic.
func (d *decoratorFactory) newBasicDecorator(t types.Generic, kind Kind) *basicDecorator {
	b := basicDecorator{
		name:     typeName(t, d.n),
		typeName: typeType(t, d.n),
		kind:     kind,
	}
	d.v[t] = &b

	switch kind {
	case KindPrimitive:
		b.serializerMethod = "avro.WritePrimitive"
	default:
		b.serializerMethod = fmt.Sprintf("write%s", b.name)
	}

	return &b
}

// newEnumDecorator returns a decorator for the given enum type.
func (d *decoratorFactory) newEnumDecorator(t *types.Enum) *parentDecorator {
	b := d.newBasicDecorator(t, KindEnum)
	p := parentDecorator{basicDecorator: *b, doc: t.Doc()}
	d.v[t] = &p

	p.children = make([]Decorator, len(t.Symbols()))
	for i, s := range t.Symbols() {
		p.children[i] = &enumSymbolDecorator{
			name:   d.n.ToPublicName(fmt.Sprintf("%s.%s", t.Name(), s)),
			symbol: s,
		}
	}
	return &p
}

// newFieldDecorator returns a field decorator for the given field type
func (d *decoratorFactory) newFieldDecorator(field *types.Field) *fieldDecorator {
	f := fieldDecorator{
		inner: d.decorate(field.Type()),
	}
	f.name = d.n.ToPublicName(field.Name())
	if f.name == "" {
		f.name = f.inner.Name()
	}
	f.doc = field.Doc()
	return &f
}

// newRecordDecorator returns a decorator for the given record type.
func (d *decoratorFactory) newRecordDecorator(t *types.Record) *parentDecorator {
	b := d.newBasicDecorator(t, KindRecord)
	b.containerizable = true
	schemas := schemas{
		binary: goSourcecodeForByteSlice(t.BinarySchema()),
		json:   fmt.Sprintf("`%s`", t.JSONSchema()),
	}
	p := parentDecorator{
		basicDecorator: *b,
		doc:            t.Doc(),
		schemas:        schemas,
	}
	d.v[t] = &p

	p.children = make([]Decorator, len(t.Children()))
	for i, c := range t.Children() {
		f := c.(*types.Field)
		p.children[i] = d.decorate(f)
	}
	return &p
}

// newSingleChildDecorator returns a decorator for the given array or map.
func (d *decoratorFactory) newSingleChildDecorator(t types.Generic, kind Kind) *parentDecorator {
	b := d.newBasicDecorator(t, kind)
	p := parentDecorator{basicDecorator: *b}
	d.v[t] = &p

	p.children = make([]Decorator, 1)
	p.children[0] = d.decorate(t.(types.SingleChild).Type())
	// Fix typeName, since the factory namer gives format strings to maps and
	// arrays, instead of proper type names
	p.typeName = fmt.Sprintf(p.typeName, p.children[0].Type())
	return &p
}

// newUnionDecorator returns a decorator for the given union type.
func (d *decoratorFactory) newUnionDecorator(t *types.Union) *unionDecorator {
	b := d.newBasicDecorator(t, KindUnion)
	u := unionDecorator{
		basicDecorator: *b,
		isOpt:          t.IsOptional(),
		isOptSimple:    t.IsOptionalSimple(),
		optIndex:       t.OptionalIndex(),
		nonOptIndex:    t.NonOptionalIndex(),
	}
	d.v[t] = &u

	u.children = make([]Decorator, len(t.Children()))
	for i, c := range t.Children() {
		f := c.(*types.Field)
		u.children[i] = d.decorate(f)
	}

	switch {
	case u.isOptSimple:
		childType := t.Children()[u.nonOptIndex].(*types.Field).Type()
		if _, isArrayOrMap := childType.(types.SingleChild); !isArrayOrMap {
			if p, ok := childType.(*types.Primitive); !ok || p.Name() != "bytes" {
				u.indirect = "*"
			}
		}
		var str strings.Builder
		str.WriteString(u.indirect)
		str.WriteString(u.children[u.nonOptIndex].Type())
		u.basicDecorator.typeName = str.String()
		u.basicDecorator.serializerMethod = u.children[u.nonOptIndex].SerializerMethod()
	case u.isOpt:
		u.indirect = "*"
		u.basicDecorator.typeName = fmt.Sprintf("*%s", u.basicDecorator.typeName)
	}
	return &u
}

func goSourcecodeForByteSlice(b []byte) string {
	var str strings.Builder
	str.WriteString("[]byte{")
	hexStr := fmt.Sprintf("% #x", b)
	str.WriteString(strings.ReplaceAll(hexStr, " ", ", "))
	str.WriteString("}")
	return str.String()
}
