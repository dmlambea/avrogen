package main

import (
	"errors"
	"fmt"
	"path/filepath"
	"strings"

	"github.com/urfave/cli"
	"gitlab.com/dmlambea/avrogen/internal/identifiers"
)

const (
	defaultPackageName    = ""
	defaultNamespaceStyle = "none"
)

type config struct {
	packageName    string
	namespaceStyle identifiers.NamespaceStyle
	files          []string
	targetDir      string
}

// createCommandLineOptions returns the []Flag array of the accepted cmdline
// parameters.
func createCommandLineOptions(cfg *config) []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:        "package, p",
			Usage:       "Use `NAME` as the package name for the generated code. If not set, the name of the package will be deduced from the target directory's name.",
			Destination: &cfg.packageName,
			Value:       defaultPackageName,
		},
		&cli.StringFlag{
			Name: "namespaces, n",
			Usage: fmt.Sprintf("Identifiers will be generated using the `STYLE` naming strategy for namespaced elements. "+
				" Accepted values are %s, %s and %s", identifiers.NamespaceNone, identifiers.NamespaceShort, identifiers.NamespaceFull),
			Value: defaultNamespaceStyle,
		},
	}
}

// checkConfiguration checks that the cmdline if Ok to proceed; otherwise it
// returns an error.
func checkConfiguration(cfg *config, c *cli.Context) (err error) {
	nsStyle := strings.ToLower(c.String("namespaces"))
	if cfg.namespaceStyle, err = identifiers.NamespaceStyleFromString(nsStyle); err != nil {
		return
	}

	fileArgs := len(c.Args())
	switch fileArgs {
	case 0:
		return errors.New("wrong number of arguments")
	case 1:
		cfg.targetDir = "."
		fileArgs = 0
	default:
		cfg.targetDir = c.Args()[0]
		fileArgs = 1
	}

	cfg.files = make([]string, 0)
	for _, glob := range c.Args()[fileArgs:] {
		files, err := filepath.Glob(glob)
		if err != nil {
			return fmt.Errorf("error parsing input file as glob: %v", err)
		}
		cfg.files = append(cfg.files, files...)
	}
	return
}
