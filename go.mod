module gitlab.com/dmlambea/avrogen

go 1.12

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.1 // indirect
	github.com/json-iterator/go v1.1.12
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/urfave/cli v1.22.5
)
