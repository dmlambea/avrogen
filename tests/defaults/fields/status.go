// Code generated by gitlab.com/dmlambea/avrogen v0.0-testing
//
// This code was generated from source AVSC file: base.avsc
package base

import (
	"fmt"
	"io"

	"gitlab.com/dmlambea/avrogen/pkg/avro"
)

// Status is the type defining type-safe numeric values for the
// list of symbols of the enum type Status.
// Enum values can be assigned by using their constant names, e.g.:
//     myEnum := A
// Symbol strings can be casted to enum types by using:
//     myEnum, err := StatusFromString("a")
type Status int32

const (
	// A is the constant value for this enum's symbol "a"
	A Status = 0

	// B is the constant value for this enum's symbol "b"
	B Status = 1

	// C is the constant value for this enum's symbol "c"
	C Status = 2
)

// StatusFromString returns the enum constant value for the given
// symbol, or an error if the symbol is not valid.
func StatusFromString(symbol string) (enum Status, err error) {
	switch symbol {
	case "a":
		enum = A
	case "b":
		enum = B
	case "c":
		enum = C
	default:
		err = fmt.Errorf("invalid symbol '%s' for enum Status", symbol)
	}
	return
}

// String returns the symbol string value for this enum constant.
func (e Status) String() string {
	switch e {
	case A:
		return "a"
	case B:
		return "b"
	case C:
		return "c"
	}
	panic(fmt.Sprintf("internal error: unexpected constant value %d for enum Status", e))
}

func writeStatus(e Status, w io.Writer) (err error) {
	return avro.WriteInt(int32(e), w)
}

