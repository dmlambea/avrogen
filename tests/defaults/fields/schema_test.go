package base

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/dmlambea/avrogen/tests/defaults/fields/evol"
)

func TestDefaultEvolutions(t *testing.T) {
	// Test with valid, common enum value
	obj := Record{
		IntField:  42,
		LongField: 84,
		EnumField: A,
	}
	roundtrip(t, obj, evol.A)

	// Test with invalid enum value for evolved type
	obj.EnumField = B
	roundtrip(t, obj, evol.D)
}

func roundtrip(t *testing.T, obj Record, expectedEnumValue evol.Status) {
	var buf bytes.Buffer
	require.Nil(t, obj.Serialize(&buf))

	obj2, err := evol.DeserializeRecordFromBinarySchema(&buf, obj.BinarySchema())
	require.Nil(t, err)

	require.Equal(t, obj.IntField, obj2.IntField)
	require.Equal(t, obj.LongField, obj2.LongField)
	require.Equal(t, "ok", obj2.StringField)
	assert.Equal(t, expectedEnumValue, obj2.EnumField)
	assert.Equal(t, true, obj2.RecordField.BoolField)
	assert.Equal(t, evol.MacAddress{0x31, 0x32, 0x33, 0x34, 0x35, 0x36}, obj2.RecordField.FixedField)
}
