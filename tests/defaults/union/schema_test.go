package base

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/dmlambea/avrogen/tests/defaults/union/evol"
)

func TestEvolution(t *testing.T) {
	var oldUnionRecord UnionRecord
	var buf bytes.Buffer
	err := oldUnionRecord.Serialize(&buf)
	require.Nil(t, err)

	newUnionRecord, err := evol.DeserializeUnionRecordFromBinarySchema(&buf, oldUnionRecord.BinarySchema())
	require.Nil(t, err)

	assert.Equal(t, true, newUnionRecord.UnionNull == nil)
	assert.True(t, newUnionRecord.UnionString.IsString())
	assert.Equal(t, "hello", newUnionRecord.UnionString.AsString())
	assert.True(t, newUnionRecord.UnionLong.IsLong())
	assert.Equal(t, int64(42), newUnionRecord.UnionLong.AsLong())
}
