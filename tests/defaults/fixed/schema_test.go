package base

import (
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestDefault(t *testing.T) {
	expected := FixedDefaultTestRecord{
		FixedField: [12]byte{0, 1, 18, 0, 19, 67, 0, 1, 18, 0, 19, 83},
	}

	fixedDefault, err := DeserializeFixedDefaultTestRecordFromJSONSchema((io.Reader)(nil), `{"name":"EmptyRecord","type":"record","fields":[]}`)
	require.Nil(t, err)

	assert.Equal(t, expected, fixedDefault, "Comparing default value")
}
