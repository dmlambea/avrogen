package base

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"testing"

	"github.com/linkedin/goavro/v2"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

// Round-trip some primitive values through our serializer and goavro to verify
const fixtureJson = `
[
  {
    "MapOfMaps": {
      "NestedMap": {
        "NestedArray": ["some","strings"],
        "NestedArray1": ["other","values"]
      }
    }
  },
  {
    "MapOfMaps": {
      "NestedMap": {
         "NestedArray": ["some","strings"],
         "NestedArray1": ["other","values"]
      }
    }
  },
  {
    "MapOfMaps": {
      "NestedMap": {
        "NestedArray": ["some","strings"],
        "NestedArray1": ["other","values"]
      }
    }
  }
]
`

func BenchmarkMapOfMapsRecord(b *testing.B) {
	buf := new(bytes.Buffer)
	record := NestedMap{
		map[string]map[string][]string{
			"key1": map[string][]string{
				"array1": {"value1", "value2"},
				"array2": {"value3", "value4"},
			},
			"key2": map[string][]string{
				"array3": {"value5"},
				"array4": {},
			},
		},
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		err := record.Serialize(buf)
		require.Nil(b, err)
	}
}

func BenchmarkMapOfMapsGoavro(b *testing.B) {
	schemaJSON, err := ioutil.ReadFile("base.avsc")
	require.Nil(b, err)

	codec, err := goavro.NewCodec(string(schemaJSON))
	require.Nil(b, err)

	someRecord := map[string]interface{}{
		"MapOfMaps": map[string]interface{}{
			"key1": map[string][]string{
				"array1": {"value1", "value2"},
				"array2": {"value3", "value4"},
			},
			"key2": map[string][]string{
				"array3": {"value5"},
				"array4": {},
			},
		},
	}
	buf := make([]byte, 0, 1024)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err := codec.BinaryFromNative(buf, someRecord)
		require.Nil(b, err)
	}
}

func TestRoundTrip(t *testing.T) {
	fixtures := make([]NestedMap, 0)
	err := json.Unmarshal([]byte(fixtureJson), &fixtures)
	require.Nil(t, err)

	var buf bytes.Buffer
	for _, f := range fixtures {
		buf.Reset()
		err = f.Serialize(&buf)
		require.Nil(t, err)

		datum, err := DeserializeNestedMap(&buf)
		require.Nil(t, err)
		assert.Equal(t, datum.MapOfMaps, f.MapOfMaps)
	}
}
