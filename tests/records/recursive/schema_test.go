package base

import (
	"bytes"
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

// Round-trip some primitive values through our serializer and goavro to verify
const fixtureJSON = `
[
	{"RecursiveField": null},
	{"RecursiveField": {"RecursiveUnionTestRecord":{"RecursiveField": null}}}
]
`

func TestRoundTrip(t *testing.T) {
	fixtures := make([]RecursiveUnionTestRecord, 0)
	err := json.Unmarshal([]byte(fixtureJSON), &fixtures)
	require.Nil(t, err)

	var buf bytes.Buffer
	for _, f := range fixtures {
		buf.Reset()
		err = f.Serialize(&buf)
		require.Nil(t, err)

		datum, err := DeserializeRecursiveUnionTestRecord(&buf)
		require.Nil(t, err)

		assert.Equal(t, datum, f)
	}
}
