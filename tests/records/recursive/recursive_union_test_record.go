// Code generated by gitlab.com/dmlambea/avrogen v0.0-testing
//
// This code was generated from source AVSC file: base.avsc
package base

import (
	"fmt"
	"io"

	"gitlab.com/dmlambea/avrogen/pkg/avro"
)

// RecursiveUnionTestRecord is the struct for holding values of
// the Avro type RecursiveUnionTestRecord.
type RecursiveUnionTestRecord struct {
	RecursiveField *RecursiveUnionTestRecord
}

// DeserializeRecursiveUnionTestRecord consumes as much data from r as needed to
// deserialize an instance of type RecursiveUnionTestRecord and returns it, or an
// error.
func DeserializeRecursiveUnionTestRecord(r io.Reader) (t RecursiveUnionTestRecord, err error) {
	return DeserializeRecursiveUnionTestRecordFromBinarySchema(r, t.BinarySchema())
}

// DeserializeRecursiveUnionTestRecordFromBinarySchema deserializes the data from r, which was
// written by a writer using the given schema in binary format.
func DeserializeRecursiveUnionTestRecordFromBinarySchema(r io.Reader, schema []byte) (t RecursiveUnionTestRecord, err error) {
	if err = avro.Deserialize(r, schema, &t); err != nil {
		err = fmt.Errorf("error deserializing RecursiveUnionTestRecord: %v", err)
	}
	return
}

// DeserializeRecursiveUnionTestRecordFromJSONSchema deserializes the data from r, which was
// written by a writer using the given schema in JSON format.
func DeserializeRecursiveUnionTestRecordFromJSONSchema(r io.Reader, schema string) (t RecursiveUnionTestRecord, err error) {
	if err = avro.DeserializeJSON(r, schema, &t); err != nil {
		err = fmt.Errorf("error deserializing RecursiveUnionTestRecord: %v", err)
	}
	return
}

// Serialize writes this struct's current data to writer w in Avro binary encoding.
func (r RecursiveUnionTestRecord) Serialize(w io.Writer) error {
	return writeRecursiveUnionTestRecord(r, w)
}

// BinarySchema returns the binary serialized schema used to generate this struct.
func (r RecursiveUnionTestRecord) BinarySchema() []byte {
	return []byte{0x0b, 0x00, 0x18, 0x00, 0x52, 0x65, 0x63, 0x75, 0x72, 0x73, 0x69, 0x76, 0x65, 0x55, 0x6e, 0x69, 0x6f, 0x6e, 0x54, 0x65, 0x73, 0x74, 0x52, 0x65, 0x63, 0x6f, 0x72, 0x64, 0x00, 0x01, 0x00, 0x0e, 0x00, 0x52, 0x65, 0x63, 0x75, 0x72, 0x73, 0x69, 0x76, 0x65, 0x46, 0x69, 0x65, 0x6c, 0x64, 0x0e, 0x00, 0x02, 0x01, 0x00, 0x0f, 0x00, 0x10}
}

// JSONSchema returns the JSON schema used to generate this struct.
func (r RecursiveUnionTestRecord) JSONSchema() string {
	return `{"fields":[{"name":"RecursiveField","type":["null","RecursiveUnionTestRecord"]}],"name":"RecursiveUnionTestRecord","type":"record"}`
}

func writeRecursiveUnionTestRecord(r RecursiveUnionTestRecord, w io.Writer) (err error) {
	if r.RecursiveField == nil {
		if err = avro.WriteLong(0, w); err != nil {
			return
		}
	} else {
		if err = avro.WriteLong(1, w); err != nil {
			return
		}
		if err = writeRecursiveUnionTestRecord(*r.RecursiveField, w); err != nil {
			return
		}
	}
	return
}
