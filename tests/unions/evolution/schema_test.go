package base

import (
	"bytes"
	"testing"

	"gitlab.com/dmlambea/avrogen/tests/unions/evolution/evol"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var (
	testID   int32  = 1
	testName string = "abcd"
)

func TestEvolution(t *testing.T) {
	oldUnionRecord := UnionRecord{
		A:    "hi",
		Id:   &testID,
		Name: &testName,
	}

	var buf bytes.Buffer
	require.Nil(t, oldUnionRecord.Serialize(&buf))

	newUnionRecord, err := evol.DeserializeUnionRecordFromBinarySchema(&buf, oldUnionRecord.BinarySchema())
	require.Nil(t, err)
	require.NotNil(t, newUnionRecord.A)
	assert.Equal(t, oldUnionRecord.A, *newUnionRecord.A)
	assert.Equal(t, *oldUnionRecord.Name, newUnionRecord.Name)
}
