package base

import (
	"bytes"
	"io/ioutil"
	"testing"

	"github.com/linkedin/goavro/v2"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

// Returns a slice of ComplexUnionTestRecord's, initialized with all supported
// values
func getSomeComplexUnionTestRecordFixtures() []ComplexUnionTestRecord {
	fixtures := []ComplexUnionTestRecord{}

	// Nil union type
	fixtures = append(fixtures, ComplexUnionTestRecord{})

	// Int union type
	fixtures = append(fixtures, ComplexUnionTestRecord{NewUnionNullIntArrayIntMapIntNestedUnionRecord(int32(42))})

	// Array union type
	a := ArrayInt{1, 2, 3}
	fixtures = append(fixtures, ComplexUnionTestRecord{NewUnionNullIntArrayIntMapIntNestedUnionRecord(a)})

	// Map union type
	m := NewMapInt()
	m["a"] = 1
	m["b"] = 3
	m["c"] = 5
	fixtures = append(fixtures, ComplexUnionTestRecord{NewUnionNullIntArrayIntMapIntNestedUnionRecord(m)})

	// Record union type
	r := NestedUnionRecord{789}
	fixtures = append(fixtures, ComplexUnionTestRecord{NewUnionNullIntArrayIntMapIntNestedUnionRecord(r)})

	return fixtures
}

func TestPrimitiveUnionFixture(t *testing.T) {
	fixtures := getSomeComplexUnionTestRecordFixtures()

	schemaJson, err := ioutil.ReadFile("base.avsc")
	assert.Nil(t, err)

	codec, err := goavro.NewCodec(string(schemaJson))
	assert.Nil(t, err)

	var buf bytes.Buffer
	for _, f := range fixtures {
		buf.Reset()
		err = f.Serialize(&buf)
		assert.Nil(t, err)

		datum, _, err := codec.NativeFromBinary(buf.Bytes())
		assert.Nil(t, err)

		record := datum.(map[string]interface{})
		recordField, ok := record["UnionField"]
		assert.Equal(t, true, ok)

		if f.UnionField == nil {
			if recordField != nil {
				t.Fatalf("Expected nil value")
			}
			continue
		}

		switch {
		case f.UnionField.IsArrayInt():
			arr := recordField.(map[string]interface{})["array"].([]interface{})
			unionArr := f.UnionField.AsArrayInt()
			for i, v := range arr {
				if v.(int32) != unionArr[i] {
					t.Fatalf("Expected int value %v for union field, got %v", unionArr[i], v)
				}
			}
		case f.UnionField.IsMapInt():
			m := recordField.(map[string]interface{})["map"].(map[string]interface{})
			unionMap := f.UnionField.AsMapInt()
			for k, v := range m {
				if v.(int32) != unionMap[k] {
					t.Fatalf("Expected int value %v for union map key %v field, got %v", unionMap[k], k, v)
				}
			}
		case f.UnionField.IsNestedUnionRecord():
			v, ok := recordField.(map[string]interface{})["NestedUnionRecord"].(map[string]interface{})["IntField"]
			if !ok {
				t.Fatalf("GOT: %#v; WANT: %#v", ok, true)
			}
			unionRec := f.UnionField.AsNestedUnionRecord()
			if v.(int32) != unionRec.IntField {
				t.Fatalf("Expected int value %v for nested record in union, got %v", unionRec.IntField, v)
			}
		}
	}
}

func TestRoundTrip(t *testing.T) {
	fixtures := getSomeComplexUnionTestRecordFixtures()
	for _, f := range fixtures {
		var buf bytes.Buffer
		assert.Nil(t, f.Serialize(&buf))

		datum, err := DeserializeComplexUnionTestRecord(&buf)
		require.Nil(t, err)
		if f.UnionField == nil {
			assert.Equal(t, f.UnionField, datum.UnionField)
		} else {
			require.NotNil(t, datum.UnionField)
			switch {
			case f.UnionField.IsInt():
				assert.Equal(t, f.UnionField.AsInt(), datum.UnionField.AsInt())
			case f.UnionField.IsArrayInt():
				assert.Equal(t, f.UnionField.AsArrayInt(), datum.UnionField.AsArrayInt())
			case f.UnionField.IsMapInt():
				assert.Equal(t, f.UnionField.AsMapInt(), datum.UnionField.AsMapInt())
			case f.UnionField.IsNestedUnionRecord():
				assert.Equal(t, f.UnionField.AsNestedUnionRecord(), datum.UnionField.AsNestedUnionRecord())
			default:
				t.Error("invalid union type")
			}
		}
		assert.Equal(t, f, datum)
	}
}
