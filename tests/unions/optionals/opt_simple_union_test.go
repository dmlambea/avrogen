package base

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestOptionalSimpleUnion(t *testing.T) {
	var val int32
	for val = -3; val < 4; val++ {
		var buf bytes.Buffer
		var obj OptionalSimpleUnionRecord

		// Test as nil
		obj.Amount = nil
		require.Nil(t, obj.Serialize(&buf))

		obj2, err := DeserializeOptionalSimpleUnionRecord(&buf)
		require.Nil(t, err)
		assert.Equal(t, obj, obj2)

		// Test as int32 valid pointer
		obj.Amount = &val
		err = obj.Serialize(&buf)
		require.Nil(t, err)

		obj2, err = DeserializeOptionalSimpleUnionRecord(&buf)
		require.Nil(t, err)
		assert.Equal(t, obj, obj2)
	}
}
