package base

import (
	"bytes"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestOptionalComplexUnion(t *testing.T) {
	for val := -3; val < 4; val++ {
		var buf bytes.Buffer
		var obj OptionalComplexUnionRecord

		// Test as nil
		obj.Amount = nil
		require.Nil(t, obj.Serialize(&buf))

		obj2, err := DeserializeOptionalComplexUnionRecord(&buf)
		require.Nil(t, err)
		assert.Equal(t, obj, obj2)

		// Test as int32 union type
		obj.Amount = NewUnionNullIntString(int32(val))
		err = obj.Serialize(&buf)
		require.Nil(t, err)

		obj2, err = DeserializeOptionalComplexUnionRecord(&buf)
		require.Nil(t, err)
		assert.Equal(t, obj, obj2)

		// Test as string union type
		obj.Amount = NewUnionNullIntString(fmt.Sprintf("string val %d", val))
		err = obj.Serialize(&buf)
		require.Nil(t, err)

		obj2, err = DeserializeOptionalComplexUnionRecord(&buf)
		require.Nil(t, err)
		assert.Equal(t, obj, obj2)
	}
}
