package base

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestNonOptionalUnion(t *testing.T) {
	for val := -3; val < 4; val++ {
		var obj, obj2 NonOptionalUnionRecord
		var buf bytes.Buffer

		// Test as int32
		obj.Amount.Set(int32(val))
		require.Nil(t, obj.Serialize(&buf))

		obj2, err := DeserializeNonOptionalUnionRecord(&buf)
		require.Nil(t, err)
		assert.Equal(t, obj, obj2)

		// Test as float32
		obj.Amount.Set(float32(val))
		err = obj.Serialize(&buf)
		require.Nil(t, err)

		obj2, err = DeserializeNonOptionalUnionRecord(&buf)
		require.Nil(t, err)
		assert.Equal(t, obj, obj2)
	}
}
