// Code generated by gitlab.com/dmlambea/avrogen v0.0-testing
//
// This code was generated from source AVSC file: base.avsc
package base

import (
	"fmt"
	"io"

	"gitlab.com/dmlambea/avrogen/pkg/avro"
)

// IntHolder is the struct for holding values of
// the Avro type IntHolder.
type IntHolder struct {
	AInt int32
}

// DeserializeIntHolder consumes as much data from r as needed to
// deserialize an instance of type IntHolder and returns it, or an
// error.
func DeserializeIntHolder(r io.Reader) (t IntHolder, err error) {
	return DeserializeIntHolderFromBinarySchema(r, t.BinarySchema())
}

// DeserializeIntHolderFromBinarySchema deserializes the data from r, which was
// written by a writer using the given schema in binary format.
func DeserializeIntHolderFromBinarySchema(r io.Reader, schema []byte) (t IntHolder, err error) {
	if err = avro.Deserialize(r, schema, &t); err != nil {
		err = fmt.Errorf("error deserializing IntHolder: %v", err)
	}
	return
}

// DeserializeIntHolderFromJSONSchema deserializes the data from r, which was
// written by a writer using the given schema in JSON format.
func DeserializeIntHolderFromJSONSchema(r io.Reader, schema string) (t IntHolder, err error) {
	if err = avro.DeserializeJSON(r, schema, &t); err != nil {
		err = fmt.Errorf("error deserializing IntHolder: %v", err)
	}
	return
}

// Serialize writes this struct's current data to writer w in Avro binary encoding.
func (r IntHolder) Serialize(w io.Writer) error {
	return writeIntHolder(r, w)
}

// BinarySchema returns the binary serialized schema used to generate this struct.
func (r IntHolder) BinarySchema() []byte {
	return []byte{0x0b, 0x00, 0x09, 0x00, 0x69, 0x6e, 0x74, 0x48, 0x6f, 0x6c, 0x64, 0x65, 0x72, 0x00, 0x01, 0x00, 0x04, 0x00, 0x61, 0x49, 0x6e, 0x74, 0x03, 0x00, 0x10}
}

// JSONSchema returns the JSON schema used to generate this struct.
func (r IntHolder) JSONSchema() string {
	return `{"fields":[{"name":"aInt","type":"int"}],"name":"intHolder","type":"record"}`
}

func writeIntHolder(r IntHolder, w io.Writer) (err error) {
	if err = avro.WritePrimitive(r.AInt, w); err != nil {
		return
	}
	return
}
