package base

import (
	"bytes"
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const fixtureJson = `
[
  {"IntField": [1, null, 2147483647]}
]
`

func TestRoundTrip(t *testing.T) {
	fixtures := make([]ArrayTestRecord, 0)
	err := json.Unmarshal([]byte(fixtureJson), &fixtures)
	require.Nil(t, err)

	var buf bytes.Buffer
	for _, f := range fixtures {
		buf.Reset()
		err = f.Serialize(&buf)
		require.Nil(t, err)

		datum, err := DeserializeArrayTestRecord(&buf)
		require.Nil(t, err)

		assert.Equal(t, f, datum)
	}
}
