package base

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"testing"

	"github.com/linkedin/goavro/v2"
	"github.com/stretchr/testify/assert"
)

const fixtureJson = `
[
	{"EnumArrayField": []},
	{"EnumArrayField": [0]},
	{"EnumArrayField": [1]},
	{"EnumArrayField": [1, 0, 1]}
]
`

func TestArrayFixture(t *testing.T) {
	fixtures := make([]ArrayEnumRecord, 0)
	err := json.Unmarshal([]byte(fixtureJson), &fixtures)
	assert.Nil(t, err)

	schemaJson, err := ioutil.ReadFile("base.avsc")
	assert.Nil(t, err)

	codec, err := goavro.NewCodec(string(schemaJson))
	assert.Nil(t, err)

	var buf bytes.Buffer
	for _, f := range fixtures {
		buf.Reset()
		err = f.Serialize(&buf)
		assert.Nil(t, err)

		datum, remaining, err := codec.NativeFromBinary(buf.Bytes())
		assert.Nil(t, err)

		assert.Equal(t, 0, len(remaining))
		record := datum.(map[string]interface{})

		recordVal, ok := record["EnumArrayField"]
		assert.Equal(t, true, ok)

		avroArray := recordVal.([]interface{})
		assert.Equal(t, len(avroArray), len(f.EnumArrayField))
		for i := 0; i < len(f.EnumArrayField); i++ {
			position := f.EnumArrayField[i]
			assert.Equal(t, avroArray[i], position.String())

			enumified, err := PositionFromString(position.String())
			assert.Nil(t, err)
			assert.Equal(t, position, enumified)
		}
	}
}

func TestRoundTrip(t *testing.T) {
	fixtures := make([]ArrayEnumRecord, 0)
	err := json.Unmarshal([]byte(fixtureJson), &fixtures)
	assert.Nil(t, err)

	var buf bytes.Buffer
	for _, f := range fixtures {
		buf.Reset()
		err = f.Serialize(&buf)
		assert.Nil(t, err)

		datum, err := DeserializeArrayEnumRecord(&buf)
		assert.Nil(t, err)
		assert.Equal(t, datum, f)
	}
}
