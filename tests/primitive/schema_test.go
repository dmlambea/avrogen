package base

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"reflect"
	"testing"

	"github.com/linkedin/goavro/v2"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/dmlambea/avrogen/pkg/avro"
	"gitlab.com/dmlambea/avrogen/pkg/parser"
)

const fixtureJSON = `
[
{"IntField": 1, "LongField": 2, "FloatField": 3.4, "DoubleField": 5.6, "StringField": "789", "BoolField": true, "BytesField": "VGhpcyBpcyBhIHRlc3Qgc3RyaW5n"},
{"IntField": 2147483647, "LongField": 9223372036854775807, "FloatField": 3.402823e+38, "DoubleField": 1.7976931348623157e+308, "StringField": "", "BoolField": false, "BytesField": ""},
{"IntField": -2147483647, "LongField": -9223372036854775807, "FloatField": 3.402823e-38, "DoubleField": 2.2250738585072014e-308, "StringField": "", "BoolField": true, "BytesField": ""}
]
`

func compareFixtureGoAvro(t *testing.T, actual interface{}, expected interface{}) {
	record := actual.(map[string]interface{})
	value := reflect.ValueOf(expected)
	for i := 0; i < value.NumField(); i++ {
		fieldName := value.Type().Field(i).Name
		structVal := value.Field(i).Interface()
		avroVal, ok := record[fieldName]
		assert.Equal(t, true, ok)
		assert.Equal(t, structVal, avroVal)
	}
}

func TestPrimitiveFixture(t *testing.T) {
	fixtures := make([]Record, 0)
	err := json.Unmarshal([]byte(fixtureJSON), &fixtures)
	assert.Nil(t, err)

	schemaJSON, err := ioutil.ReadFile("base.avsc")
	require.Nil(t, err)

	codec, err := goavro.NewCodec(string(schemaJSON))
	require.Nil(t, err)

	var buf bytes.Buffer
	for _, f := range fixtures {
		buf.Reset()
		err = f.Serialize(&buf)
		require.Nil(t, err)

		datum, remaining, err := codec.NativeFromBinary(buf.Bytes())
		require.Nil(t, err)
		assert.Equal(t, 0, len(remaining))
		compareFixtureGoAvro(t, datum, f)
	}
}

func TestRoundTrip(t *testing.T) {
	fixtures := make([]Record, 0)
	err := json.Unmarshal([]byte(fixtureJSON), &fixtures)
	require.Nil(t, err)

	var buf bytes.Buffer
	for _, f := range fixtures {
		buf.Reset()
		err = f.Serialize(&buf)
		require.Nil(t, err)

		target, err := DeserializeRecord(&buf)
		require.Nil(t, err)

		assert.Equal(t, target, f)
	}
}

func BenchmarkSerializePrimitiveRecord(b *testing.B) {
	buf := new(bytes.Buffer)
	record := Record{1, 2, 3.4, 5.6, "789", true, []byte{1, 2, 3, 4}}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		err := record.Serialize(buf)
		assert.Nil(b, err)
	}
}

func BenchmarkSerializePrimitiveGoavro(b *testing.B) {
	schemaJSON, err := ioutil.ReadFile("base.avsc")
	require.Nil(b, err)

	codec, err := goavro.NewCodec(string(schemaJSON))
	require.Nil(b, err)

	someRecord := map[string]interface{}{
		"IntField":    int32(1),
		"LongField":   int64(2),
		"FloatField":  float32(3.4),
		"DoubleField": float64(5.6),
		"StringField": "789",
		"BoolField":   true,
		"BytesField":  []byte{1, 2, 3, 4},
	}
	buf := make([]byte, 0, 1024)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err := codec.BinaryFromNative(buf, someRecord)
		assert.Nil(b, err)
	}
}

func BenchmarkDeserializePrimitiveRecord(b *testing.B) {
	buf := new(bytes.Buffer)
	record := Record{1, 2, 3.4, 5.6, "789", true, []byte{1, 2, 3, 4}}
	err := record.Serialize(buf)
	require.Nil(b, err)

	recordBytes := buf.Bytes()

	schemaJSON, err := ioutil.ReadFile("base.avsc")
	require.Nil(b, err)

	rType, err := parser.ParseSchema(string(schemaJSON))
	require.Nil(b, err)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		err := avro.DeserializeFromTypes(bytes.NewReader(recordBytes), rType, rType, &record)
		assert.Nil(b, err)
	}
}

func BenchmarkDeserializePrimitiveGoavro(b *testing.B) {
	schemaJSON, err := ioutil.ReadFile("base.avsc")
	require.Nil(b, err)

	codec, err := goavro.NewCodec(string(schemaJSON))
	require.Nil(b, err)

	someRecord := map[string]interface{}{
		"IntField":    int32(1),
		"LongField":   int64(2),
		"FloatField":  float32(3.4),
		"DoubleField": float64(5.6),
		"StringField": "789",
		"BoolField":   true,
		"BytesField":  []byte{1, 2, 3, 4},
	}

	buf, err := codec.BinaryFromNative(nil, someRecord)
	require.Nil(b, err)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, _, err := codec.NativeFromBinary(buf)
		assert.Nil(b, err)
	}
}
