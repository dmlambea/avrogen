package base

import (
	"bytes"
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const fixtureJson = `
[
	{
		"IntField": { 
			"a": 1,
			"b": null, 
			"c": 2147483647
		}
	}
]
`

func TestRoundTrip(t *testing.T) {
	fixtures := make([]MapTestRecord, 0)
	err := json.Unmarshal([]byte(fixtureJson), &fixtures)
	require.Nil(t, err)

	var buf bytes.Buffer
	for _, f := range fixtures {
		buf.Reset()
		err = f.Serialize(&buf)
		require.Nil(t, err)

		datum, err := DeserializeMapTestRecord(&buf)
		require.Nil(t, err)

		assert.Equal(t, datum, f)
	}
}
