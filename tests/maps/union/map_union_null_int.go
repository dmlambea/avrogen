// Code generated by gitlab.com/dmlambea/avrogen v0.0-testing
//
// This code was generated from source AVSC file: base.avsc
package base

import (
	"io"

	"gitlab.com/dmlambea/avrogen/pkg/avro"
)

// MapUnionNullInt is the type defining an Avro map of *int32
type MapUnionNullInt map[string]*int32

// NewMapUnionNullInt is a constructor function to create a map of type map[string]*int32
// This call can be chained with Add() to easily create & initializate a map.
func NewMapUnionNullInt() MapUnionNullInt {
	return make(MapUnionNullInt)
}

func (m MapUnionNullInt) Add(key string, val *int32) MapUnionNullInt {
	m[key] = val
	return m
}

func writeMapUnionNullInt(r MapUnionNullInt, w io.Writer) (err error) {
	if err = avro.WriteLong(int64(len(r)), w); err != nil || len(r) == 0 {
		return
	}
	for key, val := range r {
		if err = avro.WriteString(key, w); err != nil {
			return err
		}
		switch val == nil {
		case true:
			if err = avro.WriteLong(0, w); err != nil {
				return
			}
		default:
			if err = avro.WriteLong(1, w); err != nil {
				return
			}
			if err = avro.WritePrimitive(*val, w); err != nil {
				return
			}
		}
	}
	return avro.WriteLong(0, w)
}

