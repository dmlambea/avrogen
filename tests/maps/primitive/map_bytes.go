// Code generated by gitlab.com/dmlambea/avrogen v0.0-testing
//
// This code was generated from source AVSC file: base.avsc
package base

import (
	"io"

	"gitlab.com/dmlambea/avrogen/pkg/avro"
)

// MapBytes is the type defining an Avro map of []byte
type MapBytes map[string][]byte

// NewMapBytes is a constructor function to create a map of type map[string][]byte
// This call can be chained with Add() to easily create & initializate a map.
func NewMapBytes() MapBytes {
	return make(MapBytes)
}

func (m MapBytes) Add(key string, val []byte) MapBytes {
	m[key] = val
	return m
}

func writeMapBytes(r MapBytes, w io.Writer) (err error) {
	if err = avro.WriteLong(int64(len(r)), w); err != nil || len(r) == 0 {
		return
	}
	for key, val := range r {
		if err = avro.WriteString(key, w); err != nil {
			return err
		}
		if err = avro.WritePrimitive(val, w); err != nil {
			return
		}
	}
	return avro.WriteLong(0, w)
}

