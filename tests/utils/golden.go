package utils

import (
	"flag"
	"io/ioutil"
	"path/filepath"
)

var update = flag.Bool("update", false, "update .golden files")

type AssertFunc func(actual, expected []byte)

func NewGoldenFileWriter() *GoldenFileWriter {
	return &GoldenFileWriter{
		FileContents: make(map[string]string),
	}
}

type GoldenFileWriter struct {
	FileContents map[string]string
}

func (fw GoldenFileWriter) WriteFile(fileName, sourceCode string) (err error) {
	fw.FileContents[fileName] = sourceCode
	return nil
}

func LoadGoldenData(fqname string) (data []byte) {
	golden := filepath.Join("testdata", fqname)
	data, _ = ioutil.ReadFile(golden)
	return
}

func AssertGolden(baseName string, actual []byte, fn AssertFunc) {
	fqname := filepath.Join("testdata", baseName+".golden")
	var expected []byte
	if *update {
		ioutil.WriteFile(fqname, actual, 0644)
		expected = actual
	} else {
		expected, _ = ioutil.ReadFile(fqname)
	}
	fn(actual, expected)
}
