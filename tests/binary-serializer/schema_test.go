package base

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"path/filepath"
	"testing"

	"gitlab.com/dmlambea/avrogen/pkg/parser"
	"gitlab.com/dmlambea/avrogen/pkg/types"

	gparser "github.com/actgardner/gogen-avro/v7/parser"
	gresolver "github.com/actgardner/gogen-avro/v7/resolver"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var (
	update = flag.Bool("update", false, "update .golden files")

	enumFixtures   = []string{"enum", "enum_namespace"}
	fixedFixtures  = []string{"fixed", "fixed_namespace"}
	arrayFixtures  = []string{"array_enum", "array_fixed"}
	mapFixtures    = []string{"map_enum", "map_fixed"}
	unionFixtures  = []string{"union_nonopt", "union_optsimple", "union_optcomplex"}
	recordFixtures = []string{"record", "record_nested", "record_defaults", "record_defaults_nested"}

	fixtures = []struct {
		name string
		list []string
	}{
		{"Enum", enumFixtures},
		{"Fixed", fixedFixtures},
		{"Array", arrayFixtures},
		{"Map", mapFixtures},
		{"Union", unionFixtures},
		{"Record", recordFixtures},
	}
)

func TestRoundTrip(t *testing.T) {
	for _, f := range fixtures {
		t.Run(f.name, func(t *testing.T) {
			for _, item := range f.list {
				roundTrip(t, item)
			}
		})
	}
}

func roundTrip(t *testing.T, name string) {
	fixture := string(goldenLoad(fmt.Sprintf("schema_%s.avsc", name)))
	require.NotNil(t, fixture)

	tp, err := parser.ParseSchema(fixture)
	require.Nil(t, err)
	require.NotNil(t, tp)

	var buf bytes.Buffer
	err = types.Serialize(&buf, tp)
	require.Nil(t, err)

	serializedData := buf.Bytes()
	goldenEquals(t, fmt.Sprintf("schema_%s", name), serializedData)

	tp, err = types.Deserialize(bytes.NewBuffer(serializedData))
	require.Nil(t, err)
	require.NotNil(t, tp)

	buf.Reset()
	err = types.Serialize(&buf, tp)
	require.Nil(t, err)
	assert.Equal(t, serializedData, buf.Bytes())
}

func BenchmarkDeserializeTypeGogenavro(b *testing.B) {
	for _, f := range fixtures {
		b.Run(f.name, func(b *testing.B) {
			for _, item := range f.list {
				schema := goldenLoad(fmt.Sprintf("schema_%s.avsc", item))
				deserializeTypeGogenAvro(b, schema)
			}
		})
	}
}

func BenchmarkDeserializeTypeEnumAvrogenJSON(b *testing.B) {
	for _, f := range fixtures {
		b.Run(f.name, func(b *testing.B) {
			for _, item := range f.list {
				schema := string(goldenLoad(fmt.Sprintf("schema_%s.avsc", item)))
				deserializeTypeAvrogenJSON(b, schema)
			}
		})
	}
}

func BenchmarkDeserializeTypeEnumAvrogenBinary(b *testing.B) {
	for _, f := range fixtures {
		b.Run(f.name, func(b *testing.B) {
			for _, item := range f.list {
				schema := string(goldenLoad(fmt.Sprintf("schema_%s.avsc", item)))
				deserializeTypeAvrogenBinary(b, schema)
			}
		})
	}
}

var (
	disallowCompilerOptimizationsType types.Generic
	disallowCompilerOptimizationsErr  error
)

func deserializeTypeGogenAvro(b *testing.B, schema []byte) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		readerNs := gparser.NewNamespace(false)
		t, err := readerNs.TypeForSchema(schema)
		require.Nil(b, err)
		require.NotNil(b, t)

		for _, def := range readerNs.Roots {
			disallowCompilerOptimizationsErr = gresolver.ResolveDefinition(def, readerNs.Definitions)
			assert.Nil(b, disallowCompilerOptimizationsErr)
		}
	}
}

func deserializeTypeAvrogenJSON(b *testing.B, schema string) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		disallowCompilerOptimizationsType, disallowCompilerOptimizationsErr = parser.ParseSchema(schema)
		require.Nil(b, disallowCompilerOptimizationsErr)
		require.NotNil(b, disallowCompilerOptimizationsType)
	}
}

func deserializeTypeAvrogenBinary(b *testing.B, schema string) {
	t, err := parser.ParseSchema(schema)
	require.Nil(b, err)
	require.NotNil(b, t)

	var buf bytes.Buffer
	err = types.Serialize(&buf, t)
	require.Nil(b, err)

	serializedData := buf.Bytes()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		disallowCompilerOptimizationsType, disallowCompilerOptimizationsErr = types.Deserialize(bytes.NewBuffer(serializedData))
		require.Nil(b, disallowCompilerOptimizationsErr)
		require.NotNil(b, disallowCompilerOptimizationsType)
	}
}

func goldenLoad(name string) (data []byte) {
	golden := filepath.Join("testdata", name)
	data, _ = ioutil.ReadFile(golden)
	return
}

func goldenEquals(t *testing.T, name string, actual []byte) {
	golden := filepath.Join("testdata", name+".golden")
	var expected []byte
	if *update {
		ioutil.WriteFile(golden, actual, 0644)
		expected = actual
	} else {
		expected, _ = ioutil.ReadFile(golden)
	}
	require.Equal(t, expected, actual)
}
