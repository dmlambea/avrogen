package base

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
)

var fixtures = []Event{
	{
		Id:      "id1",
		StartIp: [16]byte{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16},
		EndIp:   [16]byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	},
	{
		Id:      "differentid",
		StartIp: [16]byte{0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255},
		EndIp:   [16]byte{0, 1, 3, 7, 15, 31, 63, 127, 254, 2, 4, 6},
	},
}

/*
Disabled due to impossible comparison between binary and JSON schemas
func compareFixtureGoAvro(t *testing.T, actual interface{}, expected interface{}) {
	record := actual.(map[string]interface{})
	fixture := expected.(Event)

	id, ok := record["id"]
	assert.Equal(t, ok, true)
	assert.Equal(t, id, fixture.Id)

	startIp, ok := record["start_ip"]
	assert.Equal(t, ok, true)
	assert.Equal(t, startIp.([]byte), fixture.StartIp[:])

	endIp, ok := record["end_ip"]
	assert.Equal(t, ok, true)
	assert.Equal(t, endIp.([]byte), fixture.EndIp[:])
}

func TestRootUnionFixture(t *testing.T) {
	// copied from base.avsc
	schemaJSON := `{
		"type": "fixed",
		"size": 16,
		"name": "ip_address",
		"aliases": [
		  "IPAddr",
		  "ipAddr"
		]
	  }`

	codec, err := goavro.NewCodec(schemaJSON)
	assert.Nil(t, err)

	var buf bytes.Buffer
	for _, f := range fixtures {
		buf.Reset()
		err = f.Serialize(&buf)
		assert.Nil(t, err)

		datum, remaining, err := codec.NativeFromBinary(buf.Bytes())
		assert.Nil(t, err)

		assert.Equal(t, 0, len(remaining))
		compareFixtureGoAvro(t, f, datum)
	}
}
*/

func TestRoundTrip(t *testing.T) {
	var buf bytes.Buffer
	for _, f := range fixtures {
		buf.Reset()
		err := f.Serialize(&buf)
		assert.Nil(t, err)

		datum, err := DeserializeEvent(&buf)
		assert.Nil(t, err)
		assert.Equal(t, datum, f)
	}
}
