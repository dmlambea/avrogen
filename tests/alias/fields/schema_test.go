package base

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/dmlambea/avrogen/tests/alias/fields/evol"
)

func TestEvolution(t *testing.T) {
	oldAliasRecord := AliasRecord{
		A: "hi",
		C: "bye",
	}

	var buf bytes.Buffer
	assert.Nil(t, oldAliasRecord.Serialize(&buf))

	newAliasRecord, err := evol.DeserializeAliasRecord(&buf)
	require.Nil(t, err)

	assert.Equal(t, "hi", newAliasRecord.B)
	assert.Equal(t, "bye", newAliasRecord.D)
}
