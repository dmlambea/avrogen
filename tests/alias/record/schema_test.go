package base

import (
	"bytes"
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const fixtureJson = `
[
  {
    "OtherField": {
      "StringField": "789", 
      "BoolField": true, 
      "BytesField": "VGhpcyBpcyBhIHRlc3Qgc3RyaW5n"
    }
  },
  {
    "OtherField": {
      "StringField": "abcdghejw", 
      "BoolField": true, 
      "BytesField": "VGhpcyBpcyBhIHRlc3Qgc3RyaW5n"
    }
  },
  {
    "OtherField": {
      "StringField": "jdnwjkendwedddedee", 
      "BoolField": true, 
      "BytesField": "VGhpcyBpcyBhIHRlc3Qgc3RyaW5n"
    }
  }
]
`

func TestRoundTrip(t *testing.T) {
	fixtures := make([]NestedTestRecord, 0)
	err := json.Unmarshal([]byte(fixtureJson), &fixtures)
	assert.Nil(t, err)

	var buf bytes.Buffer
	for _, f := range fixtures {
		buf.Reset()
		err = f.Serialize(&buf)
		assert.Nil(t, err)

		datum, err := DeserializeNestedTestRecord(&buf)
		require.Nil(t, err)
		assert.Equal(t, datum, f)
	}
}
