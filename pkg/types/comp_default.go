package types

// Common attributes for all default-valued types
type defaultValuedComponent struct {
	defaultValue    interface{}
	defaultValueSet bool
}

var (
	// Ensure interface implementation
	_ DefaultValued = &defaultValuedComponent{}
)

func (comp *defaultValuedComponent) Default() interface{} {
	return comp.defaultValue
}

func (comp *defaultValuedComponent) HasDefault() bool {
	return comp.defaultValueSet
}

func (comp *defaultValuedComponent) SetDefault(value interface{}) {
	comp.defaultValue = value
	comp.defaultValueSet = true
}
