package types

// Composite is implemented by any Avro type able to contain children types,
// like arrays, maps, recods and unions.
type Composite interface {
	Children() []Generic
}
