package types

var (
	// Ensure interface implementation
	_ Documented = &documentComponent{}
)

type documentComponent struct {
	doc string
}

func (comp *documentComponent) Doc() string {
	return comp.doc
}

func (comp *documentComponent) SetDoc(doc string) {
	comp.doc = doc
}
