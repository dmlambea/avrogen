package types

// Documented is implemented by any Avro type able to have a documentary
// text associated to it, like fixed, enums and records.
type Documented interface {
	Doc() string
	SetDoc(string)
}
