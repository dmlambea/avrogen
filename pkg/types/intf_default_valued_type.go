package types

// DefaultValued is implemented by any type able to have a default value,
// like enums and record fields.
type DefaultValued interface {
	Default() interface{}
	HasDefault() bool
	SetDefault(value interface{})
}
