package types

var (
	// Ensure interface implementation
	_ Named = &nameComponent{}
)

type nameComponent struct {
	name    string
	aliases []string
}

func (comp *nameComponent) String() string {
	return comp.Name()
}

func (comp *nameComponent) Name() string {
	return comp.name
}

func (comp *nameComponent) Aliases() []string {
	return comp.aliases
}

func (comp *nameComponent) SetAliases(aliases []string) {
	comp.aliases = aliases
}

func (comp *nameComponent) setName(name string) {
	comp.name = name
}

// schemaPart returns the parts of the schema defining the name and the optional
// aliases, if any
func (comp *nameComponent) schemaParts() map[string]interface{} {
	m := make(map[string]interface{})
	m["name"] = comp.name
	if len(comp.aliases) != 0 {
		m["aliases"] = comp.aliases
	}
	return m
}
