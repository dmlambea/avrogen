package types

// NewEnum creates an Enum type with the name `name` and the given set of symbols.
func NewEnum(name string, symbols []string) *Enum {
	t := &Enum{symbols: make([]string, len(symbols))}
	for i := range symbols {
		t.symbols[i] = symbols[i]
	}
	t.setName(name)
	return t
}

var (
	// Ensure interface implementations
	_ Complex       = &Enum{}
	_ DefaultValued = &Enum{}
	_ Documented    = &Enum{}
	_ Named         = &Enum{}
)

// Enum is the type defining an Avro enum type.
type Enum struct {
	nameComponent
	defaultValuedComponent
	documentComponent
	symbols []string
}

// Symbols returns the list of symbols for this enum.
func (t *Enum) Symbols() []string {
	return t.symbols
}

// IndexOfSymbol returns the position of the symbol s within this enum, or -1
// if it does not exist
func (t *Enum) IndexOfSymbol(s string) int {
	for i := range t.symbols {
		if t.symbols[i] == s {
			return i
		}
	}
	return -1
}

// IsReadableBy returns true if:
//  - the reader is also an enum type   AND
//    - the reader has a default value  OR
//    - the reader has at least one symbol in common
func (t *Enum) IsReadableBy(other Generic, visited VisitMap) bool {
	// Both types must be enums and have the same name
	f, ok := other.(*Enum)
	ok = ok && (f.Name() == t.Name())
	if !ok {
		return false
	}

	// If the reader enum has a default value, it's compatible.
	if f.HasDefault() {
		return true
	}

	// Otherwise, at least one symbol must exist in both enums.
	for i := range t.symbols {
		idx := f.IndexOfSymbol(t.symbols[i])
		if idx != -1 {
			return true
		}
	}
	return false
}

func (t *Enum) assertComplex() {}

func (t *Enum) resolveSchema(m VisitMap) interface{} {
	n := t.Name()
	if m[n] {
		return n
	}
	m[n] = true

	parts := t.schemaParts()
	parts["type"] = "enum"
	parts["symbols"] = t.symbols
	if t.HasDefault() {
		parts["default"] = t.Default()
	}
	return parts
}

func (t *Enum) typeID() binaryTypeID {
	return tEnum
}
