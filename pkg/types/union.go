package types

// NewUnion creates a new union of the given types.
func NewUnion(itemTypes []Generic) *Union {
	t := &Union{}
	t.setItemTypes(itemTypes)
	// Detect the optional (null) type
	for i, c := range t.Children() {
		if c.(*Field).Type().typeID() == tNull {
			t.optIndex = i + 1
			break
		}
	}
	return t
}

var (
	// Ensure interface implementations
	_ Complex   = &Union{}
	_ Composite = &Union{}
)

// Union defines an Avro union type.
type Union struct {
	multiChildComponent

	// Internally, optIndex is a positional index plus one of the null type within
	// an optional union in order to keep zero-value useful (0 = non-optional union).
	optIndex int
}

func (t *Union) String() string {
	return "union"
}

// IsOptional returns true if this union has a null-type option
func (t *Union) IsOptional() bool {
	return t.optIndex > 0
}

// IsOptionalSimple returns true if this union is optional and has only one another type
func (t *Union) IsOptionalSimple() bool {
	return t.IsOptional() && len(t.Children()) == 2
}

// OptionalIndex returns the index of the null type
func (t *Union) OptionalIndex() int {
	return t.optIndex - 1
}

// NonOptionalIndex has meaning on simple unions and returns the index of the non-null type.
func (t *Union) NonOptionalIndex() int {
	return 1 - (t.optIndex - 1)
}

// IsReadableBy returns true if this union can be read by the type `other`
func (t *Union) IsReadableBy(other Generic, visited VisitMap) bool {
	u, otherIsUnion := other.(*Union)

	// Check the optional case for both unions
	if otherIsUnion && t.IsOptional() {
		return true
	}

	// Report if *any* writer type could be deserialized by the reader
	for _, child := range t.Children() {
		switch otherIsUnion {
		case true:
			for _, otherUnionChild := range u.Children() {
				// Union children are fields, so their types is what is needed to match
				otherUnionChildType := otherUnionChild.(*Field).Type()
				if child.IsReadableBy(otherUnionChildType, visited) {
					return true
				}
			}
		case false:
			if child.IsReadableBy(other, visited) {
				return true
			}
		}
	}
	return false
}

func (t *Union) resolveSchema(m VisitMap) interface{} {
	fieldSchemas := make([]interface{}, len(t.Children()))
	for i, child := range t.Children() {
		fieldSchemas[i] = child.(*Field).Type().resolveSchema(m)
	}
	return fieldSchemas
}

func (t *Union) typeID() binaryTypeID {
	return tUnion
}

// This is a convenience leaf function to check if the given field is a Union
// and if so, try to find a child type that can read this type. This function
// is used as a last resort by higher types (Record, Union, Array and Map).
func tryIsReadableByUnion(tp, other Generic, visited VisitMap) bool {
	if u, ok := other.(*Union); ok {
		for _, child := range u.Children() {
			// Union children are fields, so their types is what is needed to match
			childType := child.(*Field).Type()
			if tp.IsReadableBy(childType, visited) {
				return true
			}
		}
	}
	return false
}
