package types

// Named is implemented by any Avro type able to have a  fully qualified name and
// possibly a list of aliases associated to it, like fixed, enums and records.
type Named interface {
	Name() string
	Aliases() []string
	SetAliases([]string)
	setName(string)
	schemaParts() map[string]interface{}
}
