package types

// NewPrimitive returns the cached definition of a primitive type.
func NewPrimitive(avroName string) *Primitive {
	return byName[avroName]
}

// newPrimitiveFromTypeID returns the cached definition of a primitive type
// from its type ID.
func newPrimitiveFromTypeID(id binaryTypeID) *Primitive {
	return byID[id]
}

// Primitive defines a basic, primitive Avro type
type Primitive struct {
	name   string
	goType string
	tID    binaryTypeID
	compat []string
}

var (
	// Ensure interface implementation
	_ Generic = &Primitive{}
)

var (
	byName, byID = createPrimitiveTypesMaps([]*Primitive{
		&Primitive{name: "null", goType: "", tID: tNull},
		&Primitive{name: "boolean", goType: "bool", tID: tBoolean},
		&Primitive{name: "int", goType: "int32", tID: tInt, compat: []string{"long", "float", "double"}},
		&Primitive{name: "long", goType: "int64", tID: tLong, compat: []string{"float", "double"}},
		&Primitive{name: "float", goType: "float32", tID: tFloat, compat: []string{"double"}},
		&Primitive{name: "double", goType: "float64", tID: tDouble},
		&Primitive{name: "bytes", goType: "[]byte", tID: tBytes, compat: []string{"string"}},
		&Primitive{name: "string", goType: "string", tID: tString, compat: []string{"bytes"}},
	})
)

// createPrimitiveTypesMaps makes the cached definition of all primitive types.
func createPrimitiveTypesMaps(list []*Primitive) (byName map[string]*Primitive, byID map[binaryTypeID]*Primitive) {
	byName = make(map[string]*Primitive)
	byID = make(map[binaryTypeID]*Primitive)
	for _, f := range list {
		byName[f.name] = f
		byID[f.tID] = f
	}
	return
}

// String returns the Avro name for this primitive type.
func (p Primitive) String() string {
	return p.name
}

// Name returns the Avro name for this primitive type.
func (p Primitive) Name() string {
	return p.name
}

// GoType returns the Go primitive type for this Avro type.
func (p Primitive) GoType() string {
	return p.goType
}

// IsReadableBy returns true if this type can be read by the type `other`
func (p Primitive) IsReadableBy(other Generic, visited VisitMap) bool {
	switch reader := other.(type) {
	case *Primitive:
		// Reader is also primitive type
		if p.goType == reader.goType {
			return true
		}

		// Promote writer, if able
		for i := range p.compat {
			promoted := NewPrimitive(p.compat[i])
			if promoted.goType == reader.goType {
				return true
			}
		}
	case *Union:
		// Reader is a union
		for _, child := range reader.Children() {
			// Union children are fields, so their types is what is needed to match
			childType := child.(*Field).Type()
			if p.IsReadableBy(childType, visited) {
				return true
			}
		}
	}
	return false
}

func (p Primitive) resolveSchema(_ VisitMap) interface{} {
	return p.name
}

func (p Primitive) typeID() binaryTypeID {
	return p.tID
}
