package types

var (
	// Ensure interface implementations
	_ Complex   = &multiChildComponent{}
	_ Composite = &multiChildComponent{}
)

type multiChildComponent struct {
	itemTypes []Generic
}

// Chidren returns the children types for this type and fulfills the
// Composite interface
func (comp *multiChildComponent) Children() []Generic {
	return comp.itemTypes
}

func (comp *multiChildComponent) assertComplex() {}

func (comp *multiChildComponent) setItemTypes(itemTypes []Generic) {
	comp.itemTypes = itemTypes
}
