package types

// Complex is implemented by any Avro type defined as "complex" in the specs:
// fixed, enum, map, array, record and union
type Complex interface {
	assertComplex()
}
