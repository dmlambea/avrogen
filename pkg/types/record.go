package types

import (
	"bytes"
	"encoding/json"
	"fmt"
)

// NewRecord creates a new, blank record type with no fields.
func NewRecord(name string) *Record {
	t := &Record{}
	t.setName(name)
	return t
}

var (
	// Ensure interface implementations
	_ Complex    = &Record{}
	_ Composite  = &Record{}
	_ Documented = &Record{}
	_ Named      = &Record{}
	//_ Schema     = &Record{}
)

// Record defines a record
type Record struct {
	nameComponent
	documentComponent
	multiChildComponent
}

// SetFields replaces this record's set of fields.
func (t *Record) SetFields(itemTypes []Generic) {
	t.setItemTypes(itemTypes)
}

// BinarySchema returns the binary serialization schema for this record.
func (t *Record) BinarySchema() []byte {
	var b bytes.Buffer
	if err := Serialize(&b, t); err != nil {
		panic(fmt.Errorf("internal error: %v", err))
	}
	return b.Bytes()
}

// JSONSchema returns the minimum required Avro-compatible JSON schema for this record.
func (t *Record) JSONSchema() string {
	m := make(VisitMap)
	s := t.resolveSchema(m)
	if str, ok := s.(string); ok {
		return str
	}
	b, _ := json.Marshal(s)
	return string(b)
}

// IsReadableBy returns true if this record can be read by the type `other`
func (t *Record) IsReadableBy(other Generic, visited VisitMap) bool {
	// If there's a circular reference, don't evaluate every field on the second pass
	if _, ok := visited[t.Name()]; ok {
		return true
	}
	visited[t.Name()] = true

	if otherRecord, ok := other.(*Record); ok {
		for _, child := range otherRecord.Children() {
			readerField, ok := child.(*Field)
			if !ok {
				panic(fmt.Sprintf("Unexpected non-field type %T in %s", child, otherRecord.Name()))
			}
			writerField := t.FindFieldByNameOrAlias(readerField)

			// Two schemas are incompatible if the reader has a field with no default value that is not present in the writer schema
			if writerField == nil && !readerField.HasDefault() {
				return false
			}

			// The two schemas are incompatible if two fields with the same name have different schemas
			if writerField != nil && !writerField.Type().IsReadableBy(readerField.Type(), visited) {
				return false
			}
		}
		return true
	}
	return tryIsReadableByUnion(t, other, visited)
}

// FindFieldByNameOrAlias finds a child field of this Record that matches by
// name or any of the aliases for the given field.
func (t *Record) FindFieldByNameOrAlias(sample *Field) *Field {
	for _, child := range t.Children() {
		field, ok := child.(*Field)
		if !ok {
			panic(fmt.Sprintf("Unexpected non-field type %T in %s", child, t.Name()))
		}

		if field.alsoKnownAs(sample.Name()) {
			return field
		}
		for _, alias := range sample.Aliases() {
			if field.alsoKnownAs(alias) {
				return field
			}
		}
	}
	return nil
}

func (t *Record) resolveSchema(m VisitMap) interface{} {
	n := t.Name()
	if m[n] {
		return n
	}
	m[n] = true

	parts := t.schemaParts()
	parts["type"] = "record"
	fieldSchemas := make([]interface{}, len(t.Children()))
	for i, child := range t.Children() {
		fieldSchemas[i] = child.resolveSchema(m)
	}
	parts["fields"] = fieldSchemas
	return parts
}

func (t *Record) typeID() binaryTypeID {
	return tRecord
}
