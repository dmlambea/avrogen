package bin

import (
	"bytes"
	"fmt"
	"math"
)

// Buffer is a special bytes.Buffer with extra methods for internal binary
// representation of Avro basic data types.
type Buffer struct {
	b *bytes.Buffer
}

// New creates an empty Buffer
func New() *Buffer {
	return NewFromBuffer(&bytes.Buffer{})
}

// NewFromSlice creates a Buffer backed by the given slice.
func NewFromSlice(s []byte) *Buffer {
	return NewFromBuffer(bytes.NewBuffer(s))
}

// NewFromBuffer creates a Buffer backed by the given bytes.Buffer.
func NewFromBuffer(buf *bytes.Buffer) *Buffer {
	return &Buffer{
		b: buf,
	}
}

// Bytes returns a byte slice with the contents of this Buffer.
func (b *Buffer) Bytes() []byte {
	return b.b.Bytes()
}

// Read8 reads a byte from this Buffer.
func (b *Buffer) Read8() byte {
	val, err := b.b.ReadByte()
	if err == nil {
		return val
	}
	panic(err)
}

// Write8 writes a byte to this Buffer.
func (b *Buffer) Write8(val byte) {
	if err := b.b.WriteByte(val); err != nil {
		panic(err)
	}
}

// Read16 reads a 16-bit value from this buffer, in little-endian.
func (b *Buffer) Read16() int {
	var buf [2]byte
	b.readSlice(buf[:])
	return int(buf[0]) | int(buf[1])<<8
}

// Write16 writes the given int as 16-bit value in little-endian.
func (b *Buffer) Write16(val int) {
	var buf [2]byte
	buf[0] = byte(val & 0xff)
	buf[1] = byte((val >> 8) & 0xff)
	if _, err := b.b.Write(buf[:]); err != nil {
		panic(err)
	}
}

// Read32 reads a 32-bit value from this buffer, in little-endian.
func (b *Buffer) Read32() (val int32) {
	var buf [4]byte
	b.readSlice(buf[:])
	val = int32(buf[0]) |
		int32(buf[1])<<8 |
		int32(buf[2])<<16 |
		int32(buf[3])<<24
	return
}

// Write32 writes the given int as 32-bit value in little-endian.
func (b *Buffer) Write32(val int32) {
	var buf [4]byte
	buf[0] = byte(val & 0xff)
	buf[1] = byte((val >> 8) & 0xff)
	buf[2] = byte(val >> 16 & 0xff)
	buf[3] = byte((val >> 24) & 0xff)
	if _, err := b.b.Write(buf[:]); err != nil {
		panic(err)
	}
}

// ReadFloat32 reads a 32-bit value as float32 from this buffer. The float32
// is expected to be in little-endian.
func (b *Buffer) ReadFloat32() (val float32) {
	i := b.Read32()
	val = math.Float32frombits(uint32(i))
	return
}

// WriteFloat32 writes the given float32 to this buffer, in little-endian.
func (b *Buffer) WriteFloat32(val float32) {
	b.Write32(int32(math.Float32bits(val)))
}

// Read64 reads a 64-bit value from this buffer, in little-endian.
func (b *Buffer) Read64() (val int64) {
	var buf [8]byte
	b.readSlice(buf[:])
	val = int64(buf[0]) |
		int64(buf[1])<<8 |
		int64(buf[2])<<16 |
		int64(buf[3])<<24 |
		int64(buf[4])<<32 |
		int64(buf[5])<<40 |
		int64(buf[6])<<48 |
		int64(buf[7])<<56
	return
}

// Write64 writes the given int64 value in little-endian.
func (b *Buffer) Write64(val int64) {
	var buf [8]byte
	buf[0] = byte(val & 0xff)
	buf[1] = byte((val >> 8) & 0xff)
	buf[2] = byte(val >> 16 & 0xff)
	buf[3] = byte((val >> 24) & 0xff)
	buf[4] = byte(val >> 32 & 0xff)
	buf[5] = byte((val >> 40) & 0xff)
	buf[6] = byte(val >> 48 & 0xff)
	buf[7] = byte((val >> 56) & 0xff)
	if _, err := b.b.Write(buf[:]); err != nil {
		panic(err)
	}
}

// ReadFloat64 reads a 64-bit value as float64 from this buffer. The float64
// is expected to be in little-endian.
func (b *Buffer) ReadFloat64() (val float64) {
	i := b.Read64()
	val = math.Float64frombits(uint64(i))
	return
}

// WriteFloat64 writes the given float64 to this buffer, in little-endian.
func (b *Buffer) WriteFloat64(val float64) {
	b.Write64(int64(math.Float64bits(val)))
}

// ReadString reads a 16-bit value, then that much bytes from this Buffer and
// returns the bytes as string.
func (b *Buffer) ReadString() string {
	l := b.Read16()
	buf := make([]byte, l)
	b.readSlice(buf)
	return string(buf)
}

// WriteString writes the given string's length as a 16-bit value, then the
// string bytes to this Buffer.
func (b *Buffer) WriteString(val string) {
	b.Write16(len(val))
	if _, err := b.b.WriteString(val); err != nil {
		panic(err)
	}
}

// readSlice is a convenience function to read a slice from this Buffer.
func (b *Buffer) readSlice(buf []byte) {
	n, err := b.b.Read(buf)
	if err != nil {
		panic(err)
	}
	if n != len(buf) {
		panic(fmt.Errorf("unable to read %d-byte value", len(buf)))
	}
}
