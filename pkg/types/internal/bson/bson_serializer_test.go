package bson

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestSimpleMap(t *testing.T) {
	m := make(map[string]interface{})
	m["a"] = int32(42)
	m["b"] = "demo"
	m["c"] = float64(4.2)
	m["d"] = []interface{}{
		int32(42),
		nil,
		float32(8.4),
	}

	b, err := SerializeToSlice(m)
	require.Nil(t, err)
	require.NotNil(t, b)

	n, err := DeserializeFromSlice(b)
	require.Nil(t, err)
	require.NotNil(t, n)
	assert.Equal(t, m, n)
}
