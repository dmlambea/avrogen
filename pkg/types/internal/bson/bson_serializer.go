package bson

import (
	"fmt"

	"gitlab.com/dmlambea/avrogen/pkg/types/internal/bin"
)

// SerializeToSlice serializes the JSON object in jsonMap as byte array.
func SerializeToSlice(jsonData map[string]interface{}) (bsonData []byte, err error) {
	b := bin.New()
	if err = SerializeToBuffer(jsonData, b); err == nil {
		bsonData = b.Bytes()
	}
	return
}

// SerializeToBuffer serializes the JSON object in jsonMap as byte array to the
// given bin.Buffer.
func SerializeToBuffer(jsonData map[string]interface{}, buf *bin.Buffer) (err error) {
	s := bsonSerializer{
		buf: buf,
	}
	defer func() {
		r := recover()
		if r != nil {
			err = fmt.Errorf("unable to serialize BSON: %v", r)
		}
	}()
	s.serializeObject(jsonData)
	return
}

type bsonSerializer struct {
	buf *bin.Buffer
}

func (s *bsonSerializer) serializeType(t interface{}) {
	switch tp := t.(type) {
	case nil:
		s.buf.Write8(byte(btNull))
	case bool:
		switch bool(tp) {
		case true:
			s.buf.Write8(byte(btBooleanTrue))
		default:
			s.buf.Write8(byte(btBooleanFalse))
		}
	case int32:
		val := int32(tp)
		s.buf.Write8(byte(btInt32))
		s.buf.Write32(val)
	case int64:
		val := int64(tp)
		s.buf.Write8(byte(btInt64))
		s.buf.Write64(val)
	case float32:
		val := float32(tp)
		s.buf.Write8(byte(btFloat32))
		s.buf.WriteFloat32(val)
	case float64:
		s.buf.Write8(byte(btFloat64))
		s.buf.WriteFloat64(tp)
	case string:
		s.buf.Write8(byte(btString))
		s.buf.WriteString(tp)
	case []interface{}:
		s.serializeArray(tp)
	case map[string]interface{}:
		s.buf.Write8(byte(btObject))
		s.serializeObject(tp)
	default:
		panic(fmt.Errorf("unsupported JSON data type %T for binary serialization", tp))
	}
}

func (s *bsonSerializer) serializeArray(a []interface{}) {
	s.buf.Write8(byte(btArray))
	l := len(a)
	s.buf.Write16(l)
	for _, v := range a {
		s.serializeType(v)
	}
}

func (s *bsonSerializer) serializeObject(m map[string]interface{}) {
	l := len(m)
	s.buf.Write16(l)
	for k, v := range m {
		s.buf.WriteString(k)
		s.serializeType(v)
	}
}
