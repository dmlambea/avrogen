package bson

import (
	"fmt"

	"gitlab.com/dmlambea/avrogen/pkg/types/internal/bin"
)

// DeserializeFromSlice deserializes the BSON object from the given byte slice back to
// map[string]interface{}.
func DeserializeFromSlice(bsonData []byte) (jsonData map[string]interface{}, err error) {
	b := bin.NewFromSlice(bsonData)
	return DeserializeFromBuffer(b)
}

// DeserializeFromBuffer deserializes the BSON object back to map[string]interface{}.
func DeserializeFromBuffer(buf *bin.Buffer) (jsonData map[string]interface{}, err error) {
	s := bsonDeserializer{
		buf: buf,
	}
	defer func() {
		r := recover()
		if r != nil {
			err = fmt.Errorf("unable to deserialize BSON: %v", r)
		}
	}()
	jsonData = s.deserializeObject()
	return
}

type bsonDeserializer struct {
	buf *bin.Buffer
}

func (s *bsonDeserializer) deserializeType() (t interface{}) {
	tp := bsonTypeID(s.buf.Read8())
	switch tp {
	case btNull:
		return nil
	case btBooleanFalse, btBooleanTrue:
		return (tp == btBooleanTrue)
	case btInt32:
		return s.buf.Read32()
	case btInt64:
		return s.buf.Read64()
	case btFloat32:
		return s.buf.ReadFloat32()
	case btFloat64:
		return s.buf.ReadFloat64()
	case btString:
		return s.buf.ReadString()
	case btArray:
		return s.deserializeArray()
	case btObject:
		return s.deserializeObject()
	default:
		panic(fmt.Errorf("unsupported BSON data type ID %T for binary deserialization", tp))
	}
}

func (s *bsonDeserializer) deserializeArray() (a []interface{}) {
	l := int(s.buf.Read16())
	a = make([]interface{}, l, l)
	for i := range a {
		a[i] = s.deserializeType()
	}
	return
}

func (s *bsonDeserializer) deserializeObject() (m map[string]interface{}) {
	m = make(map[string]interface{})
	l := int(s.buf.Read16())
	for i := 0; i < l; i++ {
		k := s.buf.ReadString()
		v := s.deserializeType()
		m[k] = v
	}
	return
}
