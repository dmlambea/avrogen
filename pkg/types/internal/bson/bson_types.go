package bson

import "fmt"

type bsonTypeID int

const (
	btInvalid bsonTypeID = iota
	btNull
	btBooleanFalse
	btBooleanTrue
	btInt32
	btInt64
	btFloat32
	btFloat64
	btString
	btArray
	btObject
)

func (b bsonTypeID) String() string {
	switch b {
	case btInvalid:
		return "<invalid type ID>"
	case btNull:
		return "null"
	case btBooleanFalse:
		return "boolean (false)"
	case btBooleanTrue:
		return "boolean (true)"
	case btInt32:
		return "int32"
	case btInt64:
		return "int64"
	case btFloat32:
		return "float32"
	case btFloat64:
		return "float64"
	case btString:
		return "string"
	case btObject:
		return "object"
	default:
		return fmt.Sprintf("unknown-%d", int(b))
	}
}
