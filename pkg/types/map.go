package types

// NewMap creates a new map type for items of type itemType
func NewMap(itemType Generic) *Map {
	t := &Map{}
	t.setItemType(itemType)
	t.typeName = "map"
	t.itemTypeName = "values"
	return t
}

var (
	// Ensure interface implementations
	_ Complex     = &Map{}
	_ Composite   = &Map{}
	_ SingleChild = &Map{}
)

// Map defines an Avro map type.
type Map struct {
	singleChildComponent
}

// IsReadableBy returns true if this map can be read by the type `other`
func (t *Map) IsReadableBy(other Generic, visited VisitMap) bool {
	if m, ok := other.(*Map); ok {
		return t.Type().IsReadableBy(m.Type(), visited)
	}
	return tryIsReadableByUnion(t, other, visited)
}

func (t *Map) typeID() binaryTypeID {
	return tMap
}
