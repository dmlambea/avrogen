package types

import (
	"bytes"
	"fmt"
	"math"

	"gitlab.com/dmlambea/avrogen/pkg/types/internal/bin"
	"gitlab.com/dmlambea/avrogen/pkg/types/internal/bson"
)

// Deserialize reads and returns the type t from its binary form in reader r.
func Deserialize(r *bytes.Buffer) (t Generic, err error) {
	defer func() {
		r := recover()
		if r != nil {
			t = nil
			err = fmt.Errorf("unable to deserialize type: %v", r)
		}
	}()
	d := &binaryDeserializer{
		r:      bin.NewFromBuffer(r),
		ref:    make(map[int]Generic),
		marker: int(tNamedTypesMarker),
	}
	t = d.deserializeType()
	return
}

type binaryDeserializer struct {
	r      *bin.Buffer
	ref    map[int]Generic
	marker int
}

func (d *binaryDeserializer) register(t Generic) {
	d.ref[d.marker] = t
	d.marker++
}

func (d *binaryDeserializer) deserializeType() Generic {
	id := binaryTypeID(d.r.Read16())
	switch id {
	case tNull, tBoolean, tInt, tLong, tFloat, tDouble, tBytes, tString:
		return newPrimitiveFromTypeID(id)
	case tEnum, tFixed, tRecord:
		return d.deserializeNamedType(id)
	case tArray, tMap:
		return d.deserializeSingleChildType(id)
	case tUnion:
		return d.deserializeUnion()
	default:
		if t, ok := d.ref[int(id)]; ok {
			return t
		}
		panic(fmt.Errorf("unexpected type ID %s while deserializing", id))
	}
}

// deserializeNamedType reads the named type t from its binary form as it
// corresponds for the given type ID. A named type can have aliases.
func (d *binaryDeserializer) deserializeNamedType(id binaryTypeID) (t Generic) {
	n := d.r.ReadString()
	l := d.r.Read8()
	var aliases []string
	if l > 0 {
		aliases = make([]string, l)
		for i := range aliases {
			aliases[i] = d.r.ReadString()
		}
	}
	switch id {
	case tEnum:
		t = d.deserializeEnumBody(n)
		d.register(t)
	case tFixed:
		t = d.deserializeFixedBody(n)
		d.register(t)
	case tRecord:
		t = NewRecord(n)
		d.register(t)
		d.deserializeRecordBody(t.(*Record))
	}
	t.(Named).SetAliases(aliases) // Aliases have to be set asap
	return
}

// deserializeSingleChildType deserializes the subtype for the single-child
// type "id" and returns the proper main type.
func (d *binaryDeserializer) deserializeSingleChildType(id binaryTypeID) (t Generic) {
	child := d.deserializeType()
	switch id {
	case tArray:
		return NewArray(child)
	case tMap:
		return NewMap(child)
	}
	panic(fmt.Errorf("unexpected type ID %s", id))
}

// deserializeEnumBody reads the enum type t from its binary form as documented in
// the serializeEnum method.
func (d *binaryDeserializer) deserializeEnumBody(n string) (t *Enum) {
	l := d.r.Read16()
	s := make([]string, l)
	for i := range s {
		s[i] = d.r.ReadString()
	}
	t = NewEnum(n, s)
	defIdx := d.r.Read16()
	if defIdx > 0 {
		t.SetDefault(s[defIdx-1])
	}
	return
}

// deserializeFixedBody reads the rest of fields that define a fixed type.
func (d *binaryDeserializer) deserializeFixedBody(n string) *Fixed {
	s := d.r.Read64()
	return NewFixed(n, uint64(s))
}

// deserializeRecordBody reads the rest of fields that define a record type
func (d *binaryDeserializer) deserializeRecordBody(r *Record) {
	l := d.r.Read16()
	if l == 0 {
		return
	}
	children := make([]Generic, l)
	for i := range children {
		n := d.r.ReadString()
		c := d.deserializeType()
		f := NewField(n, c, i)

		d.deserializeDefaultForField(f)

		children[i] = f
	}
	r.SetFields(children)
}

// deserializeDefaultForField reads the field's default value from its
// binary form, accordingly to its type. Fields with no default values
// are ignored.
func (d *binaryDeserializer) deserializeDefaultForField(f *Field) {
	defType := defaultValueType(d.r.Read8())
	if defType == defNone {
		return
	}

	// Resolve null/bool fields first
	f.SetDefault(d.deserializeValueForType(defType, f.Type()))
}

// deserializeValueForType reads a value given its type. The deserialized
// value type must be provided, since some fields (boolean and null) are
// "compacted" in its type indicator.
func (d *binaryDeserializer) deserializeValueForType(defType defaultValueType, t Generic) interface{} {
	var val interface{}
	switch defType {
	case defNull:
		id := t.typeID()
		switch id {
		case tArray:
			id = t.(*Array).Children()[0].typeID()
		case tMap:
			id = t.(*Map).Children()[0].typeID()
		case tUnion:
			id = t.(*Union).Children()[0].(*Field).Type().typeID()
		}
		switch {
		case id == tNull:
			return nil
		case id == tBoolean:
			return false
		default:
			panic(fmt.Errorf("invalid default type ID %s for type %T", defType, t))
		}
	case defTrue:
		return true
	case def32Bit:
		val = d.r.Read32()
	case def64Bit:
		val = d.r.Read64()
	case defString:
		val = d.r.ReadString()
	case defArray:
		l := d.r.Read32()
		arr := make([]interface{}, l)
		for i := range arr {
			itemDefType := defaultValueType(d.r.Read8())
			arr[i] = d.deserializeValueForType(itemDefType, t)
		}
		return arr
	case defMap:
		l := d.r.Read32()
		m := make(map[string]interface{}, l)
		for i := int32(0); i < l; i++ {
			key := d.r.ReadString()
			itemDefType := defaultValueType(d.r.Read8())
			m[key] = d.deserializeValueForType(itemDefType, t)
		}
		return m
	case defJSON:
		m, err := bson.DeserializeFromBuffer(d.r)
		if err != nil {
			panic(fmt.Errorf("unable to deserialize default value of record type %s: %v", t.(*Record).Name(), err))
		}
		return m
	default:
		panic(fmt.Errorf("invalid default type ID %s for type %T", defType, t))
	}

	// Fix numeric field types
	switch t.typeID() {
	case tFloat:
		val = math.Float32frombits(uint32(val.(int32)))
	case tDouble:
		val = math.Float64frombits(uint64(val.(int64)))
	}
	return val
}

// deserializeUnion reads the enum type t from its binary form as documented in
// the serializeUnion method.
func (d *binaryDeserializer) deserializeUnion() *Union {
	l := d.r.Read8()
	children := make([]Generic, l)
	for i := range children {
		child := d.deserializeType()
		children[i] = NewField("", child, i)
	}
	return NewUnion(children)
}
