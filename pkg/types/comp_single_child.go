package types

import (
	"fmt"
)

var (
	// Ensure interface implementations
	_ Complex     = &singleChildComponent{}
	_ Composite   = &singleChildComponent{}
	_ SingleChild = &singleChildComponent{}
)

type singleChildComponent struct {
	multiChildComponent
	typeName     string
	itemTypeName string
}

func (comp *singleChildComponent) String() string {
	return fmt.Sprintf("%s of %s", comp.typeName, comp.Type())
}

func (comp *singleChildComponent) Type() Generic {
	return comp.Children()[0]
}

func (comp *singleChildComponent) resolveSchema(m VisitMap) interface{} {
	parts := make(map[string]interface{})
	parts["type"] = comp.typeName
	parts[comp.itemTypeName] = comp.Type().resolveSchema(m)
	return parts
}

func (comp *singleChildComponent) setItemType(itemType Generic) {
	comp.multiChildComponent.setItemTypes([]Generic{itemType})
}

func (comp *singleChildComponent) setItemTypes(itemTypes []Generic) {
	panic(fmt.Sprintf("%v is a single-child type and it has a method for setting just one child type", comp))
}
