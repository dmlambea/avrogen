package types

// VisitMap is a map used to detect circular references in schema or type resolutions.
type VisitMap map[string]bool
