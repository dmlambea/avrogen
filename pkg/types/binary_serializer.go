package types

import (
	"bytes"
	"fmt"

	"gitlab.com/dmlambea/avrogen/pkg/types/internal/bin"
	"gitlab.com/dmlambea/avrogen/pkg/types/internal/bson"
)

// Serialize writes the type t in its binary form to writer w. All non-byte
// types are written in little-endian.
func Serialize(w *bytes.Buffer, t Generic) (err error) {
	defer func() {
		r := recover()
		if r != nil {
			err = fmt.Errorf("unable to serialize type %T: %v", t, r)
		}
	}()
	s := &binarySerializer{
		w:      bin.NewFromBuffer(w),
		ref:    make(map[string]int),
		marker: int(tNamedTypesMarker),
	}
	s.serializeType(t)
	return
}

type binarySerializer struct {
	w      *bin.Buffer
	ref    map[string]int
	marker int
}

// serializeType writes the generic type t in binary form.
// All types begin with their type ID (2 bytes).
func (s *binarySerializer) serializeType(t Generic) {
	switch t.(type) {
	case *Primitive:
		// Type ID was all required for a primitive type
		s.w.Write16(int(t.typeID()))
	case *Enum, *Fixed, *Record:
		s.serializeNamedType(t)
	case *Array, *Map:
		s.w.Write16(int(t.typeID()))
		s.serializeType(t.(SingleChild).Type())
	case *Union:
		s.serializeUnion(t.(*Union))
	default:
		panic(fmt.Errorf("binary serialization of type %T is unsupported yet", t))
	}
}

// serializeNamedType writes the named type t in binary form.
// If it is the first time this named type is serialized:
//  - type ID (2 bytes)
//  - rest of fields, for the given type (name, aliases...)
// Otherwise:
//  - named type ref (2 bytes)
// Where the named type ref is a number starting with the highest type ID for
// the base types, which is incremented each time a new named type is
// serialized for its first time.
func (s *binarySerializer) serializeNamedType(t Generic) {
	n := t.(Named)
	if idx, ok := s.ref[n.Name()]; ok {
		s.w.Write16(idx)
		return
	}
	s.ref[n.Name()] = s.marker
	s.marker++

	s.w.Write16(int(t.typeID()))

	// Named types follow with their fqname (string: 2 bytes + len bytes)
	// and the list of aliases (byte count + strings)
	s.w.WriteString(n.Name())
	s.w.Write8(byte(len(n.Aliases())))
	for _, a := range n.Aliases() {
		s.w.WriteString(a)
	}

	switch t.typeID() {
	case tEnum:
		s.serializeEnumBody(t.(*Enum))
	case tFixed:
		s.serializeFixedBody(t.(*Fixed))
	case tRecord:
		s.serializeRecordBody(t.(*Record))
	default:
		panic(fmt.Errorf("invalid type ID %s for type %T", t.typeID(), t))
	}
	return
}

// serializeEnumBody is the particular case for enums, that serialize the following:
// - symbol count (2 bytes)
// - for each symbol:
//    - symbol name (string)
// - default symbol index (1-based)
func (s *binarySerializer) serializeEnumBody(t *Enum) {
	s.w.Write16(len(t.Symbols()))
	defIdx := -1
	for i, sym := range t.Symbols() {
		s.w.WriteString(sym)
		if t.HasDefault() && sym == t.Default() {
			defIdx = i
		}
	}
	s.w.Write16(defIdx + 1)
}

// serializeFixedBody handles the particular case for fixed types:
// - size bytes (8 bytes)
func (s *binarySerializer) serializeFixedBody(t *Fixed) {
	s.w.Write64(int64(t.SizeBytes()))
}

// serializeRecordBody is the particular case for records, that serialize the following:
// - field count (2 bytes)
// - for each field:
//    - field name (string)
//    - field type (depends on the type)
//    - default value type indicator (byte)
//       - default value, if any
func (s *binarySerializer) serializeRecordBody(t *Record) {
	s.w.Write16(len(t.Children()))
	for _, c := range t.Children() {
		f := c.(*Field)
		s.w.WriteString(f.Name())
		s.serializeType(f.Type())
		s.serializeDefaultForField(f)
	}
	return
}

// serializeDefaultForField writes the field's default value, if any, in
// binary form, accordingly to its type. Fields with no default values or
// boolean fields with value 'false' are serialized as defNone.
func (s *binarySerializer) serializeDefaultForField(f *Field) {
	if !f.HasDefault() {
		s.w.Write8(byte(defNone))
		return
	}
	s.serializeValueForType(f.Type(), f.Default())
}

// serializeValueForType writes the value following the type's binary schema.
func (s *binarySerializer) serializeValueForType(t Generic, v interface{}) {
	switch t.typeID() {
	case tNull:
		s.w.Write8(byte(defNull))
	case tBoolean:
		switch v {
		case false:
			s.w.Write8(byte(defNull))
		case true:
			s.w.Write8(byte(defTrue))
		}
	case tInt:
		s.w.Write8(byte(def32Bit))
		s.w.Write32(v.(int32))
	case tFloat:
		s.w.Write8(byte(def32Bit))
		s.w.WriteFloat32(v.(float32))
	case tLong:
		s.w.Write8(byte(def64Bit))
		s.w.Write64(v.(int64))
	case tDouble:
		s.w.Write8(byte(def64Bit))
		s.w.WriteFloat64(v.(float64))
	case tBytes, tString, tEnum, tFixed:
		s.w.Write8(byte(defString))
		s.w.WriteString(v.(string))
	case tArray:
		s.w.Write8(byte(defArray))
		arr := v.([]interface{})
		arrType := t.(*Array).Type()
		s.w.Write32(int32(len(arr)))
		for _, item := range arr {
			s.serializeValueForType(arrType, item)
		}
	case tMap:
		s.w.Write8(byte(defMap))
		m := v.(map[string]interface{})
		mType := t.(*Map).Type()
		s.w.Write32(int32(len(m)))
		for key, item := range m {
			s.w.WriteString(key)
			s.serializeValueForType(mType, item)
		}
	case tRecord:
		s.w.Write8(byte(defJSON))
		m := v.(map[string]interface{})
		if err := bson.SerializeToBuffer(m, s.w); err != nil {
			panic(fmt.Errorf("unable to serialize default value of record type %s: %v", t.(*Record).Name(), err))
		}
	case tUnion:
		f := t.(*Union).Children()[0].(*Field)
		s.serializeValueForType(f.Type(), v)
	default:
		panic(fmt.Errorf("unable to serialize default value of type ID %s for type %T", t.typeID(), t))
	}
}

// serializeUnion is the particular case for unions, that serialize the
// following:
// - type ID (byte)
// - child types count (byte)
// - for each child, its type serialization
func (s *binarySerializer) serializeUnion(t *Union) {
	s.w.Write16(int(t.typeID()))
	s.w.Write8(byte(len(t.Children())))
	for _, c := range t.Children() {
		s.serializeType(c.(*Field).Type())
	}
	return
}
