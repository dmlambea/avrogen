package types

import "fmt"

type binaryTypeID int

type defaultValueType byte

const (
	// Binary type identifier
	tInvalid binaryTypeID = iota
	tNull
	tBoolean
	tInt
	tLong
	tFloat
	tDouble
	tBytes
	tString
	tEnum
	tFixed
	tRecord
	tArray
	tMap
	tUnion

	// This constant value marks the beginning index for the named types that
	// need to be serialized more than once.
	tNamedTypesMarker

	// Default value type identifier
	defNone   defaultValueType = iota // No default value, equivalent to 0, false and null for the respective types
	defNull                           // Null/false value
	defTrue                           // True value
	def32Bit                          // 32Bit value
	def64Bit                          // 64Bit value
	defString                         // String value
	defArray                          // Array value
	defMap                            // Map value
	defJSON                           // JSON value
)

func (b binaryTypeID) String() string {
	switch b {
	case tInvalid:
		return "<invalid type ID>"
	case tNull:
		return "null"
	case tBoolean:
		return "boolean"
	case tInt:
		return "int"
	case tLong:
		return "long"
	case tFloat:
		return "float"
	case tDouble:
		return "double"
	case tBytes:
		return "bytes"
	case tString:
		return "string"
	case tEnum:
		return "enum"
	case tFixed:
		return "fixed"
	case tRecord:
		return "record"
	case tArray:
		return "array"
	case tMap:
		return "map"
	case tUnion:
		return "union"
	default:
		return fmt.Sprintf("ref-%d", int(b))
	}
}

func (d defaultValueType) String() string {
	switch d {
	case defNone:
		return "<no default value>"
	case defNull:
		return "null/false"
	case defTrue:
		return "true"
	case def32Bit:
		return "32-bit numeric value"
	case def64Bit:
		return "64-bit numeric value"
	case defString:
		return "string"
	case defArray:
		return "array"
	case defMap:
		return "map"
	case defJSON:
		return "JSON data"
	}
	panic(fmt.Errorf("unsupported default value type %d in String()", int(d)))
}
