package types

// NewFixed creates a Fixed type named `name` with the given size.
func NewFixed(name string, sizeBytes uint64) *Fixed {
	t := &Fixed{sizeBytes: sizeBytes}
	t.setName(name)
	return t
}

var (
	// Ensure interface implementations
	_ Complex = &Fixed{}
	_ Named   = &Fixed{}
)

// Fixed defines an Avro fixed type.
type Fixed struct {
	nameComponent
	sizeBytes uint64
}

// SizeBytes returns the width of this fixed type in bytes.
func (t *Fixed) SizeBytes() uint64 {
	return t.sizeBytes
}

// IsReadableBy returns true if `other` is a Fixed type with the same size in bytes.
func (t *Fixed) IsReadableBy(other Generic, visited VisitMap) bool {
	f, ok := other.(*Fixed)
	if ok {
		ok = (f.Name() == t.Name() && f.SizeBytes() == t.SizeBytes())
	}
	return ok
}

func (t *Fixed) assertComplex() {}

func (t *Fixed) resolveSchema(m VisitMap) interface{} {
	n := t.Name()
	if m[n] {
		return n
	}
	m[n] = true

	parts := t.schemaParts()
	parts["type"] = "fixed"
	parts["size"] = t.SizeBytes()
	return parts
}

func (t *Fixed) typeID() binaryTypeID {
	return tFixed
}
