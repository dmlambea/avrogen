package types

// NewArray creates a new Array type
func NewArray(itemType Generic) *Array {
	t := &Array{}
	t.setItemType(itemType)
	t.typeName = "array"
	t.itemTypeName = "items"
	return t
}

var (
	// Ensure interface implementations
	_ Complex     = &Array{}
	_ Composite   = &Array{}
	_ SingleChild = &Array{}
)

// Array is the type defining an Avro array type.
type Array struct {
	singleChildComponent
}

// IsReadableBy returns true if this array can be read by the type `other`
func (t *Array) IsReadableBy(other Generic, visited VisitMap) bool {
	if a, ok := other.(*Array); ok {
		return t.Type().IsReadableBy(a.Type(), visited)
	}
	return tryIsReadableByUnion(t, other, visited)
}

func (t *Array) typeID() binaryTypeID {
	return tArray
}
