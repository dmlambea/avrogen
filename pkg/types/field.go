package types

import (
	"fmt"
)

// NewField creates a field for the given name and the given field type. Field
// index must be also informed.
func NewField(name string, itemType Generic, index int) *Field {
	t := &Field{index: index}
	t.setName(name)
	t.setItemType(itemType)
	return t
}

var (
	// Ensure interface implementations
	_ DefaultValued = &Field{}
	_ Documented    = &Field{}
	_ Named         = &Field{}
)

// Field defines a Record field or Union child type.
type Field struct {
	defaultValuedComponent
	documentComponent
	nameComponent
	singleChildComponent
	index int
}

// Index returns the index for this field.
func (t *Field) Index() int {
	return t.index
}

// String returns this field's name
func (t *Field) String() string {
	return t.nameComponent.String()
}

// Default returns the default value for this field, if any.
func (t *Field) Default() interface{} {
	defaultValuedType, ok := t.Type().(DefaultValued)
	switch {
	case t.defaultValueSet == true:
		return t.defaultValue
	case ok:
		return defaultValuedType.Default()
	default:
		panic(fmt.Sprintf("field %s has no default value", t.Name()))
	}
}

// SetDefault overrides the default value's type for numerics
func (t *Field) SetDefault(value interface{}) {
	value = t.CastValue(value)
	t.defaultValuedComponent.SetDefault(value)
}

// HasDefault returns true if this field has a default value.
func (t *Field) HasDefault() bool {
	defaultValuedType, ok := t.Type().(DefaultValued)
	switch {
	case t.defaultValueSet == true:
		return true
	case ok:
		return defaultValuedType.HasDefault()
	default:
		return false
	}
}

// CastValue casts the given numeric value to the type expected by this
// field's type. This is a convenience function to overcome the fact that all
// numeric fields deserialized by the json package are treated as float64.
// Non-numeric and non-float64 values are returned as is.
func (t *Field) CastValue(value interface{}) interface{} {
	if _, ok := value.(float64); ok {
		switch t.Type().typeID() {
		case tInt:
			value = int32(value.(float64))
		case tLong:
			value = int64(value.(float64))
		case tFloat:
			value = float32(value.(float64))
		}
	}
	return value
}

func (t *Field) resolveSchema(m VisitMap) interface{} {
	parts := t.schemaParts()
	parts["type"] = t.Type().resolveSchema(m)
	if t.HasDefault() {
		parts["default"] = t.Default()
	}
	return parts
}

// IsReadableBy returns true if this field can be read by the type `other`
func (t *Field) IsReadableBy(other Generic, visited VisitMap) bool {
	if fld, ok := other.(*Field); ok {
		return t.Type().IsReadableBy(fld, visited)
	}
	return t.Type().IsReadableBy(other, visited)
}

// TODO check if the names should always bo compared over their public versions
func (t *Field) alsoKnownAs(aka string) bool {
	if aka == t.Name() {
		return true
	}
	for _, alias := range t.Aliases() {
		if aka == alias {
			return true
		}
	}
	return false
}

func (t *Field) typeID() binaryTypeID {
	panic("field types don't have type IDs")
}
