package types

// Generic is the interface defining all types' methods
type Generic interface {
	IsReadableBy(other Generic, visited VisitMap) bool
	String() string
	resolveSchema(VisitMap) interface{}
	typeID() binaryTypeID
}
