package types

// SingleChild is the interface defining types with an associated, inner
// type, like maps and arrays.
type SingleChild interface {
	Type() Generic
}
