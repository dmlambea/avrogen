package parser

func stringFromMap(m map[string]interface{}, key string) (string, error) {
	val, ok := m[key]
	if !ok {
		return "", newRequiredMapKeyError(key)
	}
	if typedVal, ok := val.(string); ok {
		return typedVal, nil
	}
	return "", newWrongMapValueTypeError(key, "string", val)
}

func arrayFromMap(m map[string]interface{}, key string) ([]interface{}, error) {
	val, ok := m[key]
	if !ok {
		return nil, newRequiredMapKeyError(key)
	}
	if typedVal, ok := val.([]interface{}); ok {
		return typedVal, nil
	}
	return nil, newWrongMapValueTypeError(key, "array", val)
}

func floatFromMap(m map[string]interface{}, key string) (float64, error) {
	val, ok := m[key]
	if !ok {
		return 0, newRequiredMapKeyError(key)
	}
	if typedVal, ok := val.(float64); ok {
		return typedVal, nil
	}
	return 0, newWrongMapValueTypeError(key, "float", val)
}
