package parser

import (
	"fmt"
)

/*
type schemaError struct {
	FieldName   string
	NestedError error
}

func newSchemaError(fieldName string, err error) *schemaError {
	fullName := fieldName
	nestedErr := err
	if schemaErr, ok := err.(*SchemaError); ok {
		fullName = fieldName + "." + schemaErr.FieldName
		nestedErr = schemaErr.NestedError
	}
	return &schemaError{
		FieldName:   fullName,
		NestedError: nestedErr,
	}
}

func (s *schemaError) Error() string {
	return fmt.Sprintf("Error parsing schema for field %q: %v", s.FieldName, s.NestedError)
}
*/

type wrongMapValueTypeError struct {
	Key          string
	ExpectedType string
	ActualValue  interface{}
}

func newWrongMapValueTypeError(key, expectedType string, actualValue interface{}) *wrongMapValueTypeError {
	return &wrongMapValueTypeError{
		Key:          key,
		ExpectedType: expectedType,
		ActualValue:  actualValue,
	}
}

func (w *wrongMapValueTypeError) Error() string {
	return fmt.Sprintf("Wrong type for map key %q: expected type %v, got value %q of type %t", w.Key, w.ExpectedType, w.ActualValue, w.ActualValue)
}

type requiredMapKeyError struct {
	Key string
}

func newRequiredMapKeyError(key string) *requiredMapKeyError {
	return &requiredMapKeyError{
		Key: key,
	}
}

func (r *requiredMapKeyError) Error() string {
	return fmt.Sprintf("No value supplied for required map key %q", r.Key)
}
