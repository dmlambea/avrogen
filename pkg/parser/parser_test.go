package parser

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestEnum(t *testing.T) {
	jsonString := `{
		"type": "enum",
		"name": "EnumTest",
		"symbols": ["TestSymbol1", "testSymbol2", "testSymbol3"],
		"aliases": ["e", "com.acme.Enum"]
	}`
	tp, err := ParseSchema(jsonString)
	require.Nil(t, err)
	require.NotNil(t, tp)
}

func TestFixed(t *testing.T) {
	jsonString := `{
		"type": "fixed",
		"name": "FixedTest",
		"size": 16,
		"aliases": ["f", "com.acme.Fixed"]
	}`
	tp, err := ParseSchema(jsonString)
	require.Nil(t, err)
	require.NotNil(t, tp)
}

func TestMap(t *testing.T) {
	jsonString := `{
		"type": "map",
		"values": {
			"type": "enum",
			"name": "MapEnumTest",
			"symbols": ["TestSymbol1", "testSymbol2", "testSymbol3"],
			"aliases": ["e", "com.acme.Enum"]
		}
	}`
	tp, err := ParseSchema(jsonString)
	require.Nil(t, err)
	require.NotNil(t, tp)
}

func TestArray(t *testing.T) {
	jsonString := `{
		"type": "array",
		"items": {
			"type": "enum",
			"name": "ArrayEnumTest",
			"symbols": ["TestSymbol1", "testSymbol2", "testSymbol3"],
			"aliases": ["e", "com.acme.Enum"]
		}
	}`
	tp, err := ParseSchema(jsonString)
	require.Nil(t, err)
	require.NotNil(t, tp)
}

func TestRecord(t *testing.T) {
	jsonString := `{
		"type" : "record",
		"name" : "AliasRecord",
		"fields" : [ 
		{
			"name": "a",
			"type": {
				"type": "enum",
				"name": "ArrayEnumTest",
				"symbols": ["TestSymbol1", "testSymbol2", "testSymbol3"],
				"aliases": ["e", "com.acme.Enum"]
			}
		},
		{
			"name": "c",
			"aliases": ["d"],
			"type": {
				"type": "array",
				"items": "com.acme.Enum"
			}
		}
		]
	}`
	tp, err := ParseSchema(jsonString)
	require.Nil(t, err)
	require.NotNil(t, tp)
}

func TestAliasedRecord(t *testing.T) {
	jsonString := `{
	"type" : "record",
	"name" : "AliasedRecord",
	"fields" : [{
		"name": "MasterField",
		"type": {
			"type": "record",
			"name": "MasterRecord",
			"aliases": [
				"aliasedRecord"
			],
			"fields": [{
				"name": "StringField",
				"type": "string"
			}, {
				"name": "BoolField",
				"type": "boolean"
			}, {
				"name": "BytesField",
				"type": "bytes"
			}]
		}
	},{
		"type": "aliasedRecord",
		"name": "OtherField"
	}]
}`
	tp, err := ParseSchema(jsonString)
	require.Nil(t, err)
	require.NotNil(t, tp)
}
