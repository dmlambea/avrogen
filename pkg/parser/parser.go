package parser

import (
	"errors"
	"fmt"
	"strings"

	jsoniter "github.com/json-iterator/go"
	"gitlab.com/dmlambea/avrogen/pkg/types"
)

// ParseSchema accepts an Avro schema as a JSON string, parses it and returns
// back the resulting root object, or an error if any occurs.
func ParseSchema(jsonSchema string) (types.Generic, error) {
	t, err := GetParseTree(jsonSchema)
	return t.TopNode, err
}

// GetParseTree accepts an Avro schema as a JSON string, parses it and returns
// back the resulting Tree object, or an error if any occurs.
func GetParseTree(jsonSchema string) (tree Tree, err error) {
	var schema interface{}
	json := jsoniter.ConfigCompatibleWithStandardLibrary
	if err = json.Unmarshal([]byte(jsonSchema), &schema); err != nil {
		return
	}

	p := parser{
		registry: make(map[string]types.Generic),
		tree:     &Tree{},
	}
	// Start the parsing with the default, empty namespace
	p.tree.TopNode, err = p.decodeType("", schema)
	return *p.tree, err
}

// Tree holds the top node of the parse tree for a given schema and the list
// of the complex types found during parse. This list includes the top node as
// well.
type Tree struct {
	TopNode      types.Generic
	ComplexTypes []types.Generic
}

func (t *Tree) addComplex(ct types.Generic) {
	t.ComplexTypes = append(t.ComplexTypes, ct)
}

const (
	// Special Go type for null types
	nullGoType = ""
)

// parser holds the parse tree along with the registry for resolving
// references. This is a one-time structure that gets discarded after
// finishing the parse.
type parser struct {
	registry map[string]types.Generic
	tree     *Tree
}

// registerComplex registers the given type as complex type. All complex types need a Go source file
// to be generated for them.
func (p parser) registerComplex(t types.Generic) {
	if _, ok := t.(types.Complex); ok {
		p.tree.addComplex(t)
	}
}

// registerType tracks the given type by all its names (name and aliases), so
// any later reference to it can be resolved.
func (p parser) registerType(t types.Generic) {
	n := t.(types.Named)
	p.registry[n.Name()] = t
	for _, alias := range n.Aliases() {
		p.registry[alias] = t
	}
}

// decodeType is the generic method to decode any type, given its types.
func (p parser) decodeType(namespace string, typeSchema interface{}) (types.Generic, error) {
	switch t := typeSchema.(type) {
	case map[string]interface{}:
		return p.decodeComplex(namespace, typeSchema.(map[string]interface{}))
	case []interface{}:
		// Non-simple Unions must be also added to the complex types registry.
		union, err := p.decodeUnion(namespace, typeSchema.([]interface{}))
		if err == nil && !union.(*types.Union).IsOptionalSimple() {
			p.registerComplex(union)
		}
		return union, err
	case string:
		name := typeSchema.(string)
		return p.getTypeByName(name, namespace)
	default:
		return nil, fmt.Errorf("decoding of type %s is unimplemented", t)
	}
}

// decodeComplex decodes and registers a fixed, enum, map, array, record or qnamed reference complex type
func (p parser) decodeComplex(namespace string, schemaMap map[string]interface{}) (types.Generic, error) {
	typeStr, err := stringFromMap(schemaMap, "type")
	if err != nil {
		return nil, err
	}
	var at types.Generic
	switch typeStr {
	case "enum", "fixed", "record":
		at, err = p.decodeNamed(typeStr, namespace, schemaMap)
	case "map":
		at, err = p.decodeMap(namespace, schemaMap)
	case "array":
		at, err = p.decodeArray(namespace, schemaMap)
	default:
		// If the type isn't a special case, it's a primitive or a reference to an existing type
		return p.getTypeByName(typeStr, namespace)
	}
	if err != nil {
		return nil, err
	}
	p.registerComplex(at)
	return at, nil
}

// decodeNamed decodes and registers a named type (fixed, enum, or record).
func (p parser) decodeNamed(typeStr, namespace string, schemaMap map[string]interface{}) (types.Generic, error) {
	qname, err := extractQName(schemaMap, namespace)
	if err != nil {
		return nil, err
	}
	// Update the namespace
	namespace = extractNamespace(qname)

	var namedType types.Generic
	switch typeStr {
	case "enum":
		namedType, err = p.decodeEnum(qname, namespace, schemaMap)
	case "fixed":
		namedType, err = p.decodeFixed(qname, namespace, schemaMap)
	case "record":
		namedType = types.NewRecord(qname)
	default:
		// If the type isn't a special case, it's a primitive or a reference to an existing type
		return p.getTypeByName(typeStr, namespace)
	}
	if err != nil {
		return nil, err
	}

	if err = parseAliases(schemaMap, namedType.(types.Named), namespace); err != nil {
		return nil, err
	}
	p.registerType(namedType)

	if typeStr == "record" {
		if namedType, err = p.decodeRecord(qname, namespace, namedType.(*types.Record), schemaMap); err != nil {
			return nil, err
		}
	}

	return namedType, nil
}

// decodeEnum accepts a namespace and a map representing an enum definition,
// it validates the definition and build the enum type struct.
func (p parser) decodeEnum(qname, namespace string, schemaMap map[string]interface{}) (types.Generic, error) {
	symbolsSlice, err := arrayFromMap(schemaMap, "symbols")
	if err != nil {
		return nil, err
	}

	symbols, ok := interfaceSliceToStringSlice(symbolsSlice)
	if !ok {
		return nil, errors.New("'symbols' must be an array of strings")
	}

	enum := types.NewEnum(qname, symbols)
	if err = parseDefault(schemaMap, enum); err != nil {
		return nil, err
	}
	if err = parseDoc(schemaMap, enum); err != nil {
		return nil, err
	}
	return enum, nil
}

// decodeFixed accepts a namespace and a map representing a fixed type definition,
// it validates the definition and build the fixed type struct.
func (p parser) decodeFixed(qname, namespace string, schemaMap map[string]interface{}) (types.Generic, error) {
	sizeBytes, err := floatFromMap(schemaMap, "size")
	if err != nil {
		return nil, err
	}
	if sizeBytes < 0 {
		return nil, errors.New("'size' must be a positive integer")
	}

	fixed := types.NewFixed(qname, uint64(sizeBytes))
	return fixed, nil
}

// decodeRecord accepts a namespace and a map representing a fixed type definition,
// it validates the definition and build the record type struct.
func (p parser) decodeRecord(qname, namespace string, record *types.Record, schemaMap map[string]interface{}) (types.Generic, error) {
	fields, err := arrayFromMap(schemaMap, "fields")
	if err != nil {
		return nil, err
	}

	decodedFields := make([]types.Generic, len(fields))
	for i, f := range fields {
		fieldSchemaMap, ok := f.(map[string]interface{})
		if !ok {
			return nil, newWrongMapValueTypeError("fields", "map[]", fields)
		}

		fieldName, err := stringFromMap(fieldSchemaMap, "name")
		if err != nil {
			return nil, err
		}

		fieldType, err := p.decodeType(namespace, fieldSchemaMap["type"])
		if err != nil {
			return nil, err
		}

		decodedField := types.NewField(fieldName, fieldType, i)

		// Record fields have no namespaces
		if err = parseAliases(fieldSchemaMap, decodedField, namespace); err != nil {
			return nil, err
		}
		if err = parseDefault(fieldSchemaMap, decodedField); err != nil {
			return nil, err
		}
		if err = parseDoc(fieldSchemaMap, decodedField); err != nil {
			return nil, err
		}

		decodedFields[i] = decodedField

		/**  TODO: support golang tags
		var fieldTags string
		if tags, ok := field["golang.tags"]; ok {
			fieldTags, ok = tags.(string)
			if !ok {
				return nil, NewWrongMapValueTypeError("golang.tags", "string", tags)
			}
		}
		*/
	}

	record.SetFields(decodedFields)
	if err = parseDoc(schemaMap, record); err != nil {
		return nil, err
	}

	return record, nil
}

// decodeMap accepts a namespace and a map representing a fixed type definition,
// it validates the definition and build the map type struct.
func (p parser) decodeMap(namespace string, schemaMap map[string]interface{}) (types.Generic, error) {
	values, ok := schemaMap["values"]
	if !ok {
		return nil, newRequiredMapKeyError("values")
	}

	itemType, err := p.decodeType(namespace, values)
	if err != nil {
		return nil, err
	}

	return types.NewMap(itemType), nil
}

// decodeArray accepts a namespace and a map representing a fixed type definition,
// it validates the definition and build the array type struct.
func (p parser) decodeArray(namespace string, schemaMap map[string]interface{}) (types.Generic, error) {
	items, ok := schemaMap["items"]
	if !ok {
		return nil, newRequiredMapKeyError("items")
	}

	itemType, err := p.decodeType(namespace, items)
	if err != nil {
		return nil, err
	}

	return types.NewArray(itemType), nil
}

// decodeUnion accepts a namespace and a map representing a union type definition,
// it validates the definition and build the union type struct.
func (p parser) decodeUnion(namespace string, schemaList []interface{}) (types.Generic, error) {
	decodedFields := make([]types.Generic, len(schemaList))
	for i, f := range schemaList {
		fieldType, err := p.decodeType(namespace, f)
		if err != nil {
			return nil, err
		}
		decodedFields[i] = types.NewField("", fieldType, i)
	}

	return types.NewUnion(decodedFields), nil
}

// getTypeByName returns the type associated with a type name, mostly primitive types, but also qnamed, registered types.
func (p parser) getTypeByName(typeStr, namespace string) (t types.Generic, err error) {
	// Beware the nil interfaces
	if p := types.NewPrimitive(typeStr); p != nil {
		t = p
		return
	}
	// Non-primitive type: create a reference
	name := fixQName(typeStr, namespace)
	var ok bool
	if t, ok = p.registry[name]; !ok {
		err = fmt.Errorf("unknown type '%s': has it been defined before?", name)
	}
	return
}

// Enriches a named type with aliases data, if any
func parseAliases(schemaMap map[string]interface{}, t types.Named, namespace string) error {
	aliases, ok := schemaMap["aliases"]
	if !ok {
		return nil
	}

	interfaceList, ok := aliases.([]interface{})
	if !ok {
		return fmt.Errorf("Field aliases expected to be array, got %v", aliases)
	}
	stringList, ok := interfaceSliceToStringSlice(interfaceList)
	if !ok {
		return fmt.Errorf("Field aliases expected to be array of string, got %v", interfaceList)
	}

	names := make([]string, 0, len(interfaceList))
	for _, alias := range stringList {
		names = append(names, fixQName(alias, namespace))
	}
	t.SetAliases(names)
	return nil
}

// Enriches a doc-aware type with doc string data, if declared
func parseDoc(schemaMap map[string]interface{}, t types.Documented) error {
	var docString string
	if doc, ok := schemaMap["doc"]; ok {
		if docString, ok = doc.(string); !ok {
			return errors.New("'doc' must be a string")
		}
		t.SetDoc(docString)
	}
	return nil
}

// Enriches a default-valued type with default string data, if declared
func parseDefault(schemaMap map[string]interface{}, t types.Generic) (err error) {
	def, ok := schemaMap["default"]
	if !ok {
		return nil
	}

	switch tp := t.(type) {
	case *types.Enum:
		def, err = fixDefaultForType(tp, def)
	case *types.Field:
		def, err = fixDefaultForType(tp.Type(), def)
	default:
		err = fmt.Errorf("type %T cannot have a default value", t)
	}
	if err == nil {
		t.(types.DefaultValued).SetDefault(def)
	}
	return
}

// fixDefaultForType recursively fixes (casts) the default value so it fits
// properly in the given type. This has two benefits: firstly, that the numeric
// values are actually converted to their proper types; secondly, that the
// JSON type-checking is performed during parse time, which is always a good
// idea for code generation.
func fixDefaultForType(t types.Generic, def interface{}) (interface{}, error) {
	var err error
	switch tp := t.(type) {
	case *types.Array:
		arr := []interface{}(def.([]interface{}))
		for i := range arr {
			if arr[i], err = fixDefaultForType(tp.Type(), arr[i]); err != nil {
				err = fmt.Errorf("bad default value for array element %d: %v",
					i, err)
				break
			}
		}
		def = arr
	case *types.Enum, *types.Fixed:
		def = string(def.(string))
	case *types.Map:
		m := map[string]interface{}(def.(map[string]interface{}))
		for k, v := range m {
			if m[k], err = fixDefaultForType(tp.Type(), v); err != nil {
				err = fmt.Errorf("bad default value for map element %s: %v",
					k, err)
				break
			}
		}
		def = m
	case *types.Record:
		m := map[string]interface{}(def.(map[string]interface{}))
		fields := tp.Children()
		for i := range fields {
			f := fields[i].(*types.Field)
			if fDef, ok := m[f.Name()]; ok {
				if m[f.Name()], err = fixDefaultForType(f.Type(), fDef); err != nil {
					err = fmt.Errorf("bad default value for field %s.%s: %v",
						tp.Name(), f.Name(), err)
					break
				}
			}
		}
		def = m
	case *types.Union:
		f := tp.Children()[0].(*types.Field)
		if def, err = fixDefaultForType(f.Type(), def); err != nil {
			err = fmt.Errorf("bad default value for union: %v", err)
		}

	case *types.Primitive:
		def, err = fixDefaultForPrimitive(tp, def)
	default:
		err = fmt.Errorf("type %T cannot have a default value", t)
	}

	return def, err
}

// fixDefaultForPrimitive fixes (casts) the default value so it fits properly
// in the given primitive type.
func fixDefaultForPrimitive(t *types.Primitive, def interface{}) (interface{}, error) {
	var err error
	switch t.Name() {
	case "null":
		// Default values for null types are forcibly set to nil
		def = nil
	case "boolean":
		if v, ok := def.(bool); ok {
			def = (v == true)
		} else {
			err = fmt.Errorf("invalid default value '%v' for boolean type", def)
		}
	case "int":
		switch v := def.(type) {
		case int32:
			def = v
		case float64:
			def = int32(v)
		default:
			err = fmt.Errorf("invalid default value '%v' for int type", def)
		}
	case "long":
		switch v := def.(type) {
		case int64:
			def = v
		case float64:
			def = int64(v)
		default:
			err = fmt.Errorf("invalid default value '%v' for long type", def)
		}
	case "float":
		switch v := def.(type) {
		case float32:
			def = v
		case float64:
			def = float32(v)
		default:
			err = fmt.Errorf("invalid default value '%v' for float type", def)
		}
	case "double":
		switch v := def.(type) {
		case float64:
			def = v
		default:
			err = fmt.Errorf("invalid default value '%v' for double type", def)
		}
	case "bytes":
		switch v := def.(type) {
		case string:
			def = v
		case []byte:
			def = string(v)
		default:
			err = fmt.Errorf("invalid default value '%v' for bytes type", def)
		}
	case "string":
		switch v := def.(type) {
		case string:
			def = v
		case []byte:
			def = string(v)
		default:
			err = fmt.Errorf("invalid default value '%v' for string type", def)
		}
	default:
		panic(fmt.Sprintf("unexpected name %s for a primitive type", t.Name()))
	}
	return def, err
}

// extractQName returns the fully qualified name from the
// schema definition represented by schemaMap. From the specification:
//
//	In record, enum and fixed definitions, the fullname is
//	determined in one of the following ways:
//
//	- A name and namespace are both specified. For example, one
//	might use "name": "X", "namespace": "org.foo" to indicate the
//	fullname org.foo.X.
//
//	- A fullname is specified. If the name specified contains a
//	dot, then it is assumed to be a fullname, and any namespace
//	also specified is ignored. For example, use "name":
//	"org.foo.X" to indicate the fullname org.foo.X.
//
//	- A name only is specified, i.e., a name that contains no
//	dots. In this case the namespace is taken from the most
//	tightly enclosing schema or protocol. For example, if "name":
//	"X" is specified, and this occurs within a field of the record
//	definition of org.foo.Y, then the fullname is org.foo.X. If
//	there is no enclosing namespace then the null namespace is
//	used.
func extractQName(schemaMap map[string]interface{}, enclosing string) (qname string, err error) {
	if qname, err = stringFromMap(schemaMap, "name"); err != nil {
		return
	}
	ns := ""
	if _, ok := schemaMap["namespace"]; ok && strings.Index(qname, ".") < 0 {
		if ns, err = stringFromMap(schemaMap, "namespace"); err != nil {
			return
		}
	}
	if ns == "" {
		ns = enclosing
	}
	return fixQName(qname, ns), nil
}

// extractNamespace returns the namespace part of the given name, if any
func extractNamespace(name string) string {
	if lastIndex := strings.LastIndex(name, "."); lastIndex != -1 {
		return name[0:lastIndex]
	}
	return ""
}

// fixQName parses a name according to the Avro spec:
//   - If the name contains a dot ('.'), the last part is the name and the rest is the namespace
//   - Otherwise, the enclosing namespace is used
func fixQName(name, ns string) string {
	if lastIndex := strings.LastIndex(name, "."); lastIndex != -1 || ns == "" {
		return name
	}
	return fmt.Sprintf("%s.%s", ns, name)
}

func interfaceSliceToStringSlice(iSlice []interface{}) ([]string, bool) {
	var ok bool
	stringSlice := make([]string, len(iSlice))
	for i, v := range iSlice {
		stringSlice[i], ok = v.(string)
		if !ok {
			return nil, false
		}
	}
	return stringSlice, true
}
