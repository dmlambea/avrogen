package avro

import (
	"gitlab.com/dmlambea/avrogen/pkg/types"
)

// deserializeBlock is a convenience method for deserializing block-encoded
// structures, like arrays and maps.
func (d deserializer) deserializeBlock() {
	for {
		// Load block length. If no more blocks (length==0) or an error
		// occurs, go back
		count := d.readInputPrimitiveName("long").(int64)
		if count < 0 {
			// If count is signaling a blocksize indicator, ignore it and use abs(count) as count instead
			_ = d.readInputPrimitiveName("long")
			count = -count
		}

		// Inform the block owner's setter about the number of items to be expected within this block
		d.s.Init(int(count))

		// No more blocks, end processing
		if count == 0 {
			return
		}

		d.deserializeBlockChildren(int(count))
	}
}

// deserializeBlockChild is a convenience method for deserializing the child
// type of a block-encoded structures, like arrays and maps. This method is to
// be called from within deserializeBlock, by using a child deserializer.
func (d deserializer) deserializeBlockChildren(blockCount int) {
	_, isMapType := d.r.(*types.Map)
	wChildType := d.w.(types.SingleChild).Type()
	rChildType := d.r.(types.SingleChild).Type()
	primitiveChildren := isPrimitiveOrOptionalPrimitive(rChildType)

	//arreglar esto para el nuevo tipo de mapeos

	d = d.using(wChildType, rChildType)
	for i := 0; i < blockCount; i++ {
		var key string
		if isMapType {
			key = d.readInputPrimitiveName("string").(string)
		}

		// Complex (non-primitive) block types require a Dig, then a final Set
		switch primitiveChildren {
		case true:
			d.deserialize()
		default:
			d.stepIntoIfNonOptionalComposite(rChildType).deserialize()
		}
		if isMapType {
			d.s.Set(key)
		}
	}
}
