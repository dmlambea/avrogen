package avro

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDeserializerArrayCases(t *testing.T) {
	schema := `{ "type": "array", "items": "int" }`

	srcArray := []int32{1, 2, 3, 4, 5}

	var buf bytes.Buffer
	WriteLong(int64(len(srcArray)), &buf)
	for _, i := range srcArray {
		WriteInt(i, &buf)
	}
	WriteLong(0, &buf)

	var dstArray []int32
	assert.Nil(t, DeserializeFromJSONSchemas(&buf, schema, schema, &dstArray))
	assert.Equal(t, srcArray, dstArray)
}

func TestDeserializerArrayNestedCases(t *testing.T) {
	schema := `{ "type": "array", "items": { "type": "array", "items": "int" } }`
	srcArray := [][]int32{{1, 2, 3, 4, 5}, {11, 12, 13}, {21, 22, 23, 24, 25, 26}, {}, {1000}}

	var buf bytes.Buffer
	WriteLong(int64(len(srcArray)), &buf)
	for _, nested := range srcArray {
		WriteLong(int64(len(nested)), &buf)
		for i := range nested {
			WriteInt(nested[i], &buf)
		}
		if len(nested) > 0 {
			WriteLong(0, &buf)
		}
	}
	WriteLong(0, &buf)

	var dstArray [][]int32
	assert.Nil(t, DeserializeFromJSONSchemas(&buf, schema, schema, &dstArray))
	assert.Equal(t, srcArray, dstArray)
}

func TestDeserializerMapCases(t *testing.T) {
	schema := `{ "type": "map", "values": "int" }`

	srcMap := make(map[string]int32)
	srcMap["one"] = 1
	srcMap["two"] = 2

	var buf bytes.Buffer
	WriteLong(int64(len(srcMap)), &buf)
	for k, v := range srcMap {
		WriteString(k, &buf)
		WriteInt(v, &buf)
	}
	WriteLong(0, &buf)

	var dstMap map[string]int32
	assert.Nil(t, DeserializeFromJSONSchemas(&buf, schema, schema, &dstMap))
	assert.Equal(t, srcMap, dstMap)
}

func TestDeserializerMapNestedCases(t *testing.T) {
	schema := `{ "type": "map", "values": { "type": "map", "values": "int" } }`

	srcMap := make(map[string]map[string]int32)
	srcMap["one"] = make(map[string]int32)
	srcMap["two"] = make(map[string]int32)
	srcMap["one"]["eleven"] = 11
	srcMap["one"]["twelve"] = 12
	srcMap["two"]["twenty one"] = 21
	srcMap["two"]["twenty two"] = 22

	var buf bytes.Buffer
	WriteLong(int64(len(srcMap)), &buf)
	for k, nested := range srcMap {
		WriteString(k, &buf)
		WriteLong(int64(len(nested)), &buf)
		for innerK, innerV := range nested {
			WriteString(innerK, &buf)
			WriteInt(innerV, &buf)
		}
		if len(nested) > 0 {
			WriteLong(0, &buf)
		}
	}
	WriteLong(0, &buf)

	var dstMap map[string]map[string]int32
	assert.Nil(t, DeserializeFromJSONSchemas(&buf, schema, schema, &dstMap))
	assert.Equal(t, srcMap, dstMap)
}

func TestDeserializerArrayOfMapCases(t *testing.T) {
	schema := `{ "type": "array", "items": { "type": "map", "values": "int" } }`

	srcArray := []map[string]int32{make(map[string]int32), make(map[string]int32)}
	srcArray[0]["eleven"] = 11
	srcArray[0]["twelve"] = 12
	srcArray[1]["hundred"] = 100
	srcArray[1]["thousand"] = 1000

	var buf bytes.Buffer
	WriteLong(int64(len(srcArray)), &buf)
	for _, nested := range srcArray {
		WriteLong(int64(len(nested)), &buf)
		for k, v := range nested {
			WriteString(k, &buf)
			WriteInt(v, &buf)
		}
		if len(nested) > 0 {
			WriteLong(0, &buf)
		}
	}
	WriteLong(0, &buf)

	var dstArray []map[string]int32
	assert.Nil(t, DeserializeFromJSONSchemas(&buf, schema, schema, &dstArray))
	assert.Equal(t, srcArray, dstArray)
}

func TestDeserializerMapOfArrayCases(t *testing.T) {
	schema := `{ "type": "map", "values": { "type": "array", "items": "int" } }`

	srcMap := make(map[string][]int32)
	srcMap["tenths"] = []int32{11, 12, 13, 14, 15}
	srcMap["twenties"] = []int32{21, 22, 23, 24, 25}

	var buf bytes.Buffer
	WriteLong(int64(len(srcMap)), &buf)
	for k, nested := range srcMap {
		WriteString(k, &buf)
		WriteLong(int64(len(nested)), &buf)
		for i := range nested {
			WriteInt(nested[i], &buf)
		}
		if len(nested) > 0 {
			WriteLong(0, &buf)
		}
	}
	WriteLong(0, &buf)

	var dstMap map[string][]int32
	assert.Nil(t, DeserializeFromJSONSchemas(&buf, schema, schema, &dstMap))
	assert.Equal(t, srcMap, dstMap)
}
