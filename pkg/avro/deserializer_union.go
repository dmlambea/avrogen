package avro

import (
	"errors"
	"fmt"

	"gitlab.com/dmlambea/avrogen/pkg/types"
)

var errUnionTypeNotSupported = errors.New("writer's union type is not supported in reader")

// Used by unions to select all types that have a name, like primitives and
// avro named types
type namedType interface {
	Name() string
}

// deserializeUnion deserializes a union type
func (d deserializer) deserializeUnion() {
	wUnion := d.w.(*types.Union)

	// Read writer's union schema discriminator
	wIdx := d.readInputPrimitiveName("long").(int64)

	// Check if reader is also a union
	if rUnion, ok := d.r.(*types.Union); ok {
		d.deserializeUnionToUnion(wUnion, rUnion, wIdx)
		return
	}

	// Otherwise try to match selected writer's schema against reader's
	wType := asField(wUnion.Children()[wIdx]).Type()
	if d.r != nil && !wType.IsReadableBy(d.r, make(types.VisitMap)) {
		panic(fmt.Errorf("incompatible schemas: reader of type '%s' is not compatible with writer's selected schema '%s'", d.r, wType))
	}

	d.stepIntoIfNonOptionalComposite(d.r).using(wType, d.r).deserialize()
}

// deserializeNonUnionToUnion handles the case of a non-union writer being
// read by a union reader. The writer schema can be faked as a union with just
// only one schema, so the regular union-to-union reader code can be reused.
func (d deserializer) deserializeNonUnionToUnion() {
	writerAsField := types.NewField("-fake-", d.w, 0)
	wUnion := types.NewUnion([]types.Generic{writerAsField})
	rUnion := d.r.(*types.Union)
	d.deserializeUnionToUnion(wUnion, rUnion, 0)
}

// deserializeUnion deserializes a writer's message of type union for a reader
// of type union as well.
func (d deserializer) deserializeUnionToUnion(wUnion, rUnion *types.Union, wIdx int64) {
	wType := asField(wUnion.Children()[wIdx]).Type()

	// All children within a union are fields. Try and find the reader's field
	// index for the given writer's.
	rIdx, rType := findBestMatchingChildFor(wType, rUnion)
	if rType == nil {
		panic(fmt.Errorf("incompatible schemas: reader union has no compatible schemas for writer type '%s'", wType))
	}

	if rUnion.IsOptional() {
		// Optional unions might be nil, so deserialization would end here
		if rUnion.OptionalIndex() == rIdx {
			// TODO Should this be setting nil???
			d.s.Set(nil)
			return
		}

		// Optional-nonsimple require their target item's memory to be created
		// and the deserializer to point to it.
		if !rUnion.IsOptionalSimple() {
			d = d.stepInto()
		}
	}

	// Non-optional and optional-nonsimple unions are structs, and as such,
	// they have a type indicator field that must be set.
	if !rUnion.IsOptionalSimple() {
		d.s.Init(rIdx)
	}

	// Step into the target item's type and perform deserialization
	d.stepIntoIfNonOptionalComposite(rType).using(wType, rType).deserialize()

	// Complex-typed optional unions need to be finished after deserializing their
	// types, so the item type's value can be pushed into the enum struct.
	if !rUnion.IsOptional() || !rUnion.IsOptionalSimple() {
		if !isPrimitiveOrOptionalPrimitive(rType) {
			d.s.Set(nil)
		}
	}
}

func findBestMatchingChildFor(wType types.Generic, rUnion *types.Union) (rIdx int, rType types.Generic) {
	wNamed, wIsNamed := wType.(namedType)

	// All children within a union are fields. Try and find the reader's field
	// index for the given writer's.
	rIdx = -1
	for i, child := range rUnion.Children() {
		rField := asField(child)
		if wIsNamed {
			if rNamed, rIsNamed := rField.Type().(namedType); rIsNamed && wNamed.Name() == rNamed.Name() {
				rIdx = i
				rType = rField.Type()
				return
			}
		}

		if rIdx < 0 && wType.IsReadableBy(rField.Type(), make(types.VisitMap)) {
			rIdx = i
			rType = rField.Type()
		}
	}
	return
}
