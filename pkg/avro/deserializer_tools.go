package avro

import (
	"gitlab.com/dmlambea/avrogen/pkg/types"
)

const (
	// Constants used for ordering fields
	orderDiscard int = -1 // This field has to be discarded (consumed) from input
	orderSkip    int = -2 // This field has to be skipped (not set into reader's)
)

func isPrimitiveOrOptionalPrimitive(t types.Generic) bool {
	if _, ok := t.(*types.Primitive); ok {
		return true
	}
	if u, ok := t.(*types.Union); ok {
		if u.IsOptionalSimple() {
			t = asField(u.Children()[u.NonOptionalIndex()]).Type()
			if _, ok := t.(*types.Primitive); ok {
				return true
			}
		}
	}
	return false
}

func asField(t types.Generic) *types.Field {
	return t.(*types.Field)
}

// refineOrder extacts all special field indexes (opDiscardable and opSkippable)
func refineOrder(src []int) (tgt []int) {
	tgt = make([]int, len(src))
	cur := 0
	for i := range src {
		if src[i] >= 0 {
			tgt[cur] = src[i]
			cur++
		}
	}
	return tgt[:cur]
}

// matcherFunc is a utility function to match fields for a given one. It can be used to match
// record fields and union fields.
type matcherFunc func(field *types.Field) (*types.Field, error)

// getReadOrder computes in which order the reader needs to read writer's
// output. Valid, accepted fields from writer are numbered in natural field
// order. Invalid fields from writer (the ones not found in reader) are all
// numbered as (-1). Fields only found in reader, which need to be set a
// default value, are numbered in negative field order minus 2. This way, 0 is
// -2; 1 is -3 and so on...
func getReadOrder(wrtFields, rdrFields []types.Generic, matcher matcherFunc) (order []int, err error) {
	// For every writer's field, let's see in what position the reader expects the value
	for _, wrtChild := range wrtFields {
		wrtFld := asField(wrtChild)
		code := orderDiscard

		var rdrFld *types.Field
		if rdrFld, err = matcher(wrtFld); err != nil {
			return
		}
		if rdrFld != nil {
			code = rdrFld.Index()
		}
		order = append(order, code)
	}

	// The rest of reader's fields must be skipped
	var extras []int
	for idx := range rdrFields {
		if !inArray(order, idx) {
			extras = append(extras, orderSkip-idx)
		}
	}

	if len(extras) > 0 {
		order = append(order, extras...)
	}
	return
}

func inArray(arr []int, val int) bool {
	for i := range arr {
		if arr[i] == val {
			return true
		}
	}
	return false
}
