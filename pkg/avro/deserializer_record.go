package avro

import (
	"fmt"

	"gitlab.com/dmlambea/avrogen/pkg/types"
)

// deserializeRecord deserializes a record type
func (d deserializer) deserializeRecord() {
	// Cast writer and reader to their real types
	wRecord := d.w.(*types.Record)
	wFields := wRecord.Children()
	var rRecord *types.Record
	var rFields []types.Generic
	if d.r != nil {
		rRecord = d.r.(*types.Record)
		rFields = rRecord.Children()
	}

	// Find the order in which the reader's fields must be processed, relative
	// to the writer's
	order, err := getReadOrder(wFields, rFields, func(field *types.Field) (rdrField *types.Field, err error) {
		if rRecord != nil {
			// All children within a record are fields, and must be found by name matching the given one
			if rdrField = rRecord.FindFieldByNameOrAlias(field); rdrField != nil {
				if !field.Type().IsReadableBy(rdrField.Type(), make(types.VisitMap)) {
					err = fmt.Errorf("incompatible schemas: field %s in reader has incompatible type in writer field %s", rdrField.Name(), field.Name())
				}
			}
		}
		return
	})
	if err != nil {
		panic(err)
	}

	// Refine order and configure field setter
	d.s.Init(refineOrder(order))

	// Process all fields, that is, the union set of writer's and reader's
	// fields
	wIdx := 0
	for i := range order {
		curIdx := order[i]
		switch {
		case curIdx >= 0:
			f := asField(rFields[curIdx])
			d.stepIntoIfNonOptionalComposite(f.Type()).using(asField(wFields[wIdx]).Type(), f.Type()).deserialize()
			wIdx++
		case curIdx == orderDiscard:
			d.using(asField(wFields[wIdx]).Type(), nil).skipper().deserialize()
			wIdx++
		case curIdx <= orderSkip:
			// Decode back the reader's index
			curIdx = orderSkip - curIdx
			f := asField(rFields[curIdx])
			if !f.HasDefault() {
				panic(fmt.Errorf("writer record %s has no field %s and reader has no default value for it", wRecord.Name(), f.Name()))
			}
			d.deserializeDefaultType(f.Type(), f.Default())
		}
	}
}
