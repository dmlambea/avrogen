package avro

import (
	"bytes"
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type testDefaultsRecordInnerSample struct {
	C string
	D bool
}

type testDefaultsRecordOuterSample struct {
	A int64
	B testDefaultsRecordInnerSample
}

type testDefaultsRecord struct {
	Idx int32
	S   testDefaultsRecordOuterSample
}

func TestDefaultsRecordOk(t *testing.T) {
	wSchema := `{
		"type": "record",
		"name": "testDefaultsRecord",
		"fields": [ {
			"name": "idx",
			"type": "int"
		} ]
	}`

	rSchema := `{
		"type": "record",
		"name": "testDefaultsRecord",
		"fields": [ {
			"name": "idx",
			"type": "int"
		}, {
			"name": "s",
			"type": {
				"name": "outerSample",
				"type": "record",
				"fields": [
					{ "name": "a", "type": "long" },
					{ "name": "b", "type": { 
						"name": "innerSample",
						"type": "record",
						"fields": [ {
							"name": "c",
							"type": "string"
						}, {
							"name": "d",
							"type": "boolean",
							"default": true
						} ]
					} }
				]
			},
			"default": {"a": 42, "b": {"c": "So long and thanks for all the fish!"}}
		} ]
	}`

	var buf bytes.Buffer
	WriteInt(1, &buf)
	var dst testDefaultsRecord
	require.Nil(t, DeserializeFromJSONSchemas(&buf, wSchema, rSchema, &dst))
	assert.Equal(t, int32(1), dst.Idx)
	assert.Equal(t, int64(42), dst.S.A)
	assert.Equal(t, "So long and thanks for all the fish!", dst.S.B.C)
	assert.Equal(t, true, dst.S.B.D)
}

func TestDefaultsRecordNoDefaults(t *testing.T) {
	wSchema := `{
		"type": "record",
		"name": "testDefaultsRecord",
		"fields": [ {
			"name": "idx",
			"type": "int"
		} ]
	}`

	rSchema := `{
		"type": "record",
		"name": "testDefaultsRecord",
		"fields": [ {
			"name": "idx",
			"type": "int"
		}, {
			"name": "s",
			"type": {
				"name": "outerSample",
				"type": "record",
				"fields": [
					{ "name": "a", "type": "long" },
					{ "name": "b", "type": { 
						"name": "innerSample",
						"type": "record",
						"fields": [ {
							"name": "c",
							"type": "string"
						}, {
							"name": "d",
							"type": "boolean"
						} ]
					} }
				]
			},
			"default": {"a": 42, "b": {"c": "So long and thanks for all the fish!"}}
		} ]
	}`

	var buf bytes.Buffer
	WriteInt(1, &buf)

	var dst testDefaultsRecord
	require.Error(t, DeserializeFromJSONSchemas(&buf, wSchema, rSchema, &dst))
}

func TestCombinationRunnerOneToOne(t *testing.T) {
	schemas := []string{
		"boolean",
		"int",
		"long",
		"float",
		"double",
		"string",
		"bytes",
	}

	fn := func(sel []string) {
		// This is the order the reader is expecting data come in
		var srcRecord = struct {
			Bool   bool
			Int    int32
			Long   int64
			Float  float32
			Double float64
			String string
			Bytes  []byte
		}{
			true, 2, 3, 4.4, 5.5, "six", []byte("seven"),
		}
		var dstRecord struct {
			Bool   bool
			Int    int32
			Long   int64
			Float  float32
			Double float64
			String string
			Bytes  []byte
		}
		rSchema := `{
			"type": "record",
			"name": "test",
			"fields": [
				{ "name": "aBoolean", "type": "boolean" },
				{ "name": "aInt", "type": "int" },
				{ "name": "aLong", "type": "long" },
				{ "name": "aFloat", "type": "float" },
				{ "name": "aDouble", "type": "double" },
				{ "name": "aString", "type": "string" },
				{ "name": "aBytes", "type": "bytes" }
			]}`
		roundtripRecordCombination(t, &srcRecord, &dstRecord, rSchema, sel)
	}
	runCombinations(fn, nil, schemas)
}

func TestCombinationRunnerComplex(t *testing.T) {
	// Writer has all but float, strings and bytes
	schemas := []string{
		"boolean",
		"int",
		"long",
		"double",
	}

	fn := func(sel []string) {
		// Reader has all but booleans and ints
		var srcRecord = struct {
			Long   int64
			Float  float32
			Double float64
			String string
			Bytes  []byte
		}{
			3, 4.4, 5.5, "six", []byte("seven"),
		}
		var dstRecord struct {
			Long   int64
			Float  float32
			Double float64
			String string
			Bytes  []byte
		}
		rSchema := `{
			"type": "record",
			"name": "test",
			"fields": [
				{ "name": "aLong", "type": "long" },
				{ "name": "aFloat", "type": "float", "default": 4.4 },
				{ "name": "aDouble", "type": "double" },
				{ "name": "aString", "type": "string", "default": "six" },
				{ "name": "aBytes", "type": "bytes", "default": "seven" }
			]}`
		roundtripRecordCombination(t, &srcRecord, &dstRecord, rSchema, sel)
	}
	runCombinations(fn, nil, schemas)
}

func runCombinations(fn func(selection []string), sel, opts []string) {
	switch len(opts) {
	case 0:
		fn(sel)
	default:
		for i := range opts {
			innerSel := copyCombination(sel)
			innerSel = append(innerSel, opts[i])
			innerOpts := copyCombination(opts[:i])
			innerOpts = append(innerOpts, opts[i+1:]...)
			runCombinations(fn, innerSel, innerOpts)
		}
	}
}

func copyCombination(combination []string) []string {
	newCombination := make([]string, len(combination))
	copy(newCombination, combination)
	return newCombination
}

func roundtripRecordCombination(t *testing.T, srcAddress, dstAddress interface{}, rSchema string, fieldTypes []string) {
	// Create writer schema and input data, in the same order as field types'
	var str strings.Builder
	var buf bytes.Buffer

	str.WriteString(`{ "type": "record", "name": "test", "fields": [`)
	for i := range fieldTypes {
		if i > 0 {
			str.WriteString(",")
		}
		str.WriteString(fmt.Sprintf(`{ "name": "a%s", "type": "%s" }`, strings.Title(fieldTypes[i]), fieldTypes[i]))
		switch fieldTypes[i] {
		case "boolean":
			WriteBool(true, &buf)
		case "int":
			WriteInt(2, &buf)
		case "long":
			WriteLong(3, &buf)
		case "float":
			WriteFloat(4.4, &buf)
		case "double":
			WriteDouble(5.5, &buf)
		case "string":
			WriteString("six", &buf)
		case "bytes":
			WriteBytes([]byte("seven"), &buf)
		}

	}
	str.WriteString(`] }`)

	require.Nil(t, DeserializeFromJSONSchemas(&buf, str.String(), rSchema, dstAddress))
	assert.Equal(t, srcAddress, dstAddress)
}
