package avro

import (
	"bytes"
	"fmt"
	"io"

	"gitlab.com/dmlambea/avrogen/pkg/avro/internal/setter"
	"gitlab.com/dmlambea/avrogen/pkg/parser"
	"gitlab.com/dmlambea/avrogen/pkg/types"
)

type schemaInformer interface {
	BinarySchema() []byte
}

func slicesEqual(a, b []byte) bool {
	if len(a) != len(b) {
		return false
	}
	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

// Deserialize performs the task of of deserializing data written by the given
// writer schema in binary format into the receiver object, which in turn must implement
// schemaInformer. The Avro-encoded data source is interpreted for the schema
// returned from the receiver object.
func Deserialize(data io.Reader, writerSchema []byte, targetObject schemaInformer) error {
	return DeserializeFromSchemas(data, writerSchema, targetObject.BinarySchema(), targetObject)
}

// DeserializeJSON performs the task of of deserializing data written by the given
// writer schema in JSON format into the receiver object, which in turn must implement
// schemaInformer. The Avro-encoded data source is interpreted for the schema
// returned from the receiver object.
func DeserializeJSON(data io.Reader, writerSchema string, targetObject schemaInformer) error {
	return DeserializeFromJSONSchema(data, writerSchema, targetObject.BinarySchema(), targetObject)
}

// DeserializeFromSchemas performs the task of of deserializing data written for the
// writer's schema into the receiver object, which must conform to the reader's
// schema. The Avro-encoded data source is interpreted for the given type and
// deserialized into the target object pointed by targetAddr.
func DeserializeFromSchemas(data io.Reader, writerSchema, readerSchema []byte, targetAddr interface{}) error {
	wType, err := types.Deserialize(bytes.NewBuffer(writerSchema))
	if err != nil {
		return fmt.Errorf("unable to parse writer: %v", err)
	}

	rType := wType
	if !slicesEqual(writerSchema, readerSchema) {
		if rType, err = types.Deserialize(bytes.NewBuffer(readerSchema)); err != nil {
			return fmt.Errorf("unable to parse reader: %v", err)
		}
	}
	return DeserializeFromTypes(data, wType, rType, targetAddr)
}

// DeserializeFromJSONSchema performs the task of of deserializing data written for the
// writer's schema in JSON format into the receiver object, which must conform to the reader's
// schema, in binary format. The Avro-encoded data source is interpreted for the given type and
// deserialized into the target object pointed by targetAddr.
func DeserializeFromJSONSchema(data io.Reader, writerSchema string, readerSchema []byte, targetAddr interface{}) error {
	wType, err := parser.ParseSchema(writerSchema)
	if err != nil {
		return fmt.Errorf("unable to parse writer schema: %v", err)
	}

	rType, err := types.Deserialize(bytes.NewBuffer(readerSchema))
	if err != nil {
		return fmt.Errorf("unable to parse reader schema: %v", err)
	}
	return DeserializeFromTypes(data, wType, rType, targetAddr)
}

// DeserializeFromJSONSchemas performs the task of of deserializing data written for the
// writer's schema in JSON format into the receiver object, which must conform to the reader's
// schema, also in JSON format. The Avro-encoded data source is interpreted for the given type and
// deserialized into the target object pointed by targetAddr.
func DeserializeFromJSONSchemas(data io.Reader, writerSchema, readerSchema string, targetAddr interface{}) error {
	wType, err := parser.ParseSchema(writerSchema)
	if err != nil {
		return fmt.Errorf("unable to parse writer schema: %v", err)
	}

	rType := wType
	if writerSchema != readerSchema {
		if rType, err = parser.ParseSchema(readerSchema); err != nil {
			return fmt.Errorf("unable to parse reader schema: %v", err)
		}
	}
	return DeserializeFromTypes(data, wType, rType, targetAddr)
}

// DeserializeFromTypes performs the task of of deserializing data written for the
// writer's type into the receiver object, which must conform to the reader's
// type. The Avro-encoded data source is interpreted for the given type and
// deserialized into the target object pointed by targetAddr.
func DeserializeFromTypes(data io.Reader, writer, reader types.Generic, targetAddr interface{}) (err error) {
	var s setter.Setter
	if s, err = setter.New(targetAddr); err != nil {
		return fmt.Errorf("unable to start deserialization: %v", err)
	}
	defer func() {
		r := recover()
		if r != nil {
			err = fmt.Errorf("unable to deserialize %T: %v", reader, r)
		}
	}()
	deser := deserializer{input: data, s: s}
	deser = deser.using(writer, reader)
	if reader == nil {
		deser = deser.skipper()
	}
	deser.deserialize()
	return
}

const (
	errStringIncompatibleTypes = "incompatible types %T and %T"
)

type deserializer struct {
	input io.Reader
	s     setter.Setter
	w     types.Generic
	r     types.Generic
}

// deserialize runs the deserialization of this deserializer's data, which was
// written by this deserializer's writer 'w', into an object generated for reader 'r'
func (d deserializer) deserialize() {
	// Check the special case in the specs of the writer being a non-union
	// type while the reader is a union type
	if _, ok := d.r.(*types.Union); ok {
		if _, ok := d.w.(*types.Union); !ok {
			d.deserializeNonUnionToUnion()
			return
		}
	}

	switch d.w.(type) {
	case *types.Array, *types.Map:
		d.deserializeBlock()
	case *types.Enum:
		d.deserializeEnum()
	case *types.Fixed, *types.Primitive:
		d.deserializeCommon()
	case *types.Record:
		d.deserializeRecord()
	default:
		d.deserializeUnion()
	}
	return
}

// deserializeCommon deserializes a common type, i.e., those types whose requirements for
// deserialization are just consuming one piece of data, then setting the data
// into the target object
func (d deserializer) deserializeCommon() {
	data := d.readInput(d.w)
	// TODO make sure there is no need for nil-checking the reader, if using a skipper
	if d.r != nil && !d.w.IsReadableBy(d.r, make(types.VisitMap)) {
		panic(fmt.Errorf(errStringIncompatibleTypes, d.w, d.r))
	}
	d.s.Set(data)
}

func (d deserializer) skipper() deserializer {
	d.s, _ = setter.NewDataSink()
	return d
}

// stepInto returns a clone of this deserializer, but using an inner
// setter. This call can only be successful if the current setter's field is
// already a setter or can be created a setter from.
func (d deserializer) stepInto() deserializer {
	d.s = d.s.Dig()
	return d
}

// stepIntoIfNonOptionalComposite returns a clone of this deserializer, but using an inner
// setter if the given type is a Composite. This call can only be successful
// if the current setter's field is already a setter or can be created a setter from.
func (d deserializer) stepIntoIfNonOptionalComposite(tp types.Generic) deserializer {
	if _, ok := tp.(types.Composite); !ok {
		return d
	}

	if u, ok := tp.(*types.Union); ok && u.IsOptional() {
		return d
	}
	return d.stepInto()
}

// using returns a clone of this deserializer, but masking its current writer
// and reader types by the ones supplied as arguments. This is a convenience
// function for chaining sub-type deserialization calls.
func (d deserializer) using(w, r types.Generic) deserializer {
	d.w = w
	d.r = r
	return d
}

// readInput is a convenience function for consuming the right amount of data
// from the deserializer input stream for the given type.
func (d deserializer) readInput(generic types.Generic) (data interface{}) {
	var err error
	switch concrete := generic.(type) {
	case *types.Enum:
		data, err = readInt(d.input)
	case *types.Fixed:
		data, err = readFixed(d.input, int(concrete.SizeBytes()))
	case *types.Primitive:
		data = d.readInputPrimitiveType(concrete)
	}
	if err == nil {
		return data
	}
	panic(fmt.Errorf("cannot consume input for type %T: %v", generic, err))
}

// readInputPrimitiveType is a convenience function for calling the appropriate reader
// function, depending on the given primitive data type.
func (d deserializer) readInputPrimitiveType(t *types.Primitive) (data interface{}) {
	return d.readInputPrimitiveName(t.Name())
}

// readInputPrimitiveName is a convenience function for consuming data,
// depending on the primitive type name.
func (d deserializer) readInputPrimitiveName(primitiveTypeName string) (data interface{}) {
	var err error
	switch primitiveTypeName {
	case "null":
		// Nothing
	case "boolean":
		data, err = readBool(d.input)
	case "int":
		data, err = readInt(d.input)
	case "long":
		data, err = readLong(d.input)
	case "float":
		data, err = readFloat(d.input)
	case "double":
		data, err = readDouble(d.input)
	case "string":
		data, err = readString(d.input)
	case "bytes":
		data, err = readBytes(d.input)
	}
	if err == nil {
		return data
	}
	panic(fmt.Errorf("cannot consume input for primitive type %s: %v", primitiveTypeName, err))
}
