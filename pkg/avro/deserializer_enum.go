package avro

import (
	"fmt"

	"gitlab.com/dmlambea/avrogen/pkg/types"
)

// deserializeEnum deserializes an enum type
func (d deserializer) deserializeEnum() {
	data := d.readInput(d.w)
	wEnum := d.w.(*types.Enum)
	symbolPos := int(data.(int32))
	if symbolPos < 0 || symbolPos >= len(wEnum.Symbols()) {
		panic(fmt.Errorf("invalid value %d for enum type %s", symbolPos, wEnum))
	}

	if d.r == nil {
		return
	}
	if !wEnum.IsReadableBy(d.r, make(types.VisitMap)) {
		panic(fmt.Errorf(errStringIncompatibleTypes, wEnum, d.r))
	}

	// Map writer's symbol position to reader's
	rEnum := d.r.(*types.Enum)
	s := wEnum.Symbols()[symbolPos]
	if symbolPos = rEnum.IndexOfSymbol(s); symbolPos < 0 {
		if !rEnum.HasDefault() {
			panic(fmt.Errorf("symbol '%s' from writer %s does not exist in reader and reader has no default value", s, wEnum))
		}
		symbolPos = rEnum.IndexOfSymbol(rEnum.Default().(string))
	}
	d.s.Set(int32(symbolPos))
}
