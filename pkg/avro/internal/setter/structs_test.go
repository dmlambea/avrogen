package setter

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type nestedStructSetterTest struct {
	Name   string
	Salary float64
}

type structSetterTest struct {
	Int      int32
	String   string
	IntPtr   *int32
	Bytes    []byte
	Boss     nestedStructSetterTest
	Pal      *nestedStructSetterTest
	Array    []string
	Baywatch []nestedStructSetterTest
}

func TestStructSetter(t *testing.T) {
	var obj structSetterTest
	sSetter, err := New(&obj)
	require.Nil(t, err)
	require.NotNil(t, sSetter)

	sSetter.Set(int32(1))
	sSetter.Set("Hi!")
	assert.Equal(t, int32(1), obj.Int)
	assert.Equal(t, "Hi!", obj.String)

	// IntPtr
	assert.Equal(t, (*int32)(nil), obj.IntPtr)
	sSetter.Set(int32(42))
	assert.Equal(t, int32(42), *obj.IntPtr)

	// Bytes
	assert.Equal(t, ([]byte)(nil), obj.Bytes)
	sSetter.Set([]byte{1, 2, 3, 4})
	assert.Equal(t, []byte{1, 2, 3, 4}, obj.Bytes)

	// Boss
	inner := sSetter.Dig()
	require.NotNil(t, inner)
	inner.Set("John Doe")
	inner.Set(float64(4.2))
	assert.Equal(t, "John Doe", obj.Boss.Name)
	assert.Equal(t, float64(4.2), obj.Boss.Salary)

	// Pal
	inner = sSetter.Dig()
	require.NotNil(t, inner)
	require.NotNil(t, obj.Pal)
	inner.Set("Steve McQueen")
	inner.Set(float64(8.4))
	assert.Equal(t, "Steve McQueen", obj.Pal.Name)
	assert.Equal(t, float64(8.4), obj.Pal.Salary)

	// Array
	inner = sSetter.Dig()
	require.NotNil(t, inner)
	inner.Init(2)
	inner.Set("Hi")
	inner.Set("there!")
	assert.Equal(t, []string{"Hi", "there!"}, obj.Array)

	// Baywatch casting
	inner = sSetter.Dig()
	require.NotNil(t, inner)
	inner.Init(2)

	casting := inner.Dig()
	casting.Set("Mitch Buchannon")
	casting.Set(4.2)

	casting = inner.Dig()
	casting.Set("C. J. Parker")
	casting.Set(8.4)

	assert.Equal(t, []nestedStructSetterTest{
		{"Mitch Buchannon", 4.2},
		{"C. J. Parker", 8.4},
	}, obj.Baywatch)
}
