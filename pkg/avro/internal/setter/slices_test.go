package setter

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type sliceSetterTest struct {
	Name   string
	Salary float64
}

func TestPrimitiveSliceSetter(t *testing.T) {
	var s []string

	sSetter, err := New(&s)
	require.Nil(t, err)
	require.NotNil(t, sSetter)

	require.Nil(t, s)

	// Init slice memory
	sSetter.Init(2)

	// Put some values on it
	sSetter.Set("Mitch Buchannon")

	sSetter.Set("C. J. Parker")

	// Assertions
	require.NotNil(t, s)
	require.Equal(t, 2, len(s))
	assert.Equal(t, "Mitch Buchannon", s[0])
	assert.Equal(t, "C. J. Parker", s[1])
}

func TestBytesSliceSetter(t *testing.T) {
	var s [][]byte

	sSetter, err := New(&s)
	require.Nil(t, err)
	require.NotNil(t, sSetter)

	require.Nil(t, s)

	// Init slice memory
	sSetter.Init(2)

	// Put some values on it
	sSetter.Set([]byte("Mitch Buchannon"))

	sSetter.Set([]byte("C. J. Parker"))

	// Assertions
	require.NotNil(t, s)
	require.Equal(t, 2, len(s))
	assert.Equal(t, []byte("Mitch Buchannon"), s[0])
	assert.Equal(t, []byte("C. J. Parker"), s[1])
}

func TestPrimitivePtrSliceSetter(t *testing.T) {
	var s []*int32

	sSetter, err := New(&s)
	require.Nil(t, err)
	require.NotNil(t, sSetter)

	require.Nil(t, s)

	// Init slice memory
	sSetter.Init(2)

	// Put some values on it
	sSetter.Set(int32(42))

	sSetter.Set(int32(84))

	// Assertions
	require.NotNil(t, s)
	require.Equal(t, 2, len(s))
	assert.Equal(t, int32(42), *s[0])
	assert.Equal(t, int32(84), *s[1])
}

func TestStructSliceSetter(t *testing.T) {
	var s []sliceSetterTest

	sSetter, err := New(&s)
	require.Nil(t, err)
	require.NotNil(t, sSetter)

	require.Nil(t, s)

	// Init slice memory
	sSetter.Init(2)

	// Put some values on it
	val := sSetter.Dig()
	val.Set("Mitch Buchannon")
	val.Set(float64(4.2))

	val = sSetter.Dig()
	val.Set("C. J. Parker")
	val.Set(float64(8.4))

	// Assertions
	require.NotNil(t, s)
	require.Equal(t, 2, len(s))
	assert.Equal(t, "Mitch Buchannon", s[0].Name)
	assert.Equal(t, float64(4.2), s[0].Salary)
	assert.Equal(t, "C. J. Parker", s[1].Name)
	assert.Equal(t, float64(8.4), s[1].Salary)
}

func TestStructPtrSliceSetter(t *testing.T) {
	var s []*sliceSetterTest

	sSetter, err := New(&s)
	require.Nil(t, err)
	require.NotNil(t, sSetter)

	require.Nil(t, s)

	// Init slice memory
	sSetter.Init(2)

	// Put some values on it
	val := sSetter.Dig()
	val.Set("Mitch Buchannon")
	val.Set(float64(4.2))

	val = sSetter.Dig()
	val.Set("C. J. Parker")
	val.Set(float64(8.4))

	// Assertions
	require.NotNil(t, s)
	require.Equal(t, 2, len(s))
	assert.Equal(t, "Mitch Buchannon", s[0].Name)
	assert.Equal(t, float64(4.2), s[0].Salary)
	assert.Equal(t, "C. J. Parker", s[1].Name)
	assert.Equal(t, float64(8.4), s[1].Salary)
}
