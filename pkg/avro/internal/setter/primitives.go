package setter

import (
	"reflect"
)

// newPrimitiveSetter creates a setter for a single field of any of the
// primitive types bool, int32, int64, float32, float64, string or []bytes, or
// a pointer to any of those types.
func newPrimitiveSetter(field reflect.Value) (*primitiveSetter, error) {
	return &primitiveSetter{
		field: field,
	}, nil
}

type primitiveSetter struct {
	field reflect.Value
}

// Init cannot be called on a primitive setter.
func (s *primitiveSetter) Init(arg interface{}) {
	panic("primitive setter cannot be initialized")
}

// Set puts the value into the field.
func (s *primitiveSetter) Set(value interface{}) {
	setPrimitive(s.field, value)
}

// Dig cannot be called on a primitive setter.
func (s *primitiveSetter) Dig() Setter {
	panic("primitive setter cannot be digged into")
}
