package setter

import (
	"errors"
	"fmt"
	"reflect"
)

var (
	errNoFields = errors.New("unable to create setter: no public fields in struct")
)

// newStructSetter creates a setter for all exported fields in struct elem. If
// the given struct happens to have no exported fields, errNoFields is returned.
func newStructSetter(elem reflect.Value) (s Setter, err error) {
	total := elem.NumField()
	var setterFields []reflect.Value
	for i := 0; i < total; i++ {
		fld := elem.Field(i)
		if !fld.CanAddr() || !fld.Addr().CanInterface() {
			continue
		}
		setterFields = append(setterFields, fld)
	}
	if len(setterFields) > 0 {
		return newStructSetterForFields(setterFields), nil
	}
	return nil, errNoFields
}

// newStructSetterForFields returns a struct setter for the given field list.
func newStructSetterForFields(fields []reflect.Value) *structSetter {
	s := &structSetter{
		originalFields: make([]reflect.Value, len(fields)),
		fields:         fields,
		indexes:        make([]int, len(fields)),
		fieldCount:     len(fields),
	}
	copy(s.originalFields, fields)

	// TODO is this really needed??
	// Initialize sort order to natural ascending order
	for i := range s.indexes {
		s.indexes[i] = i
	}
	return s
}

type structSetter struct {
	originalFields []reflect.Value
	fields         []reflect.Value
	indexes        []int
	fieldCount     int
	currentField   int
}

// Init configures the struct setter with a list of the order its fields
// are to be set.
func (s *structSetter) Init(arg interface{}) {
	positions, ok := arg.([]int)
	if ok {
		s.sort(positions)
		return
	}
	panic(fmt.Errorf("struct setter initialization expects []int, got %T", arg))
}

// Set puts the value into the current field and advances the index to the
// next field.
func (s *structSetter) Set(value interface{}) {
	fld := s.get(s.currentField)
	setPrimitive(fld, value)
	s.next()
}

// Dig returns a setter for the current field's complex type and advances the
// index of this setter to the next field.
func (s *structSetter) Dig() Setter {
	fld := s.get(s.currentField)

	var addr interface{}
	switch fld.Type().Kind() {
	case reflect.Ptr:
		ptrType := reflect.New(fld.Type().Elem())
		fld.Set(ptrType)
		addr = ptrType.Interface()
	default:
		addr = fld.Addr().Interface()
	}

	// Try to detect a Setter from it
	inner, err := newForPointer(addr)
	if err == nil {
		s.next()
		return inner
	}
	panic(fmt.Errorf("unable to dig into field %d: %v", s.currentField, err))
}

// get returns the i-th field, respecting the sort order
func (s *structSetter) get(i int) reflect.Value {
	return s.fields[s.indexes[i]]
}

// next advances internal current pointer.
func (s *structSetter) next() {
	s.currentField++
}

// sort replaces the ordering of the fields within this setter. The length of positions
// cannot exceed the length of the fields array. The position indexes must be between 0
// and len(fields)-1. All fields not referred to in the positions array are put in order
// of appearance at the end of the list.
func (s *structSetter) sort(positions []int) {
	visited := make([]bool, s.fieldCount)
	for i := range positions {
		s.indexes[i] = positions[i]
		visited[positions[i]] = true
	}
	posCount := len(positions)
	if posCount < s.fieldCount {
		for i := 0; posCount < s.fieldCount; i++ {
			if !visited[i] {
				s.indexes[posCount] = i
				posCount++
			}
		}
	}
}
