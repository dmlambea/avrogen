package setter

// NewDataSink creates a setter that does nothing. It is useful to consume
// data from a reader that goes nowhere.
func NewDataSink() (Setter, error) {
	return &dataSinkSetter{}, nil
}

type dataSinkSetter struct{}

// Init does nothing, as everything else in this setter.
func (d *dataSinkSetter) Init(arg interface{}) {
}

// Set does nothing, as everything else in this setter.
func (d *dataSinkSetter) Set(val interface{}) {
}

// Dig does nothing, as everything else in this setter.
func (d *dataSinkSetter) Dig() Setter {
	return d
}
