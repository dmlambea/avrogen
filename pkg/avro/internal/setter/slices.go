package setter

import (
	"fmt"
	"reflect"
)

// newSliceSetter creates a setter for the given underlying slice/array, which
// can be nil. For non-primitive element types, Dig must be called prior to
// start setting complex values, so a special item setter can be created for
// every single item. In this case, Set must not be called in the slice setter
// itself: Dig is just enough.
func newSliceSetter(sliceElem reflect.Value) (Setter, error) {
	s := &sliceSetter{
		sliceElem: sliceElem,
	}
	s.initFuncs()
	return s, nil
}

type sliceSetter struct {
	sliceElem   reflect.Value
	set         setFunc
	dig         digFunc
	initialized bool
	currentPos  int
}

// Init initializes the setter. The value argument is expected to be the item count
// to be consumed before getting exhausted.
func (s *sliceSetter) Init(arg interface{}) {
	entries, ok := arg.(int)
	if !ok {
		panic(fmt.Errorf("slice setter initialization expects int, got %T", arg))
	}
	if s.initialized && entries == 0 {
		return
	}
	newSlice := reflect.MakeSlice(s.sliceElem.Type(), entries, entries)
	if !s.sliceElem.IsNil() {
		newSlice = reflect.AppendSlice(s.sliceElem, newSlice)
	}
	s.sliceElem.Set(newSlice)
	s.initialized = true
}

// Set put values into the underlying array/slice, as long as the values are
// primitive types.
func (s *sliceSetter) Set(value interface{}) {
	s.set(value)
}

// Dig creates a new complex item type and returns a setter to it. The item is
// inserted in current slice position.
func (s *sliceSetter) Dig() Setter {
	return s.dig()
}

// initFuncs makes this setter to work in any of the following four modes:
//   - slice of non-pointer primitive type
//   - slice of pointer primitive type
//   - slice of non-pointer complex type
//   - slice of pointer complex type
//
// By using pointer functions, the runtime spends less time in executing the
// same 'if' comparisons all the time.
func (s *sliceSetter) initFuncs() {
	isPrim := isPrimitive(s.sliceElem.Type().Elem())
	isPtr := s.sliceElem.Type().Elem().Kind() == reflect.Ptr
	switch {
	case isPrim && !isPtr:
		// Slice type is a primitive, non-pointer type, so dig is not necessary
		// and set is quite straightforward.
		s.set = func(val interface{}) {
			setPrimitive(s.sliceElem.Index(s.currentPos), val)
			s.currentPos++
		}
	case isPrim && isPtr:
		// Slice type is a primitive, pointer type, so dig is not necessary
		// and set creates pointer type to hold the value, then put it into slice.
		s.set = func(val interface{}) {
			v := reflect.ValueOf(val)
			switch v.Kind() {
			case reflect.Invalid:
				s.sliceElem.Index(s.currentPos).Set(reflect.Zero(s.sliceElem.Type().Elem()))
			default:
				vPtr := reflect.New(s.sliceElem.Type().Elem().Elem())
				vPtr.Elem().Set(v)
				s.sliceElem.Index(s.currentPos).Set(vPtr)
			}
			s.currentPos++
		}
	case !isPrim && !isPtr:
		// Slice type is a complex, non-pointer type. Dig is instructed to
		// return a setter to the item's position within the slice. Set is not
		// necessary at all.
		s.dig = func() Setter {
			inner, err := newForPointer(s.sliceElem.Index(s.currentPos).Addr().Interface())
			if err == nil {
				s.currentPos++
				return inner
			}
			panic(fmt.Errorf("unable to dig into complex slice of type %s: %v", s.sliceElem.Type(), err))
		}
	default:
		// Slice type is a complex, pointer type. Dig is instructed to create
		// the item's memory and to return a setter to the item. Set is only
		// necessary for setting the nil pointers.
		s.dig = func() Setter {
			pointedElem := reflect.New(s.sliceElem.Type().Elem().Elem())
			s.sliceElem.Index(s.currentPos).Set(pointedElem)
			inner, err := newForPointer(pointedElem.Elem().Addr().Interface())
			if err == nil {
				s.currentPos++
				return inner
			}
			panic(fmt.Errorf("unable to dig into complex slice of type %s: %v", s.sliceElem.Type(), err))
		}
		// Special nil-setting set function.
		s.set = func(val interface{}) {
			s.sliceElem.Index(s.currentPos).Set(reflect.Zero(s.sliceElem.Type().Elem()))
			s.currentPos++
		}
	}
}
