package setter

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type mapSetterTest struct {
	Name   string
	Salary float64
}

// Primitive map setters fill their values by calling Set for value/key pairs
// (values first!)
func TestPrimitiveMapSetter(t *testing.T) {
	var m map[string]string

	mSetter, err := New(&m)
	require.Nil(t, err)
	require.NotNil(t, mSetter)

	require.Nil(t, m)

	// Init map memory
	mSetter.Init(nil)

	// Put some values on it
	mSetter.Set("Buchannon")
	mSetter.Set("Mitch")

	mSetter.Set("Parker")
	mSetter.Set("C. J.")

	// Assertions
	require.NotNil(t, m)
	require.Equal(t, 2, len(m))
	assert.Equal(t, "Buchannon", m["Mitch"])
	assert.Equal(t, "Parker", m["C. J."])
}

func TestBytesMapSetter(t *testing.T) {
	var m map[string][]byte

	mSetter, err := New(&m)
	require.Nil(t, err)
	require.NotNil(t, mSetter)

	require.Nil(t, m)

	// Init map memory
	mSetter.Init(nil)

	// Put some values on it
	mSetter.Set([]byte("Buchannon"))
	mSetter.Set("Mitch")

	mSetter.Set([]byte("Parker"))
	mSetter.Set("C. J.")

	// Assertions
	require.NotNil(t, m)
	require.Equal(t, 2, len(m))
	assert.Equal(t, []byte("Buchannon"), m["Mitch"])
	assert.Equal(t, []byte("Parker"), m["C. J."])
}

func TestPrimitivePtrMapSetter(t *testing.T) {
	var m map[string]*int32

	mSetter, err := New(&m)
	require.Nil(t, err)
	require.NotNil(t, mSetter)

	require.Nil(t, m)

	// Init map memory
	mSetter.Init(nil)

	// Put some values on it
	mSetter.Set(int32(42))
	mSetter.Set("Mitch")

	mSetter.Set(int32(84))
	mSetter.Set("C. J.")

	// Assertions
	require.NotNil(t, m)
	require.Equal(t, 2, len(m))
	assert.Equal(t, int32(42), *m["Mitch"])
	assert.Equal(t, int32(84), *m["C. J."])
}

// Complex map setters require Dig/Set(nil) for values.
func TestStructMapSetter(t *testing.T) {
	var m map[string]mapSetterTest

	mSetter, err := New(&m)
	require.Nil(t, err)
	require.NotNil(t, mSetter)

	require.Nil(t, m)

	// Init map memory
	mSetter.Init(nil)

	// Put one some values on it
	val := mSetter.Dig()
	val.Set("Mitch Buchannon")
	val.Set(4.2)
	mSetter.Set("boss")

	val = mSetter.Dig()
	val.Set("C. J. Parker")
	val.Set(8.4)
	mSetter.Set("pal")

	// Assertions
	require.NotNil(t, m)
	require.Equal(t, 2, len(m))
	assert.Equal(t, mapSetterTest{"Mitch Buchannon", 4.2}, m["boss"])
	assert.Equal(t, mapSetterTest{"C. J. Parker", 8.4}, m["pal"])
}

func TestStructPtrMapSetter(t *testing.T) {
	var m map[string]*mapSetterTest

	mSetter, err := New(&m)
	require.Nil(t, err)
	require.NotNil(t, mSetter)

	require.Nil(t, m)

	// Init map memory
	mSetter.Init(nil)

	// Put one some values on it
	val := mSetter.Dig()
	val.Set("Mitch Buchannon")
	val.Set(4.2)
	mSetter.Set("boss")

	val = mSetter.Dig()
	val.Set("C. J. Parker")
	val.Set(8.4)
	mSetter.Set("pal")

	// Assertions
	require.NotNil(t, m)
	require.Equal(t, 2, len(m))
	assert.Equal(t, mapSetterTest{"Mitch Buchannon", 4.2}, *m["boss"])
	assert.Equal(t, mapSetterTest{"C. J. Parker", 8.4}, *m["pal"])
}
