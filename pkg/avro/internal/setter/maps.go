package setter

import (
	"fmt"
	"reflect"
)

// newMapSetter creates a setter for the given underlying map, which
// can be nil. Maps' primitive entries are put by calling Set for the value,
// then Set again for the key, which triggers the actual insertion in the map.
// For complex structures, Dig must be called prior to start setting complex
// map values, so a special item inner setter can be created for every value
// type. After setting the item (using this inner setter), Set must be called
// in the map's setter with the key for the value to be added to the map.
func newMapSetter(mapElem reflect.Value) (Setter, error) {
	s := &mapSetter{
		mapElem: mapElem,
	}
	s.initFuncs()
	return s, nil
}

type mapSetter struct {
	mapElem     reflect.Value
	item        reflect.Value
	set         setFunc
	dig         digFunc
	valueSet    bool
	initialized bool
}

// Init actually creates the memory for the underlying map.
func (s *mapSetter) Init(arg interface{}) {
	if s.initialized {
		return
	}
	m := reflect.MakeMap(s.mapElem.Type())
	s.mapElem.Set(m)
	s.initialized = true
}

// Set appends key/values to the map. Set must be called in pairs key/value
// for primitive items, or be called a single time with the key for complex items.
func (s *mapSetter) Set(value interface{}) {
	s.set(value)
}

// Dig actually reserves the memory for the item type and returns a setter to it.
// After finishing setting this inner setter, Set must be called with the
// entry's key on this mapSetter for the item to be added to the map.
func (s *mapSetter) Dig() Setter {
	return s.dig()
}

// setVal is a convenience function to keep track of the value on odd calls
// for primitive types, since even calls make the actual map insertion.
func (s *mapSetter) setVal(key interface{}) bool {
	if !s.valueSet {
		s.item = reflect.ValueOf(key)
	}
	s.valueSet = !s.valueSet
	return s.valueSet
}

// initFuncs makes this setter to work in any of the following four modes:
//  - map of non-pointer primitive type
//  - map of pointer primitive type
//  - map of non-pointer complex type
//  - map of pointer complex type
// By using pointer functions, the runtime spends less time in executing the
// same 'if' comparisons all the time.
func (s *mapSetter) initFuncs() {
	isPrim := isPrimitive(s.mapElem.Type().Elem())
	isPtr := s.mapElem.Type().Elem().Kind() == reflect.Ptr
	switch {
	case isPrim && !isPtr:
		// Map type is a primitive, non-pointer type, so dig is not necessary
		// and set is quite straightforward: 1st set, cache value; 2nd set,
		// put item into map.
		s.set = func(val interface{}) {
			if s.setVal(val) {
				return
			}
			s.mapElem.SetMapIndex(reflect.ValueOf(val), s.item)
		}
	case isPrim && isPtr:
		// Map type is a primitive, pointer type, so dig is not necessary
		// and set is easy: 1st set, cache value; 2nd set, create pointer type
		// to hold the value and put item into map.
		s.set = func(val interface{}) {
			if s.setVal(val) {
				return
			}
			switch s.item.Kind() {
			case reflect.Invalid:
				s.mapElem.SetMapIndex(reflect.ValueOf(val), reflect.Zero(s.mapElem.Type().Elem()))
			default:
				vPtr := reflect.New(s.mapElem.Type().Elem().Elem())
				vPtr.Elem().Set(s.item)
				s.mapElem.SetMapIndex(reflect.ValueOf(val), vPtr)
			}
		}
	case !isPrim && !isPtr:
		// Map type is a complex, non-pointer type. Dig is instructed to
		// create the complex struct and to return a setter to it. Set is only
		// called to finally put the item into the map and is shared with
		// the last case.
		s.dig = func() Setter {
			s.item = reflect.New(s.mapElem.Type().Elem())
			inner, err := newForPointer(s.item.Elem().Addr().Interface())
			if err == nil {
				return inner
			}
			panic(fmt.Errorf("unable to dig into complex map of type %s: %v", s.mapElem.Type(), err))
		}
	default:
		// Map type is a complex, pointer type. Dig is instructed to
		// create the pointer to the complex struct and to return a setter to
		// it. Set is only called to finally put the item into the map and is
		// shared with the last case.
		s.dig = func() Setter {
			pointedElem := reflect.New(s.mapElem.Type().Elem().Elem())
			s.item = reflect.New(s.mapElem.Type().Elem())
			s.item.Elem().Set(pointedElem)
			inner, err := newForPointer(pointedElem.Elem().Addr().Interface())
			if err == nil {
				return inner
			}
			panic(fmt.Errorf("unable to dig into complex map of type %s: %v", s.mapElem.Type(), err))
		}
	}
	// If it's complex case, the common Set function is just to put the value
	// into the map.
	if !isPrim {
		s.set = func(val interface{}) {
			s.mapElem.SetMapIndex(reflect.ValueOf(val), s.item.Elem())
		}
	}
}
