package setter

import (
	"errors"
	"fmt"
	"reflect"
)

var (
	errInvalidUnionStruct = errors.New("unable to create setter: unions require the target struct to export a method 'Set(interface{})'")
)

// newUnionSetter creates a setter for any arbitrary empty struct (one with no
// public fields) which must be exporting a method named "Set".
// Init must be called to initialize the union's value type. Then:
//  - call Set to put a primitive value
//  - call Dig to initialize a complex type, then Set to put it into the union.
// Dig must be called prior to start setting complex values, so a special item
// setter can be created for a single item. After finishing setting the item
// (using its setter), Set must be called on the union's setter for the item
// to be set into the target struct. This final Set call's value is ignored,
// so it is safe to use nil for that.
// Since spec forbids unions-directly-within-unions, all valid union types
// cannot be pointer types.
func newUnionSetter(unionElemPtr reflect.Value, unionTypes []reflect.Type) (Setter, error) {
	setMethod := unionElemPtr.MethodByName("Set")
	if setMethod.Kind() != reflect.Func {
		return nil, errInvalidUnionStruct
	}
	return &unionSetter{
		types:     unionTypes,
		setMethod: setMethod,
	}, nil
}

type unionSetter struct {
	elemType  reflect.Type
	item      reflect.Value
	types     []reflect.Type
	setMethod reflect.Value
	primitive bool
}

// Union setter can be initialized with the order its types
// are to be assigned.
func (s *unionSetter) Init(arg interface{}) {
	selectedType, ok := arg.(int)
	if ok {
		s.elemType = s.types[selectedType].Elem()
		s.primitive = isNonPointerPrimitive(s.elemType)
		return
	}
	panic(fmt.Errorf("union setter initialization expects int, got %T", arg))
}

// Set puts the value into the current field and advances the index to the
// next field.
func (s *unionSetter) Set(value interface{}) {
	var v reflect.Value
	switch s.primitive {
	case true:
		v = reflect.ValueOf(value)
	default:
		v = s.item.Elem()
	}
	s.setMethod.Call([]reflect.Value{v.Convert(s.elemType)})
}

// Dig returns a setter for the current field's pointer type and advances the
// index of this setter to the next field. Current field must be a pointer
// type.
func (s *unionSetter) Dig() Setter {
	if s.primitive {
		panic("unable to dig into primitive type union setter")
	}

	// Create the complex inner type and try to detect a Setter from it
	s.item = reflect.New(s.elemType)
	inner, err := newForPointer(s.item.Elem().Addr().Interface())
	if err == nil {
		return inner
	}
	panic(fmt.Errorf("unable to dig into complex union value of type %s: %v", s.elemType, err))
}
