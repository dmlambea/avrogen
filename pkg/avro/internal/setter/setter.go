package setter

import (
	"errors"
	"fmt"
	"reflect"
)

// Setter defines the operations for putting data into arbitrary structure types.
type Setter interface {
	Init(arg interface{})
	Set(val interface{})
	Dig() Setter
}

// Complex setters optimize the runtime operations by creating a special
// version of their Set and/or Dig operations. So the following types are used
// with that on mind.
type setFunc func(val interface{})
type digFunc func() Setter

// New creates a setter for an arbitrary type. ObjAddr must be the address of
// the given object.
func New(objAddr interface{}) (Setter, error) {
	if s, err := newForPointer(objAddr); err == nil {
		return s, nil
	}
	panic(fmt.Errorf("unable to create setter: don't know what to do with a %T", objAddr))
}

var (
	errNil    = errors.New("unable to create setter: nil address")
	errNonPtr = errors.New("unable to create setter: not a pointer")
)

// Any union-type struct must implement this interface, so the setter can
// learn what types are accepted for the union, and their order for indexing them.
type unionTypesInformer interface {
	UnionTypes() []reflect.Type
}

// newForPointer creates a setter for the given pointer type, pointed by objAddr.
func newForPointer(objAddr interface{}) (Setter, error) {
	ptr, err := assertPointer(objAddr)
	if err != nil {
		return nil, err
	}

	elem := ptr.Elem()
	switch elem.Kind() {
	case reflect.Struct:
		if s, err := newStructSetter(elem); err == nil {
			return s, err
		}
		emptyStruct := elem.Addr().Interface()
		if u, ok := emptyStruct.(unionTypesInformer); ok {
			return newUnionSetter(ptr, u.UnionTypes())
		}
		return nil, fmt.Errorf("unable to create setter: %T is not a union and does not export any field", emptyStruct)
	case reflect.Slice, reflect.Array:
		return newSliceSetter(elem)
	case reflect.Map:
		return newMapSetter(elem)
	default:
		if isPrimitive(elem.Type()) {
			return newPrimitiveSetter(elem)
		}
		return nil, fmt.Errorf("unsupported type %s", elem.Kind())
	}
}

func assertPointer(objAddr interface{}) (ptr reflect.Value, err error) {
	if objAddr == nil {
		err = errNil
	} else {
		ptr = reflect.ValueOf(objAddr)
		if ptr.Kind() != reflect.Ptr {
			err = errNonPtr
		}
	}
	return
}

// isPrimitive returns true if k is of any of the Avro primitive types, or a
// pointer to any of the Avro primitive types.
func isPrimitive(elem reflect.Type) bool {
	if elem.Kind() == reflect.Ptr {
		elem = elem.Elem()
	}
	return isNonPointerPrimitive(elem)
}

// isNonPointerPrimitive returns true if k is of any of the Avro primitive types.
func isNonPointerPrimitive(elem reflect.Type) bool {
	switch elem.Kind() {
	case reflect.Bool, reflect.Int32, reflect.Int64, reflect.Float32, reflect.Float64, reflect.String:
		return true
	case reflect.Slice:
		return elem.Elem().Kind() == reflect.Uint8
	}
	return false
}

// setPrimitive puts the value onto the receiver, for any value of any of the
// valid Avro primitive types. If the receiver is a pointer-type, its memory
// is initialized. The workflow goes from the most used case to the least.
func setPrimitive(where reflect.Value, what interface{}) {
	v := reflect.ValueOf(what)

	// Receiver is a pointer-type: initialize memory if needed, then reference it for later assignment
	if where.Kind() == reflect.Ptr {
		// Nil things
		if !v.IsValid() {
			where.Set(reflect.Zero(where.Type()))
			return
		}
		newItem := reflect.New(where.Type().Elem())
		where.Set(newItem)
		where = newItem.Elem()
	}

	// On nil values, return
	if !v.IsValid() {
		return
	}

	// Receiver is a Bytes type
	if where.Kind() == reflect.Array {
		reflect.Copy(where, v)
		return
	}

	// Receiver is convertible
	whereType := where.Type()
	if vType := v.Type(); !vType.AssignableTo(whereType) {
		if !vType.ConvertibleTo(whereType) {
			panic(fmt.Errorf("incompatible types: %s cannot be assigned to %s", vType, whereType))
		}
		v = v.Convert(whereType)
	}
	where.Set(v)
}
