package avro

import (
	"fmt"

	"gitlab.com/dmlambea/avrogen/pkg/types"
)

func (d deserializer) deserializeDefaultType(t types.Generic, def interface{}) {
	switch realType := t.(type) {
	case *types.Primitive:
		d.deserializeDefaultPrimitiveType(realType, def)
	case *types.Enum:
		d.deserializeDefaultEnumType(realType, def.(string))
	case *types.Fixed:
		d.deserializeDefaultFixedType(realType, def)
	case *types.Union:
		if !realType.IsOptional() || !realType.IsOptionalSimple() {
			// Union's default fields are always 0
			d = d.stepInto()
			d.s.Init(0)
		}
		childType := asField(realType.Children()[0]).Type()
		d.deserializeDefaultType(childType, def)
	case *types.Record:
		d.stepInto().deserializeDefaultRecordType(realType, def)
	default:
		panic(fmt.Errorf("default value for type %T not supported", realType))
	}
}

func (d deserializer) deserializeDefaultPrimitiveType(t *types.Primitive, def interface{}) {
	d.s.Set(def)
}

func (d deserializer) deserializeDefaultEnumType(t *types.Enum, def string) {
	value := int32(t.IndexOfSymbol(def))
	if value < 0 {
		panic(fmt.Errorf("invalid symbol '%s' for enum type %s", def, t.Name()))
	}
	d.s.Set(value)
}

func (d deserializer) deserializeDefaultFixedType(t *types.Fixed, def interface{}) {
	d.s.Set(def)
}

func (d deserializer) deserializeDefaultRecordType(t *types.Record, def interface{}) {
	m, ok := def.(map[string]interface{})
	if !ok {
		panic(fmt.Errorf("invalid default value type for record %s: expected map[string]interface{}, got %T", t.Name(), def))
	}
	for _, child := range t.Children() {
		f := asField(child)
		val, ok := m[f.Name()]
		switch {
		case ok == true:
			d.deserializeDefaultType(f.Type(), f.CastValue(val))
		case f.HasDefault():
			d.deserializeDefaultType(f.Type(), f.Default())
		default:
			panic(fmt.Errorf("no default value found for field %s and record %s has no default value for it", f.Name(), t.Name()))
		}
	}
	return
}
