package avro

import (
	"fmt"
	"io"
	"math"
)

type stringWriter interface {
	WriteString(string) (int, error)
}

type byteWriter interface {
	Grow(int)
	WriteByte(byte) error
}

// WritePrimitive is a generic writer for primitive types
func WritePrimitive(p interface{}, w io.Writer) error {
	switch t := p.(type) {
	case bool:
		return WriteBool(t, w)
	case []byte:
		return WriteBytes(t, w)
	case float64:
		return WriteDouble(t, w)
	case int32:
		return WriteInt(t, w)
	case int64:
		return WriteLong(t, w)
	case float32:
		return WriteFloat(t, w)
	case string:
		return WriteString(t, w)
	default:
		return fmt.Errorf("unknown primitive type %T", t)
	}
}

// WriteByte writes the given byte
func WriteByte(b byte, w io.Writer) (err error) {
	if bw, ok := w.(byteWriter); ok {
		err = bw.WriteByte(b)
	} else {
		bb := make([]byte, 1)
		bb[0] = b
		_, err = w.Write(bb)
	}
	return
}

// WriteBool writes a bool value
func WriteBool(r bool, w io.Writer) error {
	var b byte
	if r {
		b = byte(1)
	}
	return WriteByte(b, w)
}

// WriteBytes writes the length of the given slice of bytes, then the slice itself.
func WriteBytes(r []byte, w io.Writer) error {
	err := WriteLong(int64(len(r)), w)
	if err != nil {
		return err
	}
	_, err = w.Write(r)
	return err
}

// WriteDouble writes the given float64 type using zigzag-encoding.
func WriteDouble(r float64, w io.Writer) error {
	bits := uint64(math.Float64bits(r))
	const byteCount = 8
	return encodeFloat(w, byteCount, bits)
}

// WriteInt writes the given int32 type using zigzag-encoding.
func WriteInt(r int32, w io.Writer) error {
	downShift := uint32(31)
	encoded := uint64((uint32(r) << 1) ^ uint32(r>>downShift))
	const maxByteSize = 5
	return encodeInt(w, maxByteSize, encoded)
}

// WriteLong writes the given int64 type using zigzag-encoding.
func WriteLong(r int64, w io.Writer) error {
	downShift := uint64(63)
	encoded := uint64((r << 1) ^ (r >> downShift))
	const maxByteSize = 10
	return encodeInt(w, maxByteSize, encoded)
}

// WriteFloat writes the given float32 type using zigzag-encoding.
func WriteFloat(r float32, w io.Writer) error {
	bits := uint64(math.Float32bits(r))
	const byteCount = 4
	return encodeFloat(w, byteCount, bits)
}

// WriteString writes the length of the given string using zigzag-encoding,
// then the string data.
func WriteString(r string, w io.Writer) error {
	err := WriteLong(int64(len(r)), w)
	if err != nil {
		return err
	}
	if sw, ok := w.(stringWriter); ok {
		_, err = sw.WriteString(r)
	} else {
		_, err = w.Write([]byte(r))
	}
	return err
}

func encodeFloat(w io.Writer, byteCount int, bits uint64) error {
	var err error
	var bb []byte
	bw, ok := w.(byteWriter)
	if ok {
		bw.Grow(byteCount)
	} else {
		bb = make([]byte, 0, byteCount)
	}
	for i := 0; i < byteCount; i++ {
		if bw != nil {
			err = bw.WriteByte(byte(bits & 255))
			if err != nil {
				return err
			}
		} else {
			bb = append(bb, byte(bits&255))
		}
		bits = bits >> 8
	}
	if bw == nil {
		_, err = w.Write(bb)
		return err
	}
	return nil
}

func encodeInt(w io.Writer, byteCount int, encoded uint64) error {
	var err error
	var bb []byte
	bw, ok := w.(byteWriter)
	// To avoid reallocations, grow capacity to the largest possible size
	// for this integer
	if ok {
		bw.Grow(byteCount)
	} else {
		bb = make([]byte, 0, byteCount)
	}

	if encoded == 0 {
		if bw != nil {
			err = bw.WriteByte(0)
			if err != nil {
				return err
			}
		} else {
			bb = append(bb, byte(0))
		}
	} else {
		for encoded > 0 {
			b := byte(encoded & 127)
			encoded = encoded >> 7
			if !(encoded == 0) {
				b |= 128
			}
			if bw != nil {
				err = bw.WriteByte(b)
				if err != nil {
					return err
				}
			} else {
				bb = append(bb, b)
			}
		}
	}
	if bw == nil {
		_, err := w.Write(bb)
		return err
	}
	return nil

}
