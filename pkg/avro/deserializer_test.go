package avro

import (
	"bytes"
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDeserializerPrimitiveCases(t *testing.T) {
	cases := []deserializerTestFixture{
		{`"int"`, `"int"`, &deserializerTestIntValueHolder{}, &deserializerTestIntValueHolder{}, int32(1001), int32(1001)},
		{`"int"`, `"long"`, &deserializerTestIntValueHolder{}, &deserializerTestLongValueHolder{}, int32(1002), int32(1002)},
		{`"int"`, `"float"`, &deserializerTestIntValueHolder{}, &deserializerTestFloatValueHolder{}, int32(1003), int32(1003)},
		{`"int"`, `"double"`, &deserializerTestIntValueHolder{}, &deserializerTestDoubleValueHolder{}, int32(1004), int32(1004)},
		{`"long"`, `"long"`, &deserializerTestLongValueHolder{}, &deserializerTestLongValueHolder{}, int64(2001), int64(2001)},
		{`"long"`, `"float"`, &deserializerTestLongValueHolder{}, &deserializerTestFloatValueHolder{}, int64(2002), int64(2002)},
		{`"long"`, `"double"`, &deserializerTestLongValueHolder{}, &deserializerTestDoubleValueHolder{}, int64(2003), int64(2003)},
		{`"float"`, `"float"`, &deserializerTestFloatValueHolder{}, &deserializerTestFloatValueHolder{}, float32(300.1), float32(300.1)},
		{`"float"`, `"double"`, &deserializerTestFloatValueHolder{}, &deserializerTestDoubleValueHolder{}, float32(300.2), float32(300.2)},
		{`"double"`, `"double"`, &deserializerTestDoubleValueHolder{}, &deserializerTestDoubleValueHolder{}, float64(400.1), float64(400.1)},

		// String and bytes fields are interchangable
		{`"string"`, `"string"`, &deserializerTestStringValueHolder{}, &deserializerTestStringValueHolder{}, "one", "one"},
		{`"string"`, `"bytes"`, &deserializerTestStringValueHolder{}, &deserializerTestBytesValueHolder{}, "two", []byte("two")},
		{`"bytes"`, `"bytes"`, &deserializerTestBytesValueHolder{}, &deserializerTestBytesValueHolder{}, []byte("three"), []byte("three")},
		{`"bytes"`, `"string"`, &deserializerTestBytesValueHolder{}, &deserializerTestStringValueHolder{}, []byte("four"), "four"},
	}

	for _, c := range cases {
		deserializerTestRoundtrip(t, c, func(fixture deserializerTestFixture, w io.Writer) {
			WritePrimitive(fixture.srcHolder.getValue(), w)
		})
	}
}

func TestDeserializerFixedCases(t *testing.T) {
	f := deserializerTestFixture{
		srcSchema: `{ "name": "macAddress", "type": "fixed", "size": 6 }`,
		dstSchema: `{ "name": "macAddress", "type": "fixed", "size": 6 }`,
		srcHolder: &deserializerTestFixed6ValueHolder{},
		dstHolder: &deserializerTestFixed6ValueHolder{},
		srcValue:  [6]byte{0x01, 0x02, 0x03, 0x04, 0x05, 0x06},
		dstValue:  [6]byte{0x01, 0x02, 0x03, 0x04, 0x05, 0x06},
	}

	deserializerTestRoundtrip(t, f, func(fixture deserializerTestFixture, w io.Writer) {
		macAddress := fixture.srcHolder.getValue().([6]byte)
		w.Write(macAddress[:])
	})
}

func deserializerTestRoundtrip(t *testing.T, fixture deserializerTestFixture, writerFunc deserializerTestWriterFunc) {
	fixture.srcHolder.setValue(fixture.srcValue)

	var buf bytes.Buffer
	writerFunc(fixture, &buf)

	err := DeserializeFromJSONSchemas(&buf, fixture.srcSchema, fixture.dstSchema, fixture.dstHolder)
	assert.Nil(t, err)
	assert.EqualValues(t, fixture.dstValue, fixture.dstHolder.getValue())
}

type deserializerTestWriterFunc func(fixture deserializerTestFixture, w io.Writer)

type deserializerTestValueHolder interface {
	getValue() interface{}
	setValue(val interface{})
}

type deserializerTestFixture struct {
	srcSchema string
	dstSchema string
	srcHolder deserializerTestValueHolder
	dstHolder deserializerTestValueHolder
	srcValue  interface{}
	dstValue  interface{}
}

// Deserializer's test structs

type deserializerTestBoolValueHolder struct {
	Value bool
}

func (h *deserializerTestBoolValueHolder) getValue() interface{} {
	return h.Value
}

func (h *deserializerTestBoolValueHolder) setValue(val interface{}) {
	h.Value = val.(bool)
}

type deserializerTestIntValueHolder struct {
	Value int32
}

func (h *deserializerTestIntValueHolder) getValue() interface{} {
	return h.Value
}

func (h *deserializerTestIntValueHolder) setValue(val interface{}) {
	h.Value = val.(int32)
}

type deserializerTestLongValueHolder struct {
	Value int64
}

func (h *deserializerTestLongValueHolder) getValue() interface{} {
	return h.Value
}

func (h *deserializerTestLongValueHolder) setValue(val interface{}) {
	h.Value = val.(int64)
}

type deserializerTestFloatValueHolder struct {
	Value float32
}

func (h *deserializerTestFloatValueHolder) getValue() interface{} {
	return h.Value
}

func (h *deserializerTestFloatValueHolder) setValue(val interface{}) {
	h.Value = val.(float32)
}

type deserializerTestDoubleValueHolder struct {
	Value float64
}

func (h *deserializerTestDoubleValueHolder) getValue() interface{} {
	return h.Value
}

func (h *deserializerTestDoubleValueHolder) setValue(val interface{}) {
	h.Value = val.(float64)
}

type deserializerTestStringValueHolder struct {
	Value string
}

func (h *deserializerTestStringValueHolder) getValue() interface{} {
	return h.Value
}

func (h *deserializerTestStringValueHolder) setValue(val interface{}) {
	h.Value = val.(string)
}

type deserializerTestBytesValueHolder struct {
	Value []byte
}

func (h *deserializerTestBytesValueHolder) getValue() interface{} {
	return h.Value
}

func (h *deserializerTestBytesValueHolder) setValue(val interface{}) {
	h.Value = val.([]byte)
}

type deserializerTestFixed6ValueHolder struct {
	Value [6]byte
}

func (h *deserializerTestFixed6ValueHolder) getValue() interface{} {
	return h.Value
}

func (h *deserializerTestFixed6ValueHolder) setValue(val interface{}) {
	h.Value = val.([6]byte)
}
