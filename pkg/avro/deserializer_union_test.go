package avro

import (
	"bytes"
	"fmt"
	"reflect"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

// *** This testsuite checks some union cases, not all of them ***
// Currently, optional-complex unions require their reader schema to be a
// child of another type (record, map, array).

const deserializerTestUnionSchema = `["boolean", "int", "long", "float", "double", "string", "bytes"]`

var deserializerTestUnionSchemaList = []string{"boolean", "int", "long", "float", "double", "string", "bytes"}

// deserializerTestUnion is a union-type compatible structure to hold values
// for schemas int, long, float, double, string and bytes, in that order.
type deserializerTestUnion struct {
	value interface{}
}

func (u *deserializerTestUnion) Set(val interface{}) {
	u.value = val
}

func (u deserializerTestUnion) UnionTypes() []reflect.Type {
	return []reflect.Type{
		reflect.TypeOf((*bool)(nil)),
		reflect.TypeOf((*int32)(nil)),
		reflect.TypeOf((*int64)(nil)),
		reflect.TypeOf((*float32)(nil)),
		reflect.TypeOf((*float64)(nil)),
		reflect.TypeOf((*string)(nil)),
		reflect.TypeOf((*[]byte)(nil)),
	}
}

func TestUnionToNonUnion(t *testing.T) {
	// Create writer schema and input data, in the same order as field types'
	wSchema := `["null", "int", "string"]`

	var buf bytes.Buffer

	// Test the int case
	WriteLong(1, &buf)
	WritePrimitive(int32(42), &buf)

	var dst int32
	require.Nil(t, DeserializeFromJSONSchemas(&buf, wSchema, `"int"`, &dst))
	assert.Equal(t, int32(42), dst)

	// Test the nil case
	WriteLong(0, &buf)

	require.NotNil(t, DeserializeFromJSONSchemas(&buf, wSchema, `"int"`, &dst))

	// Test the string case
	WriteLong(2, &buf)
	WritePrimitive("Hi, there!", &buf)

	var str string
	require.Nil(t, DeserializeFromJSONSchemas(&buf, wSchema, `"string"`, &str))
	assert.Equal(t, "Hi, there!", str)
}

func TestNonUnionToUnion(t *testing.T) {
	var buf bytes.Buffer
	var dst deserializerTestUnion

	// Test the int case
	WritePrimitive(int32(42), &buf)

	require.Nil(t, DeserializeFromJSONSchemas(&buf, `"int"`, deserializerTestUnionSchema, &dst))
	assert.Equal(t, int32(42), dst.value)

	// Test the string case
	WritePrimitive("Hi, there!", &buf)

	require.Nil(t, DeserializeFromJSONSchemas(&buf, `"string"`, deserializerTestUnionSchema, &dst))
	assert.Equal(t, "Hi, there!", dst.value)
}

func TestCombinationRunnerNonOptionalUnion(t *testing.T) {
	schemas := []string{
		"int",
		"long",
		"float",
		"double",
		"bytes",
	}

	for _, s := range schemas {
		fn := func(sel []string) {
			roundtripNonOptionalUnionCombination(t, sel, deserializerTestUnionSchemaList, "boolean")
			roundtripNonOptionalUnionCombination(t, sel, deserializerTestUnionSchemaList, "string")
			roundtripNonOptionalUnionCombination(t, sel, deserializerTestUnionSchemaList, s)
		}

		runCombinations(fn, nil, deserializerTestUnionSchemaList)
	}
}

func TestCombinationRunnerSimpleOptionalUnion(t *testing.T) {
	fn := func(sel []string) {
		var testInt int32 = 42
		roundtripOptionalUnionCombination(t, sel, []string{"null", "int"}, nil)
		roundtripOptionalUnionCombination(t, sel, []string{"null", "int"}, &testInt)

		roundtripOptionalUnionCombination(t, sel, []string{"int", "null"}, nil)
		roundtripOptionalUnionCombination(t, sel, []string{"int", "null"}, &testInt)
	}

	runCombinations(fn, nil, []string{"null", "int"})
}

func roundtripNonOptionalUnionCombination(t *testing.T, writerTypes, readerTypes []string, selectedWriterType string) {
	var src, dst deserializerTestUnion

	// Create writer schema and input data, in the same order as field types'
	schemaFmt := `["%s"]`
	wSchema := fmt.Sprintf(schemaFmt, strings.Join(writerTypes, `","`))
	rSchema := fmt.Sprintf(schemaFmt, strings.Join(readerTypes, `","`))

	// Find selected fields in writer's and reader's schemas
	var rField, wField int64 = -1, -1
	for i := range writerTypes {
		if writerTypes[i] == selectedWriterType {
			wField = int64(i)
		}
		if readerTypes[i] == selectedWriterType {
			rField = int64(i)
		}
	}
	require.GreaterOrEqual(t, wField, int64(0))
	require.GreaterOrEqual(t, rField, int64(0))

	var buf bytes.Buffer
	switch readerTypes[rField] {
	case "boolean":
		src.Set(true)
	case "int":
		src.Set(int32(2))
	case "long":
		src.Set(int64(3))
	case "float":
		src.Set(float32(4.4))
	case "double":
		src.Set(float64(5.5))
	case "string":
		src.Set("six")
	case "bytes":
		src.Set([]byte("seven"))
	}
	WriteLong(wField, &buf)
	if selectedWriterType != "null" {
		WritePrimitive(src.value, &buf)
	}

	require.Nil(t, DeserializeFromJSONSchemas(&buf, wSchema, rSchema, &dst))
	assert.Equal(t, src, dst)
}

func roundtripOptionalUnionCombination(t *testing.T, writerTypes, readerTypes []string, ptr *int32) {
	var src, dst *int32
	src = ptr

	// Create writer schema and input data, in the same order as field types'
	schemaFmt := `["%s"]`
	wSchema := fmt.Sprintf(schemaFmt, strings.Join(writerTypes, `","`))
	rSchema := fmt.Sprintf(schemaFmt, strings.Join(readerTypes, `","`))

	// Find selected fields in writer's and reader's schemas
	var wField int64 = -1
	selectedWriterType := "int"
	if ptr == nil {
		selectedWriterType = "null"
	}
	for i := range writerTypes {
		if writerTypes[i] == selectedWriterType {
			wField = int64(i)
			break
		}
	}
	require.GreaterOrEqual(t, wField, int64(0))

	var buf bytes.Buffer
	WriteLong(wField, &buf)
	if ptr != nil {
		WritePrimitive(int32(*ptr), &buf)
	}

	require.Nil(t, DeserializeFromJSONSchemas(&buf, wSchema, rSchema, &dst))
	assert.Equal(t, src, dst)
}
