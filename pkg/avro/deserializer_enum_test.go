package avro

import (
	"io"
	"testing"
)

func TestDeserializerEnumCases(t *testing.T) {
	cases := []deserializerTestFixture{
		// One-to-one valid enums
		{
			`{ "name": "status", "type": "enum", "symbols": ["anon", "regular", "vip"] }`,
			`{ "name": "status", "type": "enum", "symbols": ["anon", "regular", "vip"] }`,
			&deserializerTestIntValueHolder{}, &deserializerTestIntValueHolder{}, int32(0), int32(0),
		},
		{
			`{ "name": "status", "type": "enum", "symbols": ["anon", "regular", "vip"] }`,
			`{ "name": "status", "type": "enum", "symbols": ["anon", "regular", "vip"] }`,
			&deserializerTestIntValueHolder{}, &deserializerTestIntValueHolder{}, int32(1), int32(1),
		},
		{
			`{ "name": "status", "type": "enum", "symbols": ["anon", "regular", "vip"] }`,
			`{ "name": "status", "type": "enum", "symbols": ["anon", "regular", "vip"] }`,
			&deserializerTestIntValueHolder{}, &deserializerTestIntValueHolder{}, int32(2), int32(2),
		},

		// Mismatched symbols, valid enums
		{
			`{ "name": "status", "type": "enum", "symbols": ["anon", "regular", "vip"] }`,
			`{ "name": "status", "type": "enum", "symbols": ["vip", "regular", "anon"] }`,
			&deserializerTestIntValueHolder{}, &deserializerTestIntValueHolder{}, int32(0), int32(2),
		},
		{
			`{ "name": "status", "type": "enum", "symbols": ["anon", "regular", "vip"] }`,
			`{ "name": "status", "type": "enum", "symbols": ["vip", "regular", "anon"] }`,
			&deserializerTestIntValueHolder{}, &deserializerTestIntValueHolder{}, int32(1), int32(1),
		},
		{
			`{ "name": "status", "type": "enum", "symbols": ["anon", "regular", "vip"] }`,
			`{ "name": "status", "type": "enum", "symbols": ["vip", "regular", "anon"] }`,
			&deserializerTestIntValueHolder{}, &deserializerTestIntValueHolder{}, int32(2), int32(0),
		},

		// Evolved symbols, valid enums
		{
			`{ "name": "status", "type": "enum", "symbols": ["anon", "vip"] }`,
			`{ "name": "status", "type": "enum", "symbols": ["anon", "regular", "vip"] }`,
			&deserializerTestIntValueHolder{}, &deserializerTestIntValueHolder{}, int32(0), int32(0),
		},
		{
			`{ "name": "status", "type": "enum", "symbols": ["anon", "vip"] }`,
			`{ "name": "status", "type": "enum", "symbols": ["regular", "anon", "vip"] }`,
			&deserializerTestIntValueHolder{}, &deserializerTestIntValueHolder{}, int32(0), int32(1),
		},
		{
			`{ "name": "status", "type": "enum", "symbols": ["anon", "vip"] }`,
			`{ "name": "status", "type": "enum", "symbols": ["vip", "regular", "anon"] }`,
			&deserializerTestIntValueHolder{}, &deserializerTestIntValueHolder{}, int32(0), int32(2),
		},

		// Default-valued readers
		{
			`{ "name": "status", "type": "enum", "symbols": ["anon", "regular", "vip"] }`,
			`{ "name": "status", "type": "enum", "symbols": ["anon", "vip"], "default": "anon" }`,
			&deserializerTestIntValueHolder{}, &deserializerTestIntValueHolder{}, int32(0), int32(0),
		},
		{
			`{ "name": "status", "type": "enum", "symbols": ["anon", "regular", "vip"] }`,
			`{ "name": "status", "type": "enum", "symbols": ["anon", "vip"], "default": "anon" }`,
			&deserializerTestIntValueHolder{}, &deserializerTestIntValueHolder{}, int32(1), int32(0),
		},
		{
			`{ "name": "status", "type": "enum", "symbols": ["anon", "regular", "vip"] }`,
			`{ "name": "status", "type": "enum", "symbols": ["anon", "vip"], "default": "anon" }`,
			&deserializerTestIntValueHolder{}, &deserializerTestIntValueHolder{}, int32(2), int32(1),
		},
	}

	for _, c := range cases {
		deserializerTestRoundtrip(t, c, func(fixture deserializerTestFixture, w io.Writer) {
			WritePrimitive(fixture.srcHolder.getValue(), w)
		})
	}
}
